# Bitsquid Blender Tools - Compiled Importing

This is a fork of [Bitsquid Blender Tools](https://gitlab.com/lschwiderski/bitsquid-blender-tools).


## Goal
As current Vermintide 2 extracting tools are not yet capable of fully decompiling models or animations, this fork is focused on developing the ability to import models and animations directly from the game files. A priority is put on reducing the amount of tedious work required to sort through and view assets.

Ideally this addon would assist users in creating mods by letting them:
* Import `.unit` files, and see models with the appropriate textures loaded (partially done)
* Import `.animation` files (done)
* Export models and animations to Bitsquid Intermediate (`.bsi`) format
* Export materials and textures in formats suitable for compiling into mods

Maybe Darktide support as well?

## Installing and Usage
See the [wiki](https://gitlab.com/qasikfwn/bitsquid-blender-tools/-/wikis/home).

## Current importing status

| Data type | Import support | Export support | Notes
| --- | --- | --- | --- |
| Meshes| Full | None | |
| Vertex normals | Full | None | |
| Vertex weights | Full | None | |
| Skeletons | Partial | None | Almost all skeletons can be imported. |
| Terrains | None | None | |
| Simple animations | Partial | None | Units with multiple simple animation groups are not yet supported. |
| `.animation` files | Full | None | Determining which animations are for a given unit is clunky. |
| Textures | Partial | None | Blender can already import `.dds` files, but it does not support all compression types. See wiki.
| `.material` files | Limited | Basic | A basic template material is applied to meshes; some textures are automatically applied. |
| `.state_machine` files | Partial | None | Animation state machines can be deserialized, but not all the data meanings are known. |
