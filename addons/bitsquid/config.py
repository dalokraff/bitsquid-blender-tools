"""
this file is needed to hold inter-module global variables (because python)
"""

from collections import namedtuple
from dataclasses import dataclass
from typing import Optional
import shutil
from bitsquid.stingray import ImportedObjects as ImportedObjects  # ew circular import

# Offset within a file while deserializing
offset = 0

# List of joints in a .unit file
joint_list: list[str] = []

# list of imported objects done by the script currently operating
imported_objects: ImportedObjects = ImportedObjects()

# debug
# if enabled, record all IDString values in these lists (as ints) then write to disk (as strings)
dump_idstrings = True
seen_idstring64: list[int] = []
seen_idstring32: list[int] = []


@dataclass
class GlobalImportSettings:
    LOOKUP_IDSTRINGS: bool
    # direction to face?


GlobalSettings = GlobalImportSettings(LOOKUP_IDSTRINGS=True)


# import settings holder for compiled unit
@dataclass
class CompiledUnitImportSettings:
    IMPORT_MESHES: bool
    IMPORT_CUSTOM_NORMALS: bool
    IMPORT_BLEND_WEIGHTS: bool
    IMPORT_VERTEX_COLORS: bool
    IMPORT_TEXCOORDS: bool
    FLIP_UVS: bool
    IMPORT_EMPTIES: bool
    IMPORT_JOINTS: bool
    NODE_PARENTING: bool
    IMPORT_SIMPLEANIMS: bool
    IMPORT_MATERIALS: bool
    IMPORT_TEXTURES: bool


CompiledUnitSettings = CompiledUnitImportSettings(
    IMPORT_MESHES=True,
    IMPORT_CUSTOM_NORMALS=True,
    IMPORT_BLEND_WEIGHTS=True,
    IMPORT_VERTEX_COLORS=True,
    IMPORT_TEXCOORDS=True,
    FLIP_UVS=True,
    IMPORT_EMPTIES=True,
    IMPORT_JOINTS=True,
    NODE_PARENTING=True,
    IMPORT_SIMPLEANIMS=True,
    IMPORT_MATERIALS=True,
    IMPORT_TEXTURES=True,
)


# import settings for both full animations and simple animations
@dataclass
class AnimationImportSettings:
    FRAMERATE: int
    # INTERPOLATION_TYPE linear? bezier?
    POSITION_UNIT_MULTIPLIER: float


AnimationSettings = AnimationImportSettings(FRAMERATE=30, POSITION_UNIT_MULTIPLIER=0.01)
