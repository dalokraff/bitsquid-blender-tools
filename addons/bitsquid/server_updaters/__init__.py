from fastapi import Depends
from sqlalchemy.orm import Session
from bitsquid.murmur.murmurhash64 import hash64_inverse
from bitsquid.server_updaters.dependancy import get_db
from bitsquid.server_updaters.unit.model import UnitModel
from bitsquid.server_updaters.unit.schema import Unit, UnitCreate

def add_unit_to_db(obj):
    """
    run database unit create model
    """
    db = next(get_db())
    inv_hex_hash = hash64_inverse(obj['IDString64'].lower(), returnHex=True)
    pos = obj.matrix_world.to_translation()
    rot = obj.matrix_world.to_quaternion()
    scale = obj.matrix_world.to_scale()
    unit_initializer = UnitCreate(
        pos_x=pos.x,
        pos_y=pos.y,
        pos_z=pos.z,
        rot_w=rot.w,
        rot_x=rot.x,
        rot_y=rot.y,
        rot_z=rot.z,
        scale_x=scale.x,
        scale_y=scale.y,
        scale_z=scale.z,
        inv_hex_hash=inv_hex_hash
    )
    print('Adding new unit', obj['IDString64'])
    print(*unit_initializer)
    unit = UnitModel(unit_initializer)
    unit.save(db)
    obj['bitsquid_db_id'] = unit.id

def update_unit_in_db(obj, scene):
    """
    run database update model
    """
    if 'bitsquid_db_id' in obj:
        unit_name = obj['IDString64']
        bitsquid_id = obj['bitsquid_db_id']
        print(f'Updating unit {unit_name} with ID: {bitsquid_id}')
        db = next(get_db())
        pos = obj.matrix_world.to_translation()
        rot = obj.matrix_world.to_quaternion()
        scale = obj.matrix_world.to_scale()
        unit_initializer = UnitCreate(
            pos_x=pos.x,
            pos_y=pos.y,
            pos_z=pos.z,
            rot_w=rot.w,
            rot_x=rot.x,
            rot_y=rot.y,
            rot_z=rot.z,
            scale_x=scale.x,
            scale_y=scale.y,
            scale_z=scale.z,
            inv_hex_hash='dummy value'
        )
        unit = UnitModel.find_unit_by_id(int(bitsquid_id), db)
        unit.update_spatial_coords(
            unit_init=unit_initializer,
            db=db
        )

def get_all_unit_ids():
    """
    retrieves all unit ids in db
    """
    db = next(get_db())
    unit_id_list = UnitModel.get_all_unit_ids(db)
    return unit_id_list

def remove_deleted_units(unit_id_list: list[int]):
    """
    deletes the given unit ids
    """
    db = next(get_db())
    for id in unit_id_list:
        UnitModel.delete_unit_by_id(id, db)
        print(f'Unit {id} deleted')

def mark_deleted_units(unit_id_list):
    """
    marks units with the given ids for deletion
    """
    db = next(get_db())
    for id in unit_id_list:
        unit = UnitModel.find_unit_by_id(id, db)
        unit.mark_for_deletion(db)
        print(f'Unit {id} marked for deletion')
