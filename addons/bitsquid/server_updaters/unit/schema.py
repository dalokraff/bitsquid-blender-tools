from pydantic import BaseModel

class UnitBase(BaseModel):
    pos_x: float
    pos_y: float
    pos_z: float
    rot_w: float
    rot_x: float
    rot_y: float
    rot_z: float
    scale_x: float
    scale_y: float
    scale_z: float
    inv_hex_hash: str

class UnitCreate(UnitBase):
    pass

class Unit(UnitBase):
    id: int
    decimal_hash: list[int]
    class Config:
        orm_mode = True