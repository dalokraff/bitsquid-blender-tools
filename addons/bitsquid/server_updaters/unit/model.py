import time
from sqlalchemy import ARRAY, Boolean, Column, ForeignKey, Integer, Float, PickleType, String
from sqlalchemy.orm import Session

from bitsquid.server.database import Base
from bitsquid.server_updaters.unit.schema import UnitCreate


class UnitModel(Base):
    """
    """
    __tablename__ = "units"

    id = Column(Integer, primary_key=True, index=True)
    pos_x = Column(Float, index=True)
    pos_y = Column(Float, index=True)
    pos_z = Column(Float, index=True)

    rot_w = Column(Float, index=True)
    rot_x = Column(Float, index=True)
    rot_y = Column(Float, index=True)
    rot_z = Column(Float, index=True)

    scale_x = Column(Float, index=True)
    scale_y = Column(Float, index=True)
    scale_z = Column(Float, index=True)

    inv_hex_hash = Column(String, index=True)

    last_updated = Column(Float, index=True)

    deleted = Column(Boolean, index=True)

    decimal_hash = Column(PickleType)

    def __init__(self, unit_init: UnitCreate):
        self.pos_x = unit_init.pos_x
        self.pos_y = unit_init.pos_y
        self.pos_z = unit_init.pos_z

        self.rot_w = unit_init.rot_w
        self.rot_x = unit_init.rot_x
        self.rot_y = unit_init.rot_y
        self.rot_z = unit_init.rot_z

        self.scale_x = unit_init.scale_x
        self.scale_y = unit_init.scale_y
        self.scale_z = unit_init.scale_z

        self.inv_hex_hash = unit_init.inv_hex_hash
        self.last_updated = time.time()
        self.deleted = False

        self.decimal_hash = [int(unit_init.inv_hex_hash[x:x+2],16) for x in range(0,16,2)]

    def save(self, db: Session):
        """
        adds a point to the database
        """

        db.add(self)
        db.commit()
        db.refresh(self)

        return self

    def update_spatial_coords(self, unit_init: UnitCreate, db: Session):
        self.pos_x = unit_init.pos_x
        self.pos_y = unit_init.pos_y
        self.pos_z = unit_init.pos_z

        self.rot_w = unit_init.rot_w
        self.rot_x = unit_init.rot_x
        self.rot_y = unit_init.rot_y
        self.rot_z = unit_init.rot_z

        self.scale_x = unit_init.scale_x
        self.scale_y = unit_init.scale_y
        self.scale_z = unit_init.scale_z

        self.last_updated = time.time()

        self.save(db)

    def mark_for_deletion(self, db: Session):
        self.deleted = True
        self.save(db)

    @staticmethod
    def find_unit_by_id(unit_id: int, db: Session):
        """Find UnitModel by database id"""
        query = db.query(UnitModel)
        query_filter = query.filter(UnitModel.id == unit_id)
        results = query_filter.first()
        return results

    @staticmethod
    def get_updated_between(time_after: float, time_before: float, db: Session):
        """
        queries all units updated between time_before and time_after
        """
        query = db.query(UnitModel)
        results = query.filter(UnitModel.last_updated > time_after, UnitModel.last_updated < time_before)
        return results.all()

    @staticmethod
    def get_all(db: Session):
        return db.query(UnitModel).all()

    @staticmethod
    def get_all_non_deleted(db: Session):
        """
        returns all units not marked for deletion
        """
        query = db.query(UnitModel)
        results = query.filter(UnitModel.deleted == False).all()
        return results

    @staticmethod
    def get_all_unit_ids(db: Session):
        all_units = db.query(UnitModel).all()
        result = [x.id for x in all_units]
        return result

    @staticmethod
    def delete_unit_by_id(unit_id: int, db: Session):
        """
        deletes units by their id
        """
        unit = UnitModel.find_unit_by_id(unit_id, db)
        db.delete(unit)
        db.commit()

    @staticmethod
    def get_marked_units(db: Session):
        """
        gets all units marked for deletion
        """
        query = db.query(UnitModel)
        marked_units = query.filter(UnitModel.deleted == True).all()
        return marked_units

    @staticmethod
    def delete_marked_units(db: Session):
        """
        deletes all units that have been marked for deletion
        """
        marked_units = UnitModel.get_marked_units(db)
        deleted_ids = []
        for unit in marked_units:
            deleted_ids.append(unit.id)
            UnitModel.delete_unit_by_id(unit.id, db)

        return deleted_ids