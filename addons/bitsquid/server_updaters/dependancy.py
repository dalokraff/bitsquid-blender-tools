from sqlalchemy.orm import Session
from bitsquid.server.database import SessionLocal

# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()