"""
Import compiled animation state machines (.state_machine)
"""

import bitsquid.config as c
import bitsquid.stingray as s
import bitsquid.deserializing as d
from bitsquid.murmur.rainbow_table import IDString64


# stingray::Array<stingray::AnimationBlendSettings>::serialize
def AnimationBlendSettingsArrayDeserializer(
    self, data: bytes
) -> list[s.AnimationBlendSettings]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.AnimationBlendSettings()
        item.To = d.ReadUint32(data)
        item.Blend = d.ReadUint32(data)
        item.Mode = d.ReadUint32(data)
        item.OnBeat = d.ReadIDString32(data)

        item.unk = d.ReadUint32(data)
        result.append(item)
    return result


# stingray::Vector<stingray::AnimationTransition>::serialize
def AnimationTransitionArrayDeserializer(
    self, data: bytes
) -> list[s.AnimationTransition]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.AnimationTransition()
        item.Event = d.ReadIDString32(data)
        item.Index = d.ReadUint32(data)
        item.IsConditionalTransition = d.ReadBool(data)
        result.append(item)
    return result


# stingray::Array<stingray::AnimationTransitionSwitchExit>::serialize
def AnimationTransitionSwitchExitArrayDeserializer(
    self, data: bytes
) -> list[s.AnimationTransitionSwitchExit]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.AnimationTransitionSwitchExit()
        item.BlendSettingsIndex = d.ReadUint32(data)
        item.IntervalStart = d.ReadUint32(data)
        item.IntervalEnd = d.ReadUint32(data)
        item.ExcludeStart = d.ReadBool(data)
        item.ExcludeEnd = d.ReadBool(data)
        result.append(item)
    return result


# stingray::Vector<stingray::AnimationTransitionSwitch>::serialize
def AnimationTransitionSwitchArrayDeserializer(
    self, data: bytes
) -> list[s.AnimationTransitionSwitch]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.AnimationTransitionSwitch()
        item.Bytecode = d.ReadUint32(data)
        item.Exits = AnimationTransitionSwitchExitArrayDeserializer(self, data)
        result.append(item)
    return result


# stingray::Vector<stingray::AnimationState::Marker>::serialize
def MarkerArrayDeserializer(self, data: bytes) -> list[s.Marker]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.Marker()
        item.Time = d.ReadSingle(data)
        item.Name = d.ReadIDString32(data)
        item.Type = s.MarkerType(d.ReadUint32(data))
        result.append(item)
    return result


# stingray::AnimationState::serialize
def AnimationStateDeserializer(self, data: bytes) -> s.AnimationState:
    result = s.AnimationState()
    result.Name = d.ReadIDString64(data)

    result.name2 = d.ReadIDString64(data)
    result.name3 = d.ReadIDString64(data)

    result.StateType = s.AnimationStateType(d.ReadUint32(data))
    result.Animations = d.ReadIDString64Array(data)
    result.Probabilities = d.ReadFloatArray(data)
    result.RandomizationType = s.RandomizationType(d.ReadUint32(data))
    result.LoopAnimation = d.ReadBool(data)
    result.BlendType = s.BlendType(d.ReadUint32(data))
    result.Transitions = AnimationTransitionArrayDeserializer(self, data)
    result.BlendSettings = AnimationBlendSettingsArrayDeserializer(self, data)
    result.Switches = AnimationTransitionSwitchArrayDeserializer(self, data)
    result.Timeline = MarkerArrayDeserializer(self, data)

    result.unk1 = d.ReadUint32(data)
    result.unk2_arr = d.ReadVector3Array(data)

    result.ExitEvent = s.ExitEvent()
    result.ExitEvent.Name = d.ReadIDString32(data)
    result.ExitEvent.BlendTime = d.ReadSingle(data)
    result.Bytecode = d.ReadIntArray(data)
    result.Weights = d.ReadIntArray(data)

    result.unk3 = d.ReadUint32(data)

    result.Speed = d.ReadSingle(data)

    result.unk4 = d.ReadUint32(data)

    # result.RootDriving = s.RootMode(d.ReadUint32(data))
    # result.BoneAnimMode = s.BoneMode(d.ReadUint32(data))
    result.BlendSet = d.ReadUint32(data)
    result.Constraints = d.ReadIntArray(data)

    result.unk6 = d.ReadUint32(data)

    # result.TimeVariable = d.ReadUint32(data)
    result.MutedLayersMask = d.ReadUint32(data)
    result.Ragdoll = d.ReadUint32(data)
    return result


def AnimationStateArrayDeserializer(self, data: bytes) -> list[s.AnimationState]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        result.append(AnimationStateDeserializer(self, data))
    return result


# stingray::Vector<stingray::AnimationLayer>::serialize
def AnimationLayerArrayDeserializer(self, data: bytes) -> list[s.AnimationLayer]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.AnimationLayer()
        item.States = AnimationStateArrayDeserializer(self, data)
        item.DefaultState = d.ReadUint32(data)
        result.append(item)
    return result


# stingray::Vector<stingray::BlendSet>::serialize
def BlendSetArrayDeserializer(self, data: bytes) -> list[s.BlendSet]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.BlendSet()
        item.BoneWeights = d.ReadFloatArray(data)

        item.unk_arr = d.ReadIntArray(data)
        item.unk_bool = d.ReadBool(data)

        result.append(item)
    return result


# stingray::Vector<stingray::Ragdoll>::serialize
def RagdollArrayDeserializer(self, data: bytes) -> list[s.Ragdoll]:
    result = []
    count = d.ReadUint32(data)
    for _ in range(count):
        item = s.Ragdoll()
        item.DynamicActors = d.ReadIDString32Array(data)
        item.KeyframedActors = d.ReadIDString32Array(data)
        result.append(item)
    return result


# stingray::AnimationStateMachineResource::serialize
def AnimationStateMachineResourceDeserializer(
    self, data: bytes
) -> s.AnimationStateMachineResource:
    result = s.AnimationStateMachineResource()
    result.Layers = AnimationLayerArrayDeserializer(self, data)
    result.EventNames = d.ReadIDString32Array(data)
    result.VariableNames = d.ReadIDString32Array(data)
    result.Variables = d.ReadFloatArray(data)

    result.unk1_arr = []
    count = d.ReadUint32(data)
    for _ in range(count):
        result.unk1_arr.append(d.ReadSingle(data))
        result.unk1_arr.append(d.ReadSingle(data))

    result.BlendSets = BlendSetArrayDeserializer(self, data)
    result.ConstraintTargets = d.ReadIDString32Array(data)
    result.ConstraintTargetPositions = d.ReadVector3Array(data)
    result.Constraints = d.ReadByteArray(data)
    result.Ragdolls = RagdollArrayDeserializer(self, data)

    count = d.ReadUint32(data)
    result.unk3_arr = []
    for _ in range(count):
        item = s.UnkStructOf7()
        item.unk1 = d.ReadUint32(data)
        item.unk2 = d.ReadUint32(data)
        item.unk3 = d.ReadUint32(data)
        item.unk4 = d.ReadUint32(data)
        item.unk5 = d.ReadUint32(data)
        item.unk6 = d.ReadUint32(data)
        item.unk7 = d.ReadSingle(data)
        result.unk3_arr.append(item)

    result.unk4 = d.ReadSingle(data)

    if c.offset != len(data):
        raise Exception(
            f"Finished deserializing at offset {c.offset}, but file length is {len(data)}"
        )

    return result


def load(self, path: str) -> s.AnimationStateMachineResource:
    c.offset = 0
    with open(path, "rb") as f:
        data = f.read()
    state_machine_res = AnimationStateMachineResourceDeserializer(self, data)

    return state_machine_res


def try_get_animations_list(self, path: str) -> list[IDString64]:
    """
    Read an animation state machine and return a list of the animations it references.
    """
    try:
        state_machine_res = load(self, path)
        result = []
        for layer in state_machine_res.Layers:
            for state in layer.States:
                for animation in state.Animations:
                    result.append(animation)
        return list(set(result))
    except Exception as e:
        self.report({"WARNING"}, f"Could not load animation state machine {path}: {e}")
        return []
