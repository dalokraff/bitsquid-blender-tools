"""
Hold methods to do bulk import tests
"""

import os
from datetime import datetime
from time import perf_counter
from math import floor
from statistics import mean
import glob
import bpy
from bitsquid import config
from bitsquid.unit import export as export_unit, import_compiled as import_compiled_unit
from bitsquid.animation import import_compiled as import_compiled_animation
from bitsquid.bones import import_compiled as import_compiled_bones
from bitsquid.state_machine import import_compiled as import_compiled_state_machine
from bitsquid.material import (
    export as export_material,
    import_compiled as import_compiled_material,
    export_compiled as export_compiled_material,
)


# https://blender.stackexchange.com/a/252097
# https://blender.stackexchange.com/a/102442
def deleteAllObjects():
    """
    Deletes all objects in the current scene
    """
    for obj in bpy.data.objects:
        obj.select_set(True)
        if obj.mode != "OBJECT":
            bpy.context.view_layer.objects.active = obj
            bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
    bpy.ops.object.delete()
    # clear unused data blocks
    bpy.ops.outliner.orphans_purge(
        do_local_ids=True, do_linked_ids=True, do_recursive=True
    )


# https://blender.stackexchange.com/a/192887
def mega_purge():
    orphan_ob = [o for o in bpy.data.objects if not o.users]
    while orphan_ob:
        bpy.data.objects.remove(orphan_ob.pop())

    orphan_mesh = [m for m in bpy.data.meshes if not m.users]
    while orphan_mesh:
        bpy.data.meshes.remove(orphan_mesh.pop())

    orphan_mat = [m for m in bpy.data.materials if not m.users]
    while orphan_mat:
        bpy.data.materials.remove(orphan_mat.pop())

    def purge_node_groups():
        orphan_node_group = [g for g in bpy.data.node_groups if not g.users]
        while orphan_node_group:
            bpy.data.node_groups.remove(orphan_node_group.pop())
        if [g for g in bpy.data.node_groups if not g.users]:
            purge_node_groups()

    purge_node_groups()

    orphan_texture = [t for t in bpy.data.textures if not t.users]
    while orphan_texture:
        bpy.data.textures.remove(orphan_texture.pop())

    orphan_images = [i for i in bpy.data.images if not i.users]
    while orphan_images:
        bpy.data.images.remove(orphan_images.pop())

    orphan_cameras = [c for c in bpy.data.cameras if not c.users]
    while orphan_cameras:
        bpy.data.cameras.remove(orphan_cameras.pop())


def dump_idstrings(directory: str):
    strings64 = set([f"{ID:016X}" for ID in config.seen_idstring64])
    strings32 = set([f"{ID:08X}" for ID in config.seen_idstring32])
    outfile64 = os.path.join(directory, "idstring64_dump.txt")
    outfile32 = os.path.join(directory, "idstring32_dump.txt")
    with open(outfile64, "w") as f:
        for ID in strings64:
            f.write(f"{ID}\n")
    with open(outfile32, "w") as f:
        for ID in strings32:
            f.write(f"{ID}\n")


def bones_inner(*args, **kwargs):
    self = args[0]
    file = args[2]
    result = import_compiled_bones.load(self, file)
    return result


def unit_inner(*args, **kwargs):
    self = args[0]
    context = args[1]
    file = args[2]
    result = import_compiled_unit.load(self, context, file, kwargs)
    return result


def animation_inner(*args, **kwargs):
    # only tests animation deserializing, not importing into blender
    self = args[0]
    file = args[2]
    import_compiled_animation.load(self, file, **kwargs)


def material_inner(*args, **kwargs):
    self = args[0]
    file = args[2]
    directory = os.path.dirname(file)
    mat_res = import_compiled_material.load(self, file, **kwargs)
    import_compiled_material.import_to_blender(self, mat_res, directory, slot_name=None)
    if mat_res:
        export_compiled_material.export(self, mat_res, f"{file}.decompiled")
    return mat_res


def state_machine_inner(*args, **kwargs):
    self = args[0]
    file = args[2]
    result = import_compiled_state_machine.load(self, file, **kwargs)
    return result


def bulk_test(self, context, filetype: str, **kwargs):
    # customize unit import settings
    config.CompiledUnitSettings = config.CompiledUnitImportSettings(
        IMPORT_MESHES=True,
        IMPORT_CUSTOM_NORMALS=False,
        IMPORT_BLEND_WEIGHTS=False,
        IMPORT_VERTEX_COLORS=False,
        IMPORT_TEXCOORDS=False,
        FLIP_UVS=False,
        IMPORT_EMPTIES=True,
        IMPORT_JOINTS=True,
        NODE_PARENTING=True,
        IMPORT_SIMPLEANIMS=False,
        IMPORT_MATERIALS=False,
        IMPORT_TEXTURES=False,
    )

    # customize animation import settings
    config.AnimationSettings = config.AnimationImportSettings(
        FRAMERATE=30, POSITION_UNIT_MULTIPLIER=0.01
    )

    # customize global import settings
    config.GlobalSettings = config.GlobalImportSettings(LOOKUP_IDSTRINGS=True)

    # fake user for Stingray Standard material so it doesn't get lost
    bpy.data.materials["Stingray Standard"].use_fake_user = True

    # change `testdir` to a folder containing lots of extracted files
    testdir = "F:\\Games\\steamapps\\common\\Warhammer Vermintide 2\\bundle\\extract"

    # open output file
    with open(os.path.join(testdir, f"{filetype}_test_output.log"), "w") as o:
        # log start time
        now = datetime.now()
        dt_string = now.strftime("%Y-%m-%d %H:%M:%S")
        o.write(f"Bulk .{filetype} import test started at {dt_string}\n")
        # get list of files, recursively
        o.write(f"Test directory: {testdir}\n")
        o.write(f"Looking for {filetype} files (might take a while)...\n")
        o.flush()
        test_files = []
        for filename in glob.iglob(f"{testdir}/**/*.{filetype}", recursive=True):
            test_files.append(os.path.abspath(filename))
        o.write(f"Found {len(test_files)} {filetype} files\n")
        # if there are no files of the required type, we have nothing to do
        if len(test_files) == 0:
            return {"FINISHED"}
        # clear blender objects
        deleteAllObjects()
        mega_purge()
        # list of files we've already looked at
        files_seen = []

        o.write("Importing files now...\n")
        o.flush()
        overall_start = perf_counter()
        i = 0
        old_percent = 0
        success_count = 0
        unique_count = 0
        file_times = []
        for file in test_files:
            file_time_start = perf_counter()
            percent_done = floor((i / len(test_files)) * 100)
            if percent_done > old_percent:
                o.write(f"{i} / {len(test_files)} ({percent_done:02d}%) done\n")
                o.flush()
            basename = os.path.basename(file)
            # skip duplicate files
            if basename in files_seen:
                i += 1
                old_percent = percent_done
                continue
            files_seen.append(basename)
            unique_count += 1
            try:
                match filetype:
                    case "unit":
                        unit_inner(self, context, file, **kwargs)
                    case "animation":
                        animation_inner(self, context, file, **kwargs)
                    case "material":
                        material_inner(self, context, file, **kwargs)
                    case "state_machine":
                        state_machine_inner(self, context, file, **kwargs)
                    case "bones":
                        bones_inner(self, context, file, **kwargs)

            except (AssertionError, TypeError, Exception) as e:
                o.write(f"{file} {type(e)} {e}\n")

            else:
                success_count += 1

            o.flush()
            mega_purge()
            deleteAllObjects()
            i += 1
            # assert success_count == unique_count
            old_percent = percent_done
            file_time_stop = perf_counter()
            file_times.append(file_time_stop - file_time_start)

        overall_stop = perf_counter()
        overall_time = round(overall_stop - overall_start)
        o.write(f"Finished testing {i} {filetype} files in {overall_time} seconds\n")
        o.write(f"Out of {i} files, {unique_count} were unique\n")
        success_rate = floor(100 * success_count / unique_count)
        o.write(
            f"Successfully imported {success_count} / {unique_count} ({(success_rate)}%) files\n"
        )
        average_file_time = mean(file_times)
        o.write(f"Average file processing time: {average_file_time:.4f} seconds\n")

    if config.dump_idstrings:
        dump_idstrings(testdir)
