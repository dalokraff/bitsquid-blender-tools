"""
Lots of classes, enums, functions to help with Stingray file data
"""

from enum import Enum, Flag, auto
from mathutils import Vector, Matrix, Quaternion
from typing import Optional
import struct
import math
import bpy
from bpy_extras.io_utils import unpack_list  # replace more list flattening with this?
import bitsquid.config as config
from bitsquid.murmur.rainbow_table import IDString32, IDString64

# from bitsquid.config import resource_map

# uint32
Validity = Enum("Validity", ["Static", "Updatable", "Dynamic"], start=0)

# uint32
# stingray::geometry::StreamType
StreamType = Enum("StreamType", ["Array", "Raw"], start=0)

# uint32
# stingray::geometry::VertexComponent
VertexComponent = Enum(
    "VertexComponent",
    [
        "Position",
        "Normal",
        "Tangent",
        "Binormal",
        "Texcoord",
        "Color",
        "BlendIndices",
        "BlendWeights",
        "Unknown",
    ],
    start=0,
)

# uint32
# stingray::geometry::ChannelType
ChannelType = Enum(
    "ChannelType",
    [
        "Float1",
        "Float2",
        "Float3",
        "Float4",
        "UByte4",
        "Short1",
        "Short2",
        "Short3",
        "Short4",
        "Int1",
        "Int2",
        "Int3",
        "Int4",
        "Float3_CMP_11_11_10",
        "Half1",
        "Half2",
        "Half3",
        "Half4",
        "UByte4_NORM",
        "UInt1",
        "Unknown",
    ],
    start=0,
)

# uint32
# stingray::IndexStream::Format
IndexFormat = Enum("IndexFormat", ["Index16", "Index32"], start=0)

# uint32
# enum stingray::ActorDesc:: unnamed enum
ShapeType = Enum(
    "ShapeType",
    [
        "SphereShape",
        "BoxShape",
        "CapsuleShape",
        "MeshShape",
        "ConvexShape",
        "HeightFieldShape",
        "PlaneShape",
    ],
    start=0,
)

# uint32
# stingray::JointDesc
JointType = Enum(
    "JointType",
    [
        "Spherical",
        "Revolute",
        "Prismatic",
        "Cylindrical",
        "Fixed",
        "Distance",
        "PointInPlane",
        "PointOnLine",
    ],
    start=0,
)


# enum stingray::interleaved_animation:: unnamed enum
class InterleavedAnimationItemType(Enum):
    HEADER_ITEM = 0
    SYNC_ITEM = 1
    TRIGGER_ITEM = 2
    FOOTER_ITEM = 3
    UNPACKED_POSITION_KEY_ITEM = 4
    UNPACKED_ROTATION_KEY_ITEM = 5
    UNPACKED_SCALE_KEY_ITEM = 6
    UNPACKED_SYNC_ITEM = 7
    SCALE_KEY_ITEM = 0x4000
    POSITION_KEY_ITEM = 0x8000
    ROTATION_KEY_ITEM = 0xC000


# TODO this can be removed?
def GetSize(indexFormat):
    match indexFormat:
        case indexFormat.Index16:
            return 2
        case indexFormat.Index32:
            return 4
        case _:
            raise Exception(f"{indexFormat}")


# ushort
ParentType = Enum("ParentType", ["None", "Internal", "External", "Unlinked"], start=0)


# uint
class RenderableFlags(Flag):
    ViewportVisible = auto()  # 0x1
    ShadowCaster = auto()  # 0x2
    CullingAlwaysVisible = auto()  # 0x4
    CullingDisabled = auto()  # 0x8
    Occluder = auto()  # 0x10
    SurfaceQueries = auto()  # 0x20
    StaticShadowCaster = auto()  # 0x40
    UmbraTarget = auto()  # 0x80
    AlwaysKeep = auto()  # 0x100
    UmbraV2 = auto()  # 0x200
    DoubleSided = auto()  # 0x400
    UmbraOccluder = auto()  # 0x800
    ParticleMeshSpawning = auto()  # 0x1000


class Channel:
    Component: VertexComponent
    Type: ChannelType
    Set: int
    Stream: int
    IsInstance: int  # actually a byte


class VertexBuffer:
    Data: bytes  # byte array
    Validity: Validity
    StreamType: StreamType
    Count: int
    Stride: int
    Channel: Channel

    def data_V4H_to_Vec3(self, bself) -> list[Vector]:
        """
        Used for vertex positions and normals, since they both appear to be
        in the same format
        """
        # This covers the biggest I've seen (so far)
        MAXIMUM_FLOAT_SIZE = 3000
        # this function should only be called if the VertexBuffer channel type is Half4
        assert self.Channel.Type == ChannelType.Half4
        # `Data` is array of Vector4Half (4*2=8 bytes each) (XYZW) (half precision floats)
        # We will convert to array of Vector3 (3*4=12 bytes each) (XYZ, discard W) (single precision floats)
        # make sure `Data` is multiple of 8 bytes
        assert len(self.Data) % (4 * 2) == 0
        temp = struct.iter_unpack("<eeee", self.Data)
        result = [V4H_to_Vec3(vec) for vec in temp]
        for vec in result:
            for f in vec:
                # Floats shouldn't be infinite
                if math.isinf(f):
                    raise Exception("Float is infinite")

                if not (-MAXIMUM_FLOAT_SIZE <= f <= MAXIMUM_FLOAT_SIZE):
                    raise Exception(
                        f"Float {f} not within range [{-MAXIMUM_FLOAT_SIZE}:{MAXIMUM_FLOAT_SIZE}]"
                    )

        return result

    def get_positions(self, bself) -> list[Vector]:
        # Vector4Half -> Vector3
        assert self.Channel.Component == VertexComponent.Position
        return self.data_V4H_to_Vec3(bself)

    def get_normals(self, bself) -> list[Vector]:
        # Vector4Half -> Vector3
        assert self.Channel.Component == VertexComponent.Normal
        return self.data_V4H_to_Vec3(bself)

    def get_tangents(self, bself) -> list[Vector]:
        # Vector4Half -> Vector4
        # Don't need to do any float type conversion; floats in python are all the same
        assert (
            self.Channel.Component == VertexComponent.Tangent
            and self.Channel.Type == ChannelType.Half4
        )
        assert len(self.Data) % (4 * 2) == 0
        temp = struct.iter_unpack("<eeee", self.Data)
        result = [Vector(vec) for vec in temp]
        return result

    def get_texcoords(self, bself) -> list[float]:
        # Vector2Half -> Vector2
        assert (
            self.Channel.Component == VertexComponent.Texcoord
            and self.Channel.Type == ChannelType.Half2
        )
        assert len(self.Data) % (2 * 2) == 0
        temp = struct.iter_unpack("<ee", self.Data)
        result = [item for tup in temp for item in tup]

        if config.CompiledUnitSettings.FLIP_UVS:
            # flip UV in Y axis
            for i in range(1, len(result) - 1, 2):
                result[i] = 1 - result[i]

        return result

    def get_colors(
        self, bself
    ) -> tuple[list[float], ChannelType] | tuple[list[int], ChannelType]:
        # can be in UByte4_NORM, Float3, or Half4?
        # return tuple of result and ChannelType
        assert self.Channel.Component == VertexComponent.Color
        CT = self.Channel.Type
        match CT:
            case ChannelType.UByte4_NORM:
                # byte color
                assert len(self.Data) % (4 * 1) == 0
                temp = struct.iter_unpack("<BBBB", self.Data)
            case ChannelType.Float3:
                # float color
                assert len(self.Data) % (3 * 4) == 0
                temp = struct.iter_unpack("<fff", self.Data)
            case ChannelType.Half4:
                # also float color?
                assert len(self.Data) % (4 * 2) == 0
                temp = struct.iter_unpack("<eeee", self.Data)
            case _:
                raise Exception(f"Unknown channel type {self.Channel.Type}")
        # result arrays must be unidimensional for bpy
        # flattens list of tuples, how wacky
        result = [item for tup in temp for item in tup]
        return (result, CT)

    def get_blendindices(self, bself) -> list[list[int]]:
        # Vector of 4 bytes -> list[int]
        assert (
            self.Channel.Component == VertexComponent.BlendIndices
            and self.Channel.Type == ChannelType.UInt1
        )
        assert self.Stride == 4
        assert len(self.Data) == self.Stride * self.Count
        temp = struct.iter_unpack("<BBBB", self.Data)
        # result = [item for tup in temp for item in tup]
        result = [list(tup) for tup in temp]
        return result

    def get_blendweights(self, bself) -> list[list[float]]:
        # Vector4Half -> list[float]
        assert (
            self.Channel.Component == VertexComponent.BlendWeights
            and self.Channel.Type == ChannelType.Half4
        )
        assert self.Stride == 8
        assert len(self.Data) == self.Stride * self.Count
        temp = struct.iter_unpack("<eeee", self.Data)
        # result = [item for tup in temp for item in tup]
        result = [list(tup) for tup in temp]
        return result


class IndexBuffer:
    Validity: Validity
    StreamType: StreamType
    IndexFormat: IndexFormat
    IndexCount: int
    Data: bytes

    def get_indices(self, bself) -> list[int]:
        bself.report(
            {"DEBUG"},
            f"Getting indices: validity={self.Validity}, streamtype={self.StreamType}, indexformat={self.IndexFormat}, indexcount={self.IndexCount}",
        )
        if self.IndexFormat == IndexFormat.Index32:  # index size is 4
            assert len(self.Data) % 4 == 0
            # luv me list comprehension
            temp = [self.Data[i : i + 4] for i in range(0, len(self.Data), 4)]
        elif self.IndexFormat == IndexFormat.Index16:  # index size is 2
            assert len(self.Data) % 2 == 0
            temp = [self.Data[i : i + 2] for i in range(0, len(self.Data), 2)]
        else:
            raise Exception("IndexFormat size was not 2 or 4")

        indices = [int.from_bytes(i, "little", signed=False) for i in temp]
        # make sure we have the correct number of indices
        assert len(indices) == self.IndexCount
        # make sure indices are not negative
        for index in indices:
            assert 0 <= index
        # turn into list of lists of 3 ints
        indices = [indices[i : i + 3] for i in range(0, len(indices), 3)]
        return indices


class BatchRange:
    MaterialIndex: int
    Start: int  # Per-triangle; multiply by 3 to get the index.
    Size: int  # Per-triangle; multiply by 3 to get the index.
    BoneSet: int

    def get_face_indices(self) -> list[int]:
        faces = list(range(self.Start, self.Start + self.Size, 1))
        return faces


class BoundingVolume:
    LowerBounds: Vector  # Vec3
    UpperBounds: Vector  # Vec3
    Radius: float


class SkinData:
    InvBindMatrices: list[Matrix]  # Matrix4x4
    NodeIndices: list[int]
    MatrixIndexSets: list[list[int]]

    def GetJointsBelongingToSet(self, boneset_index: int) -> list[int]:
        # TODO what does this mean??
        # returns list of tuples of (joint node index, node IBM)
        if boneset_index > len(self.MatrixIndexSets):
            raise Exception(
                f"Out of range: index_set {boneset_index} > {len(self.MatrixIndexSets)}"
            )
        set_indices = self.MatrixIndexSets[boneset_index]
        node_indices = [self.NodeIndices[i] for i in set_indices]
        node_IBMs = [self.InvBindMatrices[i] for i in set_indices]
        # return list(zip(node_indices, node_IBMs))
        # we don't need inverse bind matrices for blender?
        return node_indices


def find_material_by_idstring32(
    self, idstring: IDString32
) -> Optional[bpy.types.Material]:
    for mat in bpy.data.materials:
        if "IDString32" in mat.keys():
            if mat["IDString32"] == str(idstring):
                return mat
    return None


class MeshGeometry:
    VertexBuffers: list[VertexBuffer]
    IndexBuffer: IndexBuffer
    BatchRanges: list[BatchRange]
    BoundingVolume: BoundingVolume
    Materials: list[IDString32]

    # added to make mesh creation easier
    BlendWeights: list[list[float]]
    BlendIndices: list[list[int]]

    def create_blender_mesh(self, bself, name: str) -> bpy.types.Object:
        # get the starting index and number of vertices in the batch
        # vert_start = self.BatchRanges[batch_index].Start
        # vert_count = self.BatchRanges[batch_index].Size
        # get list of vertices
        vertex_buffers = {}
        face_buffer: list[int] = self.IndexBuffer.get_indices(bself)
        for vbuffer in self.VertexBuffers:
            match vbuffer.Channel.Component.name:
                case "Color":
                    # most meshes don't have vertex colors
                    vertex_buffers["colors"] = vbuffer.get_colors(bself)
                case "Position":
                    # Vertex coords
                    vertex_buffers["positions"] = vbuffer.get_positions(bself)
                case "Normal":
                    # Custom normal data
                    vertex_buffers["normals"] = vbuffer.get_normals(bself)
                case "Binormal":
                    # we don't need this for blender?
                    continue
                case "Tangent":
                    # we don't need this for blender?
                    continue
                    # vertex_buffers["tangents"] = vbuffer.get_tangents(bself)
                case "Texcoord":
                    uv_data = vbuffer.get_texcoords(bself)
                    # we have to do something funny to texcoord data after getting it
                    uv_defs = [uv_data[j : j + 2] for j in range(0, len(uv_data), 2)]
                    # flatten index stream
                    index_stream = [index for face in face_buffer for index in face]
                    # flatten the final result
                    temp = unpack_list(
                        [uv_defs[index_stream[j]] for j in range(0, len(index_stream))]
                    )
                    vertex_buffers["texcoords"] = temp
                case "BlendWeights":
                    self.BlendWeights = vbuffer.get_blendweights(bself)
                case "BlendIndices":
                    self.BlendIndices = vbuffer.get_blendindices(bself)
                case "Unknown":
                    # bself.report({'WARNING'}, f"Unknown vertex buffer channel component: {vbuffer.Channel.Component.name}")
                    raise Exception(
                        f"Unknown vertex buffer channel component: {vbuffer.Channel.Component.name}"
                    )
                case _:
                    # If the component name did not match one of the enumerated values, it
                    # should have already been caught
                    raise Exception("This should not happen")
        # now we have all the vertex buffer data
        # all vertex buffers should be the same length
        lengths = [buf.Count for buf in self.VertexBuffers]
        for count in lengths:
            assert count == lengths[0]

        # make sure we're in object mode
        # bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
        mesh = bpy.data.meshes.new(name)
        mesh.from_pydata(vertex_buffers["positions"], [], face_buffer)
        mesh.update()

        # check if mesh is valid. the check also 'fixes' the mesh, so save a backup
        # check output of validation in Blender: Window -> Toggle System Console
        backup_mesh = mesh.copy()
        mesh_invalid = mesh.validate(verbose=True)
        if mesh_invalid:
            # raise Exception(f"Mesh {name} was invalid")
            bself.report({"WARNING"}, f"Mesh {name} is invalid")
            mesh = backup_mesh
        else:
            bself.report({"INFO"}, f"Created mesh {name}")

        # if we have vertex color data
        if "colors" in vertex_buffers.keys():
            bself.report({"DEBUG"}, f"Custom vertex colors found for mesh {name}")
            if config.CompiledUnitSettings.IMPORT_VERTEX_COLORS:
                CT = vertex_buffers["colors"][1]
                # get correct attribute format
                match CT:
                    case ChannelType.UByte4_NORM:
                        colattr = mesh.color_attributes.new(
                            name="VertexBufferColor", type="BYTE_COLOR", domain="POINT"
                        )
                    case ChannelType.Float3:
                        colattr = mesh.color_attributes.new(
                            name="VertexBufferColor", type="FLOAT_COLOR", domain="POINT"
                        )
                    case ChannelType.Half4:
                        colattr = mesh.color_attributes.new(
                            name="VertexBufferColor", type="FLOAT_COLOR", domain="POINT"
                        )
                    case _:
                        raise Exception(f"Unknown color ChannelType {CT}")
                # give mesh the vertex color data
                try:
                    colattr.data.foreach_set("color", vertex_buffers["colors"][0])
                    bself.report({"DEBUG"}, "Vertex color attributes set")
                except Exception as e:
                    bself.report({"WARNING"}, f"Could not set vertex colors: {e}")
            else:
                bself.report({"DEBUG"}, "Skipping custom vertex colors")
        # if we have custom normal data
        if "normals" in vertex_buffers.keys():
            bself.report({"DEBUG"}, f"Custom normals found for mesh {name}")
            if config.CompiledUnitSettings.IMPORT_CUSTOM_NORMALS:
                mesh.normals_split_custom_set_from_vertices(vertex_buffers["normals"])
                # Set smooth shading to true so we can see the effect of the normals.
                mesh.use_auto_smooth = True
            else:
                bself.report({"DEBUG"}, "Skipping custom normals")
        if "texcoords" in vertex_buffers.keys():
            bself.report({"DEBUG"}, f"Texcoord data found for mesh {name}")
            if config.CompiledUnitSettings.IMPORT_TEXCOORDS:
                uv_name = f"UVMap{name}"
                # check for negative floats in texcoords
                has_negative = False
                for f in vertex_buffers["texcoords"]:
                    if f < 0.0:
                        has_negative = True
                if has_negative:  # this might be bad?
                    pass
                    # bself.report({'WARNING'}, f"Texcoord channel for {name} has negative float(s)")
                uv_layer = mesh.uv_layers.new(name=uv_name)
                uv_layer.data.foreach_set("uv", vertex_buffers["texcoords"])
                # bself.report({'WARNING'}, "Texcoord application failed")
            else:
                bself.report({"DEBUG"}, "Skipping texcoords")

        new_obj = bpy.data.objects.new(name, mesh)
        bpy.context.collection.objects.link(new_obj)
        bpy.context.view_layer.objects.active = new_obj

        # material slots
        # bpy.ops.object.mode_set(mode = 'EDIT')
        # ob = bpy.context.edit_object
        # me = ob.data
        # bm = bmesh.from_edit_mesh(me)
        for i in range(len(self.BatchRanges)):
            batch_range = self.BatchRanges[i]
            faces = batch_range.get_face_indices()
            wanted_material = self.Materials[batch_range.MaterialIndex]
            mat = find_material_by_idstring32(self, wanted_material)
            if not mat:
                # assign default material
                # TODO what if the blend file doesn't have this file?
                bself.report(
                    {"WARNING"},
                    f"Mesh {name} wants material {wanted_material.lookup()} ({str(wanted_material)}), but it is not imported. Assigning default material instead.",
                )
                mat = bpy.data.materials["Standard"]
            new_obj.data.materials.append(mat)

            # get material index of the material we just added to the object
            mat_name = mat.name
            mat_index = 1
            for m in range(len(new_obj.data.materials)):
                if new_obj.data.materials[m].name == mat_name:
                    mat_index = m
                    break

            # assign material index to each face in the batch range
            for face_index in faces:
                new_obj.data.polygons[face_index].material_index = mat_index

            # name = str(self.Materials[batch_range.MaterialIndex])
            # bpy.ops.object.material_slot_add()

            # deselect all
            # bpy.ops.object.mode_set(mode = 'EDIT')
            # bpy.ops.mesh.select_all(action='DESELECT')
            """
            for polygon in bpy.context.active_object.data.polygons:
                polygon.select = False
            for edge in bpy.context.active_object.data.edges:
                edge.select = False
            for vertex in bpy.context.active_object.data.vertices:
                vertex.select = False
            """

            # select the faces
            # bpy.ops.mesh.select_mode(type="FACE")
            # for face in faces:
            #    new_obj.data.polygons[face].select = True
            """
            new_vertex_group = bpy.context.active_object.vertex_groups.new(name=name)
            new_vertex_group.add(verts, 1.0, 'ADD') # can't be in edit
            """
            # bpy.ops.object.vertex_group_set_active(group=new_vertex_group.name)

            # bpy.ops.object.vertex_group_select() # must be in edit
            # bpy.ops.object.material_slot_assign()
            # bpy.ops.object.mode_set(mode = 'OBJECT')
            # bpy.ops.mesh.select_all(action='DESELECT')

        # bpy.ops.object.mode_set(mode = 'OBJECT')

        return new_obj

    def do_weights(
        self,
        bself,
        mesh,
        blend_weights,
        blend_indices,
        skin: "SkinData",
        batchrange_index: int,
    ):
        # how do we know what joints we should make groups for
        bone_set = self.BatchRanges[batchrange_index].BoneSet
        joints_data = skin.GetJointsBelongingToSet(bone_set)
        joint_index_list = [i[0] for i in joints_data]


class SimpleAnimationGroup:
    Name: IDString32
    Data: list[int]


class SceneGraphData:
    LocalTransform: Matrix  # Matrix4x4
    WorldTransform: Matrix  # Matrix4x4
    ParentType: ParentType
    ParentIndex: int  # ushort
    Name: IDString32


class SceneGraph:
    Nodes: list[SceneGraphData]


# stingray::MeshObject
class MeshObject:
    Name: IDString32
    NodeIndex: int  # actually Node: SceneGraphHandle
    GeometryIndex: int
    SkinIndex: int
    # Flags: s.RenderableFlags #TODO
    Flags: int
    BoundingVolume: BoundingVolume

    def HasMeshGeo(self) -> bool:
        return self.GeometryIndex > 0


"""
Actor resource stuff
"""


# stingray::CollisionFilter
class CollisionFilter:
    Is: int  # uint64
    CollidesWith: int  # uint64


# stingray::ShapeProperties
class ShapeProperties:
    Flags: int  # uint32
    CollisionFilter: CollisionFilter


# stingray::MaterialProperties
class MaterialProperties:
    DynamicFriction: int  # uint32
    StaticFriction: int  # uint32
    Restitution: int  # uint32
    Density: int  # uint32
    FrictionCombineMode: int  # uint32
    RestitutionCombineMode: int  # uint32


# stingray::ActorResource
class ActorResource:

    # stingray::ActorResource::Shape
    class Shape:
        Type: ShapeType  # uint32
        # u1: ? 32 bytes to hold data depending on shape type
        Material: IDString32
        ShapeTemplate: IDString32
        LocalTM: Matrix  # Matrix4x4
        ShapeData: bytes  # array<char>
        ShapeNode: IDString32

    Name: IDString32
    ActorTemplate: IDString32
    Node: IDString32
    Mass: int  # uint32
    Shapes: list[Shape]
    OnStartTouch: int  # uint32
    OnStayTouching: int  # uint32
    OnEndTouch: int  # uint32
    OnTriggerEnter: int  # uint32
    OnTriggerLeave: int  # uint32
    OnTriggerStay: int  # uint32
    Enabled: int  # byte


# not to be confused with ActorResource?
# doesn't look like this is deserialized; do we need this?
# stingray::ActorDesc
class ActorDesc:

    # stingray::ActorDesc::Shape
    class Shape:
        Type: int  # uint32
        u1: int  # unknown?
        Material: MaterialProperties
        Shape: ShapeProperties
        LocalTM: Matrix  # Matrix4x4
        ShapeNode: IDString32

    Enabled: int  # byte
    OnStartTouch: int  # uint32
    OnStayTouching: int  # uint32
    OnEndTouch: int  # uint32
    OnTriggerEnter: int  # uint32
    OnTriggerLeave: int  # uint32
    OnTriggerStay: int  # uint32
    Shapes: list[Shape]


# stingray::SceneGraphHandle
class SceneGraphHandle:
    # _graph dq # ? pointer to graph?
    Index: int


# stingray::CameraData
class CameraData:
    NearRange: int
    FarRange: int
    VerticalFov: int
    Type: int


# stingray::Camera
class Camera:
    Name: IDString32
    Node: SceneGraphHandle
    CameraData: CameraData


# stingray::LightData
class LightData:
    Color: Vector  # Vector3
    Intensity: int
    VolumetricIntensity: int
    Parameters: list[int]  # 6 items
    Type: int
    Flags: int
    Material: IDString64


# stingray::Light
class Light:
    Name: IDString32
    Node: SceneGraphHandle
    LightData: LightData


# stingray::LODObjectData
class LODObjectData:

    # stingray::LODObjectData::Step
    class Step:
        unk1: int  # uint32?
        VisibleHeightRange: int  # uint32
        Meshes: list[int]  # uint32

    Name: IDString32
    Node: SceneGraphHandle
    Steps: list[Step]
    BoundingVolume: BoundingVolume
    Flags: int  # uint32


"""
TerrainData stuff
"""


# ?
class Terrain:

    # stingray::terrain::Node
    class Node:
        HeightMin: int  # uint32
        HeightMax: int  # uint32
        Children: list[int]  # list of 4 ints


# stingray::TerrainData
class TerrainData:
    # stingray::TerrainData::Layer
    class Layer:

        # stingray::TerrainData::Layer::DecorationUnit
        class DecorationUnit:
            Resource: IDString64
            Mesh: IDString32
            Density: int  # uint32

        # stingray::TerrainData::Layer::DecorationMaterial
        class DecorationMaterial:
            NUnits: int  # uint32
            Units: "list[DecorationUnit]"  # list of 4

        # stingray::TerrainData::Layer::Context
        class Context:
            Name: IDString32
            Materials: list[IDString32]  # list of 5

        # stingray::TerrainData::Layer::SurfaceProperties
        class TDSurfaceProperties:
            NumContexts: int  # uint32
            Contexts: "list[Context]"  # list of up to 8

        Map: IDString64
        Resolution: int  # uint32
        Type: int  # enum stingray::TerrainData::Layer::Type
        Materials: list[IDString32]  # list of 4
        DecorationMaterials: list[DecorationMaterial]  # list of 4
        SurfaceProperties: TDSurfaceProperties

    Name: IDString32
    Node: SceneGraphHandle
    BoundingVolume: BoundingVolume
    Flags: int  # uint32
    Nodes: list[Terrain.Node]
    BaseMaterial: IDString32
    NLayers: int  # uint32
    Layers: list[Layer]  # up to 16?
    HeightData: bytes  # up to 24?


# stingray::JointDesc
# TODO a lot of these ints may actually be floats
class JointDesc:
    class Motor:
        VelocityTarget: int  # uint32
        MaxForce: int  # uint32

    class Spring:
        Spring: int  # uint32
        Damper: int  # uint32
        Target: int  # uint32

    class RevoluteJoint:
        LimitLow: int  # uint32
        LimitHigh: int  # uint32
        Motor: "Motor"
        Spring: "Spring"

    class DistanceJoint:
        MinDistance: int  # uint32
        MaxDistance: int  # uint32
        Spring: "Spring"

    class SphericalJoint:
        SwingAxis: Vector  # vec3
        TwistLimitLow: int  # uint32
        TwistLimitHigh: int  # uint32
        SwingLimit: int  # uint32
        TwistSpring: "Spring"  # uint32
        SwingSpring: "Spring"  # uint32
        JointSpring: "Spring"  # uint32

    Name: IDString32
    Enabled: int  # byte
    Actor1: IDString32
    Actor2: IDString32
    Normal1: Vector  # vec3
    Normal2: Vector  # vec3
    Axis1: Vector  # vec3
    Axis2: Vector  # vec3
    GlobalAxis: Vector  # vec3
    Anchor1: Vector  # vec3
    Anchor2: Vector  # vec3
    GlobalAnchor: Vector  # vec3
    MaxForce: int  # uint32
    MaxTorque: int  # uint32
    JointFlags: int  # uint32
    Type: JointType  # uint32
    # joint data, up to 60 bytes
    Joint: RevoluteJoint | DistanceJoint | SphericalJoint


# stingray::MoverDesc
# TODO a lot of these ints may actually be floats
class MoverDesc:
    Name: IDString32
    Height: int  # uint32
    Radius: int  # uint32
    CollisionFilter: IDString32
    SlopeLimit: int  # uint32
    OnActorHit: int  # uint32
    OnMoverHit: int  # uint32


# stingray::ScriptMethod
class ScriptMethod:
    Klass: bytes  # dynamic string
    F: bytes  # dynamic string


# stingray::UnitResource
class UnitResource:
    Version: int
    MeshGeometries: list[MeshGeometry]
    Skins: list[SkinData]
    SimpleAnimation: bytes
    SimpleAnimationGroups: list[SimpleAnimationGroup]
    SceneGraph: SceneGraph
    Meshes: list[MeshObject]
    Actors: list[ActorResource]
    Cameras: list[Camera]
    Lights: list[Light]
    LODObjects: list[LODObjectData]
    Terrains: list[TerrainData]
    Joints: list[JointDesc]
    Movers: list[MoverDesc]
    ScriptEvents: list[tuple[IDString32, ScriptMethod]]
    HasAnimationBlenderBones: bool
    AnimationStateMachine: bytes  # dynamic string
    DynamicData: bytes  # dynamic string
    VisibilityGroups: list[tuple[IDString32, list[int]]]
    Flow: bytes
    FlowDynamicData: bytes
    MeshGeometryTriangleFinder: bytes
    PhysicsSceneData: bytes
    PhysicsSceneData64bit: bytes
    DefaultMaterialResource: IDString64
    Materials: list[tuple[IDString32, IDString64]]
    ApexData: bytes
    # Vehicles: list[VehicleResource]
    SkeletonName: IDString64

    def does_node_have_geometry(self, node_index: int) -> bool:
        # get list of the node indices referred to by any MeshObject objects
        mesh_object_node_indices = [meshobj.NodeIndex for meshobj in self.Meshes]
        return node_index in mesh_object_node_indices


# stingray::BonesResource
class BonesResource:
    BoneCount: int  # uint32
    LodCount: int  # uint32
    BoneNameHashes: list[IDString32]
    LodLevels: list[int]  # [uint32]
    BoneNames: list[str]


# stingray::AnimationResource
class AnimationResource:
    HeaderIdentifier: InterleavedAnimationItemType  # uint16
    # ZeroFiller
    NumBones: int  # uint32
    Length: float  # uint32
    Size: int  # uint32
    NumBeats: int  # uint32


"""
Materials
"""


# ShaderVariable klass
class VariableType(Enum):
    SCALAR = 0
    FLOAT = 0
    VECTOR2 = 1
    FLOAT2 = 1
    VECTOR3 = 2
    FLOAT3 = 2
    VECTOR4 = 3
    FLOAT4 = 3
    OTHER = 12  # what is this?


# stingray::MaterialTemplate::TextureChannel
class TextureChannel:
    Channel: IDString32  # uint32
    Name: IDString64  # uint64


# stingray::MaterialTemplate::MaterialContext
class MaterialContext:
    Context: IDString32  # uint32
    Material: IDString32  # uint32


# stingray::ShaderVariable
class ShaderVariable:
    Klass: VariableType  # uint32
    Elements: int  # uint32
    Name: IDString32  # uint32
    Offset: int  # uint32
    ElementStride: int  # uint32

    def to_dict(self, data) -> tuple[str, dict[str, str | int | float | list]]:
        # TODO fix
        match self.Klass, self.Elements:

            case VariableType.SCALAR, 0:
                (value,) = struct.unpack_from("<f", data, self.Offset)
                if value.is_integer():
                    value = int(value)

            case VariableType.VECTOR2, 0:
                x, y = struct.unpack_from("<ff", data, self.Offset)
                if x.is_integer():
                    x = int(x)
                if y.is_integer():
                    y = int(y)
                value = [x, y]

            case VariableType.VECTOR3, 0:
                x, y, z = struct.unpack_from("<fff", data, self.Offset)
                if x.is_integer():
                    x = int(x)
                if y.is_integer():
                    y = int(y)
                if z.is_integer():
                    z = int(z)
                value = [x, y, z]

            case VariableType.VECTOR4, 0:
                x, y, z, w = struct.unpack_from("<ffff", data, self.Offset)
                if x.is_integer():
                    x = int(x)
                if y.is_integer():
                    y = int(y)
                if z.is_integer():
                    z = int(z)
                if w.is_integer():
                    w = int(w)
                value = [x, y, z, w]

            case x, y:
                raise NotImplementedError(
                    f"Can't handle Klass of {x} and Elements of {y}"
                )

        d = {"type": self.Klass.name.lower(), "value": value}
        return (self.Name.lookup(), d)

    def get_type(self) -> int:
        return self.Klass.value


# stingray::MaterialTemplate
class MaterialTemplate:
    Textures: list[TextureChannel]
    MaterialContexts: list[MaterialContext]
    ParentMaterial: IDString64
    u3: int  # union of {uber_shader: IDString32, parent_is_shader: 0}
    VariableReflection: list[ShaderVariable]
    VariableData: bytes

    def get_variables(self) -> dict:
        result = {}
        for variable in self.VariableReflection:
            tup = variable.to_dict(self.VariableData)
            result[tup[0]] = tup[1]
        return result


# stingray::MaterialManager::MaterialData
class MaterialData:
    MaterialTemplate: MaterialTemplate
    Material: IDString64


# stingray::MaterialResource
class MaterialResource:
    # header is 24 bytes
    Version: int  # uint32 should be 43
    Type: int  # uint32 does file have a single material or material set
    MaterialOffset: (
        int  # uint32 offset is relative to start of material header. always 24?
    )
    MaterialSize: int  # uint32 length of material data in bytes
    ShaderOffset: int  # uint32 also relative to start of material header
    ShaderSize: int  # uint32 length of shader data in bytes

    Materials: list[tuple[IDString32, MaterialTemplate]]
    Name: IDString64

    def has_shader_nodes(self):
        return self.ShaderOffset != 0xFFFFFFFF


# stingray::shader_resource::Header
class ShaderResource:
    Version: int  # uint32 should be 0x21
    Name: IDString32
    ShaderContextsOffset: int  # uint32
    NumShaderContexts: int  # uint32
    ConditionExpressionsOffset: int  # uint32
    DefaultDataBlockOffset: int  # uint32
    RenderConfigDependenciesOffset: int  # uint32
    NumRenderConfigDependencies: int  # uint32
    ShaderDataOffset: int  # uint32
    ShaderDataSize: int  # uint32
    DeviceDataOffset: int  # uint32
    DeviceDataSize: int  # uint32


"""
Animation state machine
"""


# stingray::AnimationBlender::BoneMode
class BoneMode(Enum):
    BM_IGNORE = 0
    BM_POSITION = 1
    BM_ROTATION = 2
    BM_POSITION_AND_ROTATION = 3
    BM_SCALE = 4
    BM_POSITION_AND_SCALE = 5
    BM_ROTATION_AND_SCALE = 6
    BM_TRANSFORM = 7


# stingray::AnimationBlender::RootMode
class RootMode(Enum):
    RM_IGNORE = 0
    RM_DELTA_POSITION = 1
    RM_DELTA_ROTATION = 2
    RM_DELTA_POSITION_AND_ROTATION = 3
    RM_DELTA_SCALE = 4
    RM_DELTA_POSITION_AND_SCALE = 5
    RM_DELTA_ROTATION_AND_SCALE = 6
    RM_DELTA_TRANSFORM = 7


# stingray::AnimationState::StateType
class AnimationStateType(Enum):
    REGULAR_STATE = 0
    EMPTY_STATE = 1
    MIX_STATE = 2
    TIME_STATE = 3
    RAGDOLL_STATE = 4


# stingray::AnimationState::RandomizationType
class RandomizationType(Enum):
    RANDOMIZE_ON_ENTRY = 0
    RANDOMIZE_EVERY_LOOP = 1
    RANDOMIZE_EVERY_LOOP_DONT_REPEAT = 2


# stingray::AnimationBlender::BlendType
class BlendType(Enum):
    BT_NORMAL = 0
    BT_OFFSET = 1


# stingray::AnimationState::Marker::Type
class MarkerType(Enum):
    DISABLE_COLLISION = 0
    DISABLE_RAYCASTING = 1
    DISABLE_RESPONSE = 2
    DISABLE_SCENE_QUERIES = 3
    SWEEP = 4
    TRIGGER = 5
    N_SHAPE_FLAGS = 6


# stingray::AnimationTransition
class AnimationTransition:
    Event: IDString32
    Index: int
    IsConditionalTransition: bool


# stingray::AnimationBlendSettings
class AnimationBlendSettings:
    To: int
    Blend: int
    Mode: int
    OnBeat: IDString32
    unk: int


# stingray::AnimationTransitionSwitchExit
class AnimationTransitionSwitchExit:
    BlendSettingsIndex: int
    IntervalStart: int
    IntervalEnd: int
    ExcludeStart: bool
    ExcludeEnd: bool


# stingray::AnimationTransitionSwitch
class AnimationTransitionSwitch:
    Bytecode: int
    Exits: list[AnimationTransitionSwitchExit]


# stingray::AnimationState::ExitEvent
class ExitEvent:
    Name: IDString32
    BlendTime: float  # probably a float?


# stingray::AnimationState::Marker
class Marker:
    Time: float  # probably a float
    Name: IDString32
    Type: MarkerType


# stingray::AnimationState
class AnimationState:
    Name: IDString64

    name2: IDString64
    name3: IDString64

    StateType: AnimationStateType
    Animations: list[IDString64]
    Probabilities: list[float]
    RandomizationType: RandomizationType
    LoopAnimation: bool
    BlendType: BlendType  # uint32
    Transitions: list[AnimationTransition]
    BlendSettings: list[AnimationBlendSettings]
    Switches: list[AnimationTransitionSwitch]
    Timeline: list[Marker]

    unk1: int
    unk2_arr: list[int]

    ExitEvent: ExitEvent
    Bytecode: list[int]
    Weights: list[int]

    unk3: int

    Speed: float  # float?

    unk4: int

    # RootDriving: RootMode
    # BoneAnimMode: BoneMode
    BlendSet: int
    Constraints: list[int]

    unk6: int

    # TimeVariable: int
    MutedLayersMask: int
    Ragdoll: int


# stingray::AnimationLayer
class AnimationLayer:
    States: list[AnimationState]
    DefaultState: int  # uint32


# stingray::BlendSet
class BlendSet:
    BoneWeights: list[float]

    unk_arr: list[int]
    unk_bool: bool


# stingray::Ragdoll
class Ragdoll:
    DynamicActors: list[IDString32]
    KeyframedActors: list[IDString32]


class UnkStructOf7:
    unk1: int
    unk2: int
    unk3: int
    unk4: int
    unk5: int
    unk6: int
    unk7: float  # usually 0xBF800000 which is -1.0


# stingray::AnimationStateMachineResource
class AnimationStateMachineResource:
    Layers: list[AnimationLayer]
    EventNames: list[IDString32]
    VariableNames: list[IDString32]
    Variables: list[float]

    unk1_arr: list[float]  # 2float

    BlendSets: list[BlendSet]
    ConstraintTargets: list[IDString32]
    ConstraintTargetPositions: list[Vector]  # list[Vec3]
    Constraints: bytes
    Ragdolls: list[Ragdoll]

    unk3_arr: list[UnkStructOf7]
    unk4: float
    # ResourceName: IDString64


"""
Other
"""


# not in stingray
class SimpleAnimationResource:
    Data: bytes
    Groups: list[SimpleAnimationGroup]


# not in stingray
class ObjectType(Enum):
    Object = 0
    Bone = 1


# not in stingray
class ImportedObject:
    # there are many limitations to how we can hold references to blender data objects
    # better to just hold the name and type, then look them up when needed
    Type: ObjectType
    Name: str
    ID: IDString32
    Obj: Optional[bpy.types.Object]

    def __init__(self, obj, name: str, id: IDString32):
        self.Name = name
        self.ID = id
        t = type(obj)
        if t == bpy.types.EditBone:
            self.Type = ObjectType.Bone
            # can't hold a reference to an EditBone
            self.Obj = None
        elif t == bpy.types.Object:
            self.Type = ObjectType.Object
            self.Obj = obj
        else:
            raise Exception(f"Unknown object type {t}")


# not in stingray
class ImportedObjects:
    Objects: list[Optional[ImportedObject]]
    Armature: Optional[bpy.types.Object]

    def __init__(self):
        self.Objects = []
        self.Armature = None

    def try_find_object(self, name: str) -> Optional[bpy.types.Object]:
        """
        Try to return a reference to the actual blender object with the given name,
        or armature object for bones.
        """
        for imported_obj in self.Objects:
            if not imported_obj:
                continue
            if imported_obj.Name != name:
                continue
            if imported_obj.Type == ObjectType.Bone:
                assert self.Armature
                return self.Armature
            return imported_obj.Obj
        # TODO, is this correct when we skip importing some objects
        raise Exception(f'"{name}" cannot be found')
        return None

    def get_reference_list(self) -> list[Optional[bpy.types.Object]]:
        """
        Return list of references to actual blender objects, or armature object for bones.
        """
        result: list[Optional[bpy.types.Object]] = []
        for imported_obj in self.Objects:
            if not imported_obj:
                result.append(None)
                continue
            result.append(self.try_find_object(imported_obj.Name))
        return result


# create 4x4 translation matrix from vec3
# Vec3 -> Matrix4x4
def CreateTranslation(vec: Vector) -> Matrix:
    m = Matrix.Translation((vec[0], vec[1], vec[2]))
    return m


# create 4x4 scale matrix from vec3
# Vec3 -> Matrix4x4
def CreateScale(vec: Vector) -> Matrix:
    m = Matrix.Scale(1, 4, vec)
    return m


# get translation-rotation-scale matrix
# Vec3, Matrix4x4, Vec3 -> Matrix4x4
def GetTRSMatrixM(position: Vector, rotation: Matrix, scale: Vector) -> Matrix:
    t = CreateTranslation(position)
    r = rotation
    s = CreateScale(scale)
    return s @ r @ t


# Vector4Half to Vector3
def V4H_to_Vec3(vec) -> Vector:
    # Vector4Half has XYZW
    # Return vector of XYZ, discarding W
    return Vector((vec[0], vec[1], vec[2]))


# stingray::math::normalize_representation
def NormalizeRepresentation(a: Quaternion, b: Quaternion) -> Quaternion:
    if (a.x * b.x + a.y * b.y + a.z * b.z + a.w * b.w) < 0.0:
        return b * -1
    else:
        return b
