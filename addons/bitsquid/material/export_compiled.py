"""
Export compiled .material files to SJSON
"""

from bitsquid import sjson
import bitsquid.stingray as s
from bitsquid.murmur.rainbow_table import IDString32


def do_one_material(self, mat_template: s.MaterialTemplate) -> dict[str, dict]:
    result = {}

    # parent material
    parent_material = mat_template.ParentMaterial.lookup()

    # material_contexts
    material_contexts = {}
    for mat_context in mat_template.MaterialContexts:
        material_contexts[mat_context.Context.lookup()] = mat_context.Material.lookup()

    # shader
    # either shader nodes, an uber shader, or nothing
    # probably not possible to directly recreate shader nodes from compiled shader data
    # self.report({'INFO'}, "Shader node exporting not implemented")
    shader: dict | str = {}
    if mat_template.u3 != 0:
        uber_shader = IDString32(mat_template.u3).lookup()
        shader = uber_shader

    # textures
    textures = {}
    for tex_channel in mat_template.Textures:
        textures[tex_channel.Channel.lookup()] = tex_channel.Name.lookup()

    # variables
    variables = mat_template.get_variables()

    # overall dict
    if parent_material:
        result["parent_material"] = parent_material
    if material_contexts:
        result["material_contexts"] = material_contexts
    if shader:
        result["shader"] = shader
    if textures:
        result["textures"] = textures
    if variables:
        result["variables"] = variables

    return result


def get_sjson(self, mat_res) -> str:
    # we have a list of tuples of IDString32, MaterialTemplate
    result: dict[str, dict] = {}
    if mat_res.Type == 1:  # single material
        assert len(mat_res.Materials) == 1
        mat_template = mat_res.Materials[0][1]
        result = do_one_material(self, mat_template)

    else:
        assert mat_res.Type == 0
        for tup in mat_res.Materials:
            name: IDString32 = tup[0]
            mat_template: s.MaterialTemplate = tup[1]
            data = do_one_material(self, mat_template)

            result[name.lookup()] = data

    return sjson.dumps(result, indent=4)


def export(self, mat_res: s.MaterialResource, outpath: str) -> set:

    result = get_sjson(self, mat_res)
    try:
        with open(outpath, "w") as f:
            f.write(result)
    except Exception as e:
        self.report({"ERROR"}, f"Can't write to file {outpath}. {e}")
        return {"CANCELLED"}

    return {"FINISHED"}
