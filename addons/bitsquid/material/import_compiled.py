"""
Import compiled .material file
"""

import os
from typing import Optional
import bpy
import bitsquid.config as c
import bitsquid.stingray as s
import bitsquid.deserializing as d
from bitsquid.utils import ensure_module as ensure_module
from bitsquid.murmur.rainbow_table import IDString32, IDString64


# stingray::Array<stingray::MaterialTemplate::TextureChannel>::serialize()
def TextureChannelArrayDeserializer(data: bytes) -> list[s.TextureChannel]:
    count = d.ReadUint32(data)
    result = [s.TextureChannel() for i in range(count)]
    for i in range(count):
        result[i].Channel = d.ReadIDString32(data)
        result[i].Name = d.ReadIDString64(data)
    return result


# stingray::Array<stingray::MaterialTemplate::MaterialContext>::serialize()
def MaterialContextArrayDeserializer(data: bytes) -> list[s.MaterialContext]:
    count = d.ReadUint32(data)
    result = [s.MaterialContext() for i in range(count)]
    for i in range(count):
        result[i].Context = d.ReadIDString32(data)
        result[i].Material = d.ReadIDString32(data)
    return result


# stingray::Array<stingray::ShaderVariable>::serialize()
def ShaderVariableArrayDeserializer(data: bytes) -> list[s.ShaderVariable]:
    count = d.ReadUint32(data)
    result = [s.ShaderVariable() for i in range(count)]
    for i in range(count):
        result[i].Klass = s.VariableType(d.ReadUint32(data))
        result[i].Elements = d.ReadUint32(data)
        result[i].Name = d.ReadIDString32(data)
        result[i].Offset = d.ReadUint32(data)
        result[i].ElementStride = d.ReadUint32(data)
    return result


# read a single material
# stingray::material_resource::load()
def deserialize_material(data: bytes) -> s.MaterialTemplate:
    result = s.MaterialTemplate()
    result.u3 = d.ReadUint32(
        data
    )  # funny union. shader = "base"? 0 if a shader is included in the file?
    result.ParentMaterial = d.ReadIDString64(data)  # parent_material
    result.Textures = TextureChannelArrayDeserializer(
        data
    )  # list of textures eg color_map = "textures/wood_basecolor"
    result.MaterialContexts = MaterialContextArrayDeserializer(data)
    result.VariableReflection = ShaderVariableArrayDeserializer(
        data
    )  # shader variable array
    result.VariableData = d.ReadByteArray(data)  # other shader variable data?
    return result


# this is for files with multiple materials
# stingray::material_resource::instantiate_set()
# stingray::Vector<stingray::Pair<stingray::IdString32,stingray::MaterialManager::MaterialData,0,1>>::serialize()
def deserialize_material_set(
    data: bytes,
) -> list[tuple[IDString32, s.MaterialTemplate]]:
    count = d.ReadUint32(data)
    result = []
    for i in range(count):
        first = d.ReadIDString32(data)
        second = deserialize_material(data)
        result.append((first, second))
    return result


def deserialize_shader(data: bytes):
    """
    Shader data is basically a whole other file tacked
    onto the end of the material data; it has its own header.
    Shader header offsets are relative to the start of the shader header
    TODO maybe we can get some useful data from shaders?
    """
    EXPECTED_VERSION = 0x21

    # Shader header
    result = s.ShaderResource()
    result.Version = d.ReadUint32(data)
    if result.Version != EXPECTED_VERSION:
        # wrong shader version
        raise Exception(
            f"Wrong shader version. Got {result.Version}, expected {EXPECTED_VERSION}"
        )
    result.Name = d.ReadIDString32(data)
    result.ShaderContextsOffset = d.ReadUint32(data)  # right after the header
    result.NumShaderContexts = d.ReadUint32(data)  # 20 bytes each?
    result.ConditionExpressionsOffset = d.ReadUint32(
        data
    )  # is this data always present?
    result.DefaultDataBlockOffset = d.ReadUint32(
        data
    )  # small amount of data at end of file?
    result.RenderConfigDependenciesOffset = d.ReadUint32(
        data
    )  # is this data always present?
    result.NumRenderConfigDependencies = d.ReadUint32(data)
    result.ShaderDataOffset = d.ReadUint32(data)
    result.ShaderDataSize = d.ReadUint32(data)  # decent amount of data
    result.DeviceDataOffset = d.ReadUint32(data)
    result.DeviceDataSize = d.ReadUint32(data)  # this is the bulk of the shader data
    # End header


# stingray::material_resource::load()
def load_material_resource(self, data: bytes, name: IDString64) -> s.MaterialResource:
    EXPECTED_VERSION = 43
    result = s.MaterialResource()

    # Material header
    result.Version = d.ReadUint32(data)
    if result.Version != EXPECTED_VERSION:
        raise Exception(
            f"Wrong material version. Got {result.Version}, expected {EXPECTED_VERSION}"
        )
    # Type is set to 1 when the file contains only a single, unnamed material definition
    # it is set to 0 if the file contains a material set
    result.Type = d.ReadUint32(data)
    if (result.Type != 0) and (result.Type != 1):
        raise Exception(f"Unknown type {result.Type}")
    result.MaterialOffset = d.ReadUint32(data)
    # always 24?
    assert result.MaterialOffset == 24
    result.MaterialSize = d.ReadUint32(data)
    # 0xffffffff when there are no shader nodes
    # offset into file otherwise
    result.ShaderOffset = d.ReadUint32(data)
    # 0x0 when there are no shader nodes
    # length of shader data otherwise
    result.ShaderSize = d.ReadUint32(data)
    # End header

    c.offset = result.MaterialOffset
    if result.Type == 0:  # multiple materials in the file
        result.Materials = deserialize_material_set(data)
    else:  # single material
        mat = deserialize_material(data)
        empty_name = IDString32(0)
        result.Materials = [(empty_name, mat)]

    # testing
    """
    for pair in result.Materials:
        # this one is true
        assert pair[1].u3 != 1
        # these are not true
        if pair[1].u3 == 0:
            assert result.ShaderOffset == 0xffffffff
        if result.ShaderOffset != 0xffffffff:
            assert pair[1].u3 != 0
    """

    # then padding zeroes to nearest 16 bytes
    # should we bother checking it?
    if c.offset == len(data):
        # EOF, no padding
        pass
    elif c.offset % 16 > 0:  # if the offset isn't a multiple of 16
        num_zeroes = 16 - (c.offset % 16)
        padding = data[c.offset : c.offset + num_zeroes]
        if padding != b"\x00" * num_zeroes:
            raise Exception("Padding was not all zeroes")

    if result.has_shader_nodes():
        # no shader data deserializing at the moment, just the header
        c.offset = result.ShaderOffset
        deserialize_shader(data)
    result.Name = name
    mat_names = [i[0].lookup() for i in result.Materials]

    self.report(
        {"INFO"},
        f"Successfully decompiled [{len(result.Materials)}] materials: {mat_names}",
    )
    return result


def try_load_texture(
    self, tex_channel: s.TextureChannel, directory: str
) -> Optional[bpy.types.Image]:
    """
    Try to look for directory/<name>.dds. If it can't be found,
    look for <name>.png.
    Return bpy.data.Image, or None.
    """
    name = tex_channel.Name
    filename_dds = f"{str(name)}.dds"
    filepath_dds = os.path.join(directory, filename_dds)
    exists_dds = os.path.isfile(filepath_dds)
    if exists_dds:
        try:
            new_image = bpy.data.images.load(filepath_dds)
            return new_image
        except RuntimeError as e:
            self.report({"WARNING"}, f"{e}")

    filename_png = f"{str(name)}.png"
    filepath_png = os.path.join(directory, filename_png)
    exists_png = os.path.isfile(filepath_png)
    if exists_png:
        try:
            new_image = bpy.data.images.load(filepath_png)
            return new_image
        except RuntimeError as e:
            self.report({"WARNING"}, f"{e}")

    self.report({"WARNING"}, f"Cannot find texture file {str(tex_channel.Name)}")
    return None


def try_find_material(self, name: str) -> Optional[bpy.types.Material]:
    for mat in bpy.data.materials:
        if mat.name == name:
            return mat
    return None


def copy_blender_material(
    self, material_name: str, new_name: str
) -> Optional[bpy.types.Material]:
    old_mat = try_find_material(self, material_name)
    if old_mat:
        new_mat: bpy.types.Material = old_mat.copy()
        new_mat.name = new_name
        return new_mat
    return None


def import_to_blender(
    self, mat_res: s.MaterialResource, directory: str, slot_name: Optional[IDString32]
):
    """
    mat_res: a deserialized MaterialResource object
    directory: he folder the material was found in. Needed to look for texture files.
    slot_name: the name this material is called in a unit file
    """
    # when a unit file requests a material, it refers to it by an IDString32 (the material slot on a mesh)
    if slot_name:
        default_name = slot_name.lookup()
    else:
        # if this material is being loaded by itself, just call it by its filename
        default_name = mat_res.Name.lookup()

    if default_name in bpy.data.materials:
        # skip, or replace?
        # have global option for skipping or replacing
        pass

    for material in mat_res.Materials:
        # A material resource may contain a single material or material set
        # If it is a material set, each material has its own name
        mat_name = material[0].lookup()

        # If the name of a material in a material resource is an empty string, it is probably a single material
        # Give it the default name
        if mat_name == "":
            mat_name = default_name

        mat_template = material[1]

        # make a copy of Standard
        new_mat = copy_blender_material(self, "Standard", mat_name)
        if not new_mat:
            continue

        new_mat["IDString32"] = str(slot_name)

        # import textures to blender
        assert c.CompiledUnitSettings
        if c.CompiledUnitSettings.IMPORT_TEXTURES:
            for tex_channel in mat_template.Textures:
                new_img = try_load_texture(self, tex_channel, directory)
                if new_img:
                    new_img["IDString32"] = str(material[0])

                    # TODO find a systematic way to figure out what texture channels each texture
                    # should be matched to
                    if tex_channel.Channel.ID in [
                        1761994824,
                        3450502756,
                        1349742260,
                        4051620787,
                        4180223857,
                    ]:
                        new_mat.node_tree.nodes["Diffuse map"].image = new_img
                        new_img.colorspace_settings.name = "sRGB"
                        self.report(
                            {"INFO"},
                            f"Texture {tex_channel.Name.lookup()} assigned to channel {tex_channel.Channel.lookup()} in material {mat_name}",
                        )
                    elif tex_channel.Channel.ID in [
                        2402785366,
                        1429121651,
                        927287490,
                        1659368545,
                        2426208499,
                    ]:
                        new_mat.node_tree.nodes["Combined map"].image = new_img
                        new_img.colorspace_settings.name = "Non-Color"
                        self.report(
                            {"INFO"},
                            f"Texture {tex_channel.Name.lookup()} assigned to channel {tex_channel.Channel.lookup()} in material {mat_name}",
                        )
                    elif tex_channel.Channel.ID in [
                        2783956546,
                        2597656977,
                        1760732602,
                        86438781,
                        67373306,
                    ]:
                        new_mat.node_tree.nodes["Normal map"].image = new_img
                        new_img.colorspace_settings.name = "Non-Color"
                        self.report(
                            {"INFO"},
                            f"Texture {tex_channel.Name.lookup()} assigned to channel {tex_channel.Channel.lookup()} in material {mat_name}",
                        )
                    else:
                        self.report(
                            {"INFO"},
                            f"Texture {tex_channel.Name.lookup()} (unknown channel) loaded by material {mat_name}",
                        )
    """
    mat_template = mat_res.Materials[0][1]
    if mat_res.Materials[0][0].lookup():
        new_name = mat_res.Materials[0][0].lookup()
    """


def load(self, filepath: str, **kwargs) -> s.MaterialResource:
    name = os.path.splitext(os.path.basename(filepath))[0]
    with open(filepath, "rb") as f:
        data = f.read()

    self.report({"INFO"}, f"Importing .material file: {filepath}")
    c.offset = 0
    material_res = load_material_resource(self, data, IDString64(name))

    return material_res


def try_import(
    self, filepath: str, name: Optional[s.IDString32] = None, **kwargs
) -> set:
    try:
        directory = os.path.dirname(filepath)
        mat_res = load(self, filepath, **kwargs)
        import_to_blender(self, mat_res, directory, name)
        return {"FINISHED"}

    except Exception as e:
        self.report({"WARNING"}, f"Could not import .material file: {filepath}: {e}")
        return {"CANCELLED"}
