#    Bitsquid Blender Tools
#    Copyright (C) 2021  Lucas Schwiderski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

import os
import bpy
from bitsquid import step
from bpy_extras import io_utils


template = """
parent_material = "core/stingray_renderer/shader_import/standard"

textures = {}

variables = {
    base_color = {
        type = "vector3"
        value = [
            {{ base_color[0] }}
            {{ base_color[1] }}
            {{ base_color[2] }}
        ]
    }
    emissive = {
        type = "vector3"
        value = [
            {{ emissive[0] }}
            {{ emissive[1] }}
            {{ emissive[2] }}
        ]
    }
    emissive_intensity = {
        type = "scalar"
        value = {{ emissive_intensity }}
    }
    metallic = {
        type = "scalar"
        value = {{ metallic }}
    }
    roughness = {
        type = "scalar"
        value = {{ roughness }}
    }
    use_ao_map = {
        type = "scalar"
        value = {{ use_ao_map }}
    }
    use_color_map = {
        type = "scalar"
        value = {{ use_color_map }}
    }
    use_emissive_map = {
        type = "scalar"
        value = {{ use_emissive_map }}
    }
    use_metallic_map = {
        type = "scalar"
        value = {{ use_metallic_map }}
    }
    use_normal_map = {
        type = "scalar"
        value = {{ use_normal_map }}
    }
    use_roughness_map = {
        type = "scalar"
        value = {{ use_roughness_map }}
    }
}

"""


def build_filepath(context, material, mode='ABSOLUTE'):
    project_root = bpy.path.abspath(context.scene.bitsquid.project_root)
    base_src = os.path.dirname(context.blend_data.filepath)
    print(base_src, bpy.path.abspath(project_root))
    filename = "{}.material".format(material.name)
    return io_utils.path_reference(
        os.path.join(material.bitsquid.filepath, filename),
        base_src=base_src,
        base_dst=project_root,
        mode=mode,
    )


def save(self, context, material):
    filepath = build_filepath(context, material)
    self.report({'INFO'}, "Saving material to " + filepath)

    namespace = {
        'material': material,

        'base_color': (1, 1, 1),
        'roughness': 0.0,
        'metallic': 0.0,
        'emissive': (0, 0, 0),
        'emissive_intensity': 0,

        'use_color_map': 0,
        'use_roughness_map': 0,
        'use_metallic_map': 0,
        'use_emissive_map': 0,
        'use_ao_map': 0,
        'use_normal_map': 0
    }

    nodes = material.node_tree.nodes
    try:
        namespace['base_color'] = nodes["Base Color"].outputs[0].default_value
        namespace['roughness'] = nodes["Roughness"].outputs[0].default_value
        namespace['metallic'] = nodes["Metallic"].outputs[0].default_value
        namespace['emissive'] = nodes["Emissive"].outputs[0].default_value
        namespace['emissive_intensity'] = nodes["Emissive Intensity"].outputs[0].default_value
        namespace['use_color_map'] = nodes["Use Color Map"].outputs[0].default_value
        namespace['use_roughness_map'] = nodes["Use Roughness Map"].outputs[0].default_value
        namespace['use_metallic_map'] = nodes["Use Metallic Map"].outputs[0].default_value
        namespace['use_emissive_map'] = nodes["Use Emissive Map"].outputs[0].default_value
        namespace['use_ao_map'] = nodes["Use AO Map"].outputs[0].default_value
        namespace['use_normal_map'] = nodes["Use Normal Map"].outputs[0].default_value
    except:
        self.report({'WARNING'}, "Couldn't find Stingray Standard nodes")

    content = step.Template(template, strip=False).expand(namespace)

    with open(filepath, "w", encoding="utf8", newline="\n") as f:
        f.write(content)

    return {'FINISHED'}
