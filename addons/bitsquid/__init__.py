#    Bitsquid Blender Tools
#    Copyright (C) 2021  Lucas Schwiderski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
bl_info = {
    "name": "Bitsquid Engine",
    "author": "Lucas Schwiderski",
    "version": (0, 0, 1),
    "blender": (2, 90, 0),
    "location": "File > Import-Export",
    "description": "Import-Export Bitsquid related files",
    "category": "Import-Export",
}

# installs the needed python packages

# user needs to run this in their python enviroment on install
# can't auto accept prompt for some reason
# import pip

# needed_modules = [
#     'fastapi',
#     'sqlalchemy',
# ]

# for module in needed_modules:
#     pip.main(['install', 'needed_modules', '--user'])

# Reload sub modules if they are already loaded
if "bpy" in locals():
    import importlib

    if "config" in locals():
        importlib.reload(config)
    if "rainbow_table" in locals():
        importlib.reload(rainbow_table)
    if "stingray" in locals():
        importlib.reload(stingray)
    if "export_unit" in locals():
        importlib.reload(export_unit)
    if "export_material" in locals():
        importlib.reload(export_material)
    if "import_bsi" in locals():
        importlib.reload(import_bsi)
    if "import_compiled_unit" in locals():
        importlib.reload(import_compiled_unit)
    if "import_compiled_animation" in locals():
        importlib.reload(import_compiled_animation)
    if "import_compiled_bones" in locals():
        importlib.reload(import_compiled_bones)
    if "import_compiled_material" in locals():
        importlib.reload(import_compiled_material)
    if "export_compiled_material" in locals():
        importlib.reload(export_compiled_material)
    if "import_compiled_state_machine" in locals():
        importlib.reload(import_compiled_state_machine)
    if "utils" in locals():
        importlib.reload(utils)


import os
from datetime import datetime
from time import perf_counter
from math import floor
from statistics import mean
import glob
import uvicorn
import bpy
from bpy.app.handlers import persistent
from bpy.types import (
    Panel,
    Operator,
    PropertyGroup,
)
from bpy.props import (
    EnumProperty,
    BoolProperty,
    StringProperty,
    PointerProperty,
    FloatProperty,
    IntProperty,
)
from bpy_extras.io_utils import (
    ImportHelper,
    axis_conversion,
    orientation_helper,
    path_reference_mode,
)
from bpy.utils import script_paths
from pathlib import Path
from threading import Thread
from bitsquid import server
from bitsquid import server_updaters
from bitsquid.unit import export as export_unit, import_compiled as import_compiled_unit
from bitsquid.animation import import_compiled as import_compiled_animation
from bitsquid.bones import import_compiled as import_compiled_bones
from bitsquid.material import (
    export as export_material,
    import_compiled as import_compiled_material,
    export_compiled as export_compiled_material,
)
from bitsquid.state_machine import import_compiled as import_compiled_state_machine
from bitsquid import import_bsi, config, stingray, utils
from bitsquid.murmur import rainbow_table as rainbow_table


class BitsquidSettings(PropertyGroup):
    project_root: StringProperty(
        name="Project Root",
        description="The project directory considered as root path for all Bitsquid related operations.",
        default="//",
        subtype="DIR_PATH",
    )


class SCENE_PT_bitsquid(Panel):
    bl_label = "Bitsquid"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "scene"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = False
        layout.use_property_decorate = False

        bitsquid = context.scene.bitsquid
        layout.prop(bitsquid, "project_root", text="Project Root")


class BitsquidObjectSettings(PropertyGroup):
    filepath: StringProperty(
        name="Unit File Path",
        description="The directory to store the .unit file in. Needs to be within the project root.",
        default="//",
        subtype="DIR_PATH",
    )

    export_materials: BoolProperty(
        name="Export materials",
        description="Automatically export all referenced materials",
        default=True,
    )


class OBJECT_OT_bitsquid_export(Operator):
    """Export this object into a Bitsquid Unit file"""

    bl_idname = "object.bitsquid_export_unit"
    bl_label = "Export .unit"
    bl_options = {"REGISTER"}

    @classmethod
    def poll(cls, context):
        object = context.active_object
        return bpy.data.is_saved and object is not None and object.type == "MESH"

    def execute(self, context):
        object = context.active_object
        if object.bitsquid.export_materials:
            for material_slot in object.material_slots.values():
                export_material.save(self, context, material_slot.material)

        return export_unit.save(self, context, object)


class OBJECT_PT_bitsquid(Panel):
    bl_label = "Bitsquid"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "object"

    def draw(self, context):
        object = context.object
        layout = self.layout

        if object.type != "MESH":
            layout.label(text="Only objects of type 'MESH' are supported.")
            return

        layout.use_property_split = False
        layout.use_property_decorate = False

        bitsquid = object.bitsquid
        layout.prop(bitsquid, "filepath", text="Unit File Path")
        layout.prop(bitsquid, "export_materials", text="Export materials")
        layout.operator("object.bitsquid_export_unit", text="Export .unit")


class BitsquidMaterialSettings(PropertyGroup):
    filepath: StringProperty(
        name="Material File Path",
        description="The directory to store the .material file in. Needs to be within the project root.",
        default="//",
        subtype="DIR_PATH",
    )


class MATERIAL_OT_bitsquid_export(Operator):
    """Export this material into a Bitsquid Material file"""

    bl_idname = "object.bitsquid_export_material"
    bl_label = "Export .material"
    bl_options = {"REGISTER"}

    @classmethod
    def poll(cls, context):
        return bpy.data.is_saved and context.active_object is not None

    def execute(self, context):
        material = context.material
        return material_export.save(self, context, material)


class MATERIAL_PT_bitsquid(Panel):
    bl_label = "Bitsquid"
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "material"

    @classmethod
    def poll(cls, context):
        object = context.active_object
        return len(object.material_slots) > 0

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = False
        layout.use_property_decorate = False

        if context.material:
            bitsquid = context.material.bitsquid
            layout.prop(bitsquid, "filepath", text="Material File Path")
            layout.operator("object.bitsquid_export_material", text="Export .material")


@orientation_helper(axis_forward="-Z", axis_up="Y")
class ImportBSI(bpy.types.Operator, ImportHelper):
    """Load a Bitsquid .bsi File"""

    bl_idname = "import_scene.bsi"
    bl_label = "Import BSI"
    bl_options = {"PRESET", "UNDO"}

    filename_ext = ".bsi"
    filter_glob: StringProperty(
        default="*.bsi;*.bsiz",
        options={"HIDDEN"},
    )

    def execute(self, context):
        keywords = self.as_keywords(
            ignore=(
                "axis_forward",
                "axis_up",
                "filter_glob",
            )
        )

        if bpy.data.is_saved and context.preferences.filepaths.use_relative_paths:
            import os

            keywords["relpath"] = os.path.dirname(bpy.data.filepath)

        return import_bsi.load(self, context, **keywords)

    def draw(self, context):
        pass


@orientation_helper(axis_forward="-Z", axis_up="Y")
class ImportCompiledAnimation(bpy.types.Operator, ImportHelper):
    """Load a Bitsquid .animation File"""

    bl_idname = "import_scene.animation"
    bl_label = "Import animation"
    bl_options = {"PRESET", "UNDO"}

    filename_ext = ".animation"
    filter_glob: StringProperty(
        default="*.animation",
        options={"HIDDEN"},
    )

    @classmethod
    def poll(cls, context):
        obj = context.active_object
        if not obj:
            return False
        return obj.type == "ARMATURE"

    def execute(self, context):
        keywords = self.as_keywords(
            ignore=(
                "axis_forward",
                "axis_up",
                "filter_glob",
            )
        )
        # store animation import settings
        config.AnimationSettings = config.AnimationImportSettings(
            keywords["FRAMERATE"], keywords["POSITION_UNIT_MULTIPLIER"]
        )

        if bpy.data.is_saved and context.preferences.filepaths.use_relative_paths:
            import os

            keywords["relpath"] = os.path.dirname(bpy.data.filepath)

        return import_compiled_animation.try_import(self, **keywords)

    def draw(self, context):
        pass

    # animation
    FRAMERATE: IntProperty(
        name="Framerate",
        description="Set the framerate for animations. Does not speed up or slow down animations; it only affects the times chosen for keyframes.",
        default=30,
    )

    POSITION_UNIT_MULTIPLIER: FloatProperty(
        name="Position unit multiplier",
        description="The multiplier for position data in animation keyframes.",
        default=0.01,
    )


@orientation_helper(axis_forward="-Z", axis_up="Y")
class ImportCompiledUnit(bpy.types.Operator, ImportHelper):
    """Load a Bitsquid .unit File"""

    bl_idname = "import_scene.unit"
    bl_label = "Import unit"
    bl_options = {"PRESET", "UNDO"}

    filename_ext = ".unit"
    filter_glob: StringProperty(
        default="*.unit",
        options={"HIDDEN"},
    )

    # include
    IMPORT_MESHES: BoolProperty(
        name="Mesh Objects",
        description="Import nodes that have MeshObject data, otherwise skip.",
        default=True,
    )

    IMPORT_CUSTOM_NORMALS: BoolProperty(
        name="Custom normals",
        description="Import custom split normals. Smooth shading is automatically enabled.",
        default=True,
    )

    IMPORT_BLEND_WEIGHTS: BoolProperty(
        name="Vertex Weights",
        description="Import vertex blend weight data.",
        default=True,
    )

    IMPORT_VERTEX_COLORS: BoolProperty(
        name="Viewport Vertex Colors",
        description="Import custom viewport vertex colors. Not all units have custom colors.",
        default=True,
    )

    FLIP_UVS: BoolProperty(
        name="Flip UVs",
        description="Flip UVs in the Y axis.",
        default=True,
    )

    IMPORT_TEXCOORDS: BoolProperty(
        name="Texture Coordinates",
        description="Import texture coordinate data.",
        default=True,
    )

    IMPORT_EMPTIES: BoolProperty(
        name="Empty Nodes",
        description="Import nodes that do not have geometry data or joint data.",
        default=True,
    )

    IMPORT_JOINTS: BoolProperty(
        name="Joints",
        description="Import nodes that have joint data. Creates an armature.",
        default=True,
    )

    LOOKUP_IDSTRINGS: BoolProperty(
        name="IDString Lookup",
        description="Try to lookup original strings from hashes using the rainbow table.",
        default=True,
    )

    NODE_PARENTING: BoolProperty(
        name="Node Parenting",
        description="Parent nodes according to scene graph data.",
        default=True,
    )

    IMPORT_SIMPLEANIMS: BoolProperty(
        name="Simple Animations",
        description="Import Simple Animations that are held inside unit files.",
        default=False,
    )

    IMPORT_MATERIALS: BoolProperty(
        name="Materials",
        description="Try to import materials from .material files based on what the unit resource requests.",
        default=True,
    )

    IMPORT_TEXTURES: BoolProperty(
        name="Textures",
        description="Try to import textures from .texture files based on what material resources request.",
        default=True,
    )

    # animation
    FRAMERATE: IntProperty(
        name="Framerate",
        description="Set the framerate for animations. Does not speed up or slow down animations; it only affects the times chosen for keyframes.",
        default=30,
    )

    POSITION_UNIT_MULTIPLIER: FloatProperty(
        name="Position unit multiplier",
        description="The multiplier for position data in animation keyframes.",
        default=0.01,
    )

    # global
    LOOKUP_IDSTRINGS: BoolProperty(
        name="Look up IDStrings",
        description="Try to look up the original strings for hashes using dictionary files.",
        default=True,
    )

    def execute(self, context):
        keywords = self.as_keywords(
            ignore=(
                "axis_forward",
                "axis_up",
                "filter_glob",
            )
        )
        # store unit import settings
        config.CompiledUnitSettings = config.CompiledUnitImportSettings(
            keywords["IMPORT_MESHES"],
            keywords["IMPORT_CUSTOM_NORMALS"],
            keywords["IMPORT_BLEND_WEIGHTS"],
            keywords["IMPORT_VERTEX_COLORS"],
            keywords["IMPORT_TEXCOORDS"],
            keywords["FLIP_UVS"],
            keywords["IMPORT_EMPTIES"],
            keywords["IMPORT_JOINTS"],
            keywords["NODE_PARENTING"],
            keywords["IMPORT_SIMPLEANIMS"],
            keywords["IMPORT_MATERIALS"],
            keywords["IMPORT_TEXTURES"],
        )
        # store animation import settings
        config.AnimationSettings = config.AnimationImportSettings(
            keywords["FRAMERATE"], keywords["POSITION_UNIT_MULTIPLIER"]
        )
        # store global import settings
        config.GlobalSettings = config.GlobalImportSettings(
            keywords["LOOKUP_IDSTRINGS"]
        )

        if bpy.data.is_saved and context.preferences.filepaths.use_relative_paths:
            import os

            keywords["relpath"] = os.path.dirname(bpy.data.filepath)

        result = import_compiled_unit.load(self, context, **keywords)
        return result

    def draw(self, context):
        pass


class ImportCompiledMaterial(bpy.types.Operator, ImportHelper):
    """Load a Bitsquid .material File"""

    bl_idname = "import_scene.material"
    bl_label = "Import material"
    bl_options = {"PRESET", "UNDO"}

    filename_ext = ".material"
    filter_glob: StringProperty(
        default="*.material",
        options={"HIDDEN"},
    )

    def execute(self, context):
        keywords = self.as_keywords(
            ignore=(
                "axis_forward",
                "axis_up",
                "filter_glob",
            )
        )

        if bpy.data.is_saved and context.preferences.filepaths.use_relative_paths:
            import os

            keywords["relpath"] = os.path.dirname(bpy.data.filepath)

        return import_compiled_material.try_import(self, **keywords)

    def draw(self, context):
        pass


class COMPILEDUNIT_PT_import_global(bpy.types.Panel):
    bl_space_type = "FILE_BROWSER"
    bl_region_type = "TOOL_PROPS"
    bl_label = "Global"
    bl_parent_id = "FILE_PT_operator"

    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator

        return operator.bl_idname == "IMPORT_SCENE_OT_unit"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        sfile = context.space_data
        operator = sfile.active_operator

        layout.prop(operator, "LOOKUP_IDSTRINGS")

        sub = layout.row()
        sub.enabled = operator.LOOKUP_IDSTRINGS
        # sub.prop(operator, "DUMP_IDSTRINGS") # TODO


class COMPILEDUNIT_PT_import_include(bpy.types.Panel):
    bl_space_type = "FILE_BROWSER"
    bl_region_type = "TOOL_PROPS"
    bl_label = "Include"
    bl_parent_id = "FILE_PT_operator"

    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator

        return operator.bl_idname == "IMPORT_SCENE_OT_unit"

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        sfile = context.space_data
        operator = sfile.active_operator

        layout.prop(operator, "IMPORT_MESHES")

        sub = layout.row()
        sub.enabled = operator.IMPORT_MESHES
        sub.prop(operator, "IMPORT_CUSTOM_NORMALS")

        sub2 = layout.row()
        sub2.enabled = operator.IMPORT_MESHES
        sub2.prop(operator, "IMPORT_BLEND_WEIGHTS")

        sub3 = layout.row()
        sub3.enabled = operator.IMPORT_MESHES
        sub3.prop(operator, "IMPORT_VERTEX_COLORS")

        sub4 = layout.row()
        sub4.enabled = operator.IMPORT_MESHES
        sub4.prop(operator, "IMPORT_TEXCOORDS")

        sub4_1 = layout.row()
        sub4_1.enabled = operator.IMPORT_TEXCOORDS and operator.IMPORT_MESHES
        sub4_1.prop(operator, "FLIP_UVS")

        layout.prop(operator, "IMPORT_EMPTIES")
        layout.prop(operator, "IMPORT_JOINTS")
        layout.prop(operator, "NODE_PARENTING")
        layout.prop(operator, "IMPORT_SIMPLEANIMS")

        layout.prop(operator, "IMPORT_MATERIALS")
        sub5 = layout.row()
        sub5.enabled = operator.IMPORT_MATERIALS
        sub5.prop(operator, "IMPORT_TEXTURES")


class COMPILEDANIMATION_PT_import_animation(bpy.types.Panel):
    bl_space_type = "FILE_BROWSER"
    bl_region_type = "TOOL_PROPS"
    bl_label = "Animation"
    bl_parent_id = "FILE_PT_operator"

    @classmethod
    def poll(cls, context):
        sfile = context.space_data
        operator = sfile.active_operator

        return operator.bl_idname in [
            "IMPORT_SCENE_OT_unit",
            "IMPORT_SCENE_OT_animation",
        ]

    def draw(self, context):
        layout = self.layout
        layout.use_property_split = True
        layout.use_property_decorate = False  # No animation.

        sfile = context.space_data
        operator = sfile.active_operator

        layout.prop(operator, "FRAMERATE")
        layout.prop(operator, "POSITION_UNIT_MULTIPLIER")


class ImportCompiledBonesTest(bpy.types.Operator):
    bl_idname = "import_scene.bones_test"
    bl_label = "Test the .bones importing function"
    bl_description = (
        "Recursively imports all files from a folder to see which ones cause exceptions"
    )

    def execute(self, context):
        from bitsquid import bulk_import

        bulk_import.bulk_test(self, context, "bones")
        return {"FINISHED"}


class ImportCompiledUnitTest(bpy.types.Operator):
    """
    Tests import_compiled.py by importing every .unit file in a given folder.
    Any unit files that cause exceptions are reported to unit_test_output.log
    """

    bl_idname = "import_scene.unit_test"
    bl_label = "Test the .unit importing function"
    bl_description = (
        "Recursively imports all files from a folder to see which ones cause exceptions"
    )

    def execute(self, context):
        from bitsquid import bulk_import

        bulk_import.bulk_test(self, context, "unit")
        return {"FINISHED"}


class ImportCompiledAnimationTest(bpy.types.Operator):
    """
    Tests import_compiled.py by importing every .animation file in a given folder.
    Any animation files that cause exceptions are reported to animation_test_output.log
    """

    bl_idname = "import_scene.animation_test"
    bl_label = "Test the .animation importing function"
    bl_description = (
        "Recursively imports all files from a folder to see which ones cause exceptions"
    )

    def execute(self, context):
        from bitsquid import bulk_import

        bulk_import.bulk_test(self, context, "animation")
        return {"FINISHED"}


class ImportCompiledMaterialTest(bpy.types.Operator):
    """
    Test imports a lot of material files.
    """

    bl_idname = "import_scene.material_test"
    bl_label = "Test the .material importing function"
    bl_description = (
        "Recursively imports all files from a folder to see which ones cause exceptions"
    )

    def execute(self, context) -> set:
        from bitsquid import bulk_import

        bulk_import.bulk_test(self, context, "material")
        return {"FINISHED"}


class ImportCompiledStateMachineTest(bpy.types.Operator):
    """
    Test imports a lot of animation state machines files.
    """

    bl_idname = "import_scene.state_machine_test"
    bl_label = "Test the .state_machine importing function"
    bl_description = (
        "Recursively imports all files from a folder to see which ones cause exceptions"
    )

    def execute(self, context) -> set:
        from bitsquid import bulk_import

        bulk_import.bulk_test(self, context, "state_machine")
        return {"FINISHED"}


def menu_func_import_bsi(self, context):
    self.layout.operator(ImportBSI.bl_idname, text="Bitsquid Object (.bsi)")


def menu_func_import_compiled_unit(self, context):
    self.layout.operator(
        ImportCompiledUnit.bl_idname, text="Bitsquid Compiled Unit (.unit)"
    )


def menu_func_import_compiled_animation(self, context):
    self.layout.operator(
        ImportCompiledAnimation.bl_idname,
        text="Bitsquid Compiled Animation (.animation)",
    )


def menu_func_import_compiled_material(self, context):
    self.layout.operator(
        ImportCompiledMaterial.bl_idname, text="Bitsquid Compiled Material (.material)"
    )


# Register
classes = [
    ImportBSI,
    ImportCompiledUnit,
    COMPILEDUNIT_PT_import_include,
    COMPILEDANIMATION_PT_import_animation,
    COMPILEDUNIT_PT_import_global,
    ImportCompiledBonesTest,
    ImportCompiledUnitTest,
    ImportCompiledAnimation,
    ImportCompiledAnimationTest,
    ImportCompiledMaterial,
    ImportCompiledMaterialTest,
    ImportCompiledStateMachineTest,
    BitsquidSettings,
    SCENE_PT_bitsquid,
    BitsquidObjectSettings,
    OBJECT_PT_bitsquid,
    OBJECT_OT_bitsquid_export,
    BitsquidMaterialSettings,
    MATERIAL_PT_bitsquid,
    MATERIAL_OT_bitsquid_export,
]


def load_rainbow_table(dict_files):
    for file in dict_files:
        with open(file, "r") as f:
            data = f.read()
            strings = data.removesuffix("\n").split("\n")
            for s in strings:
                rainbow_table.resource_map.addstr(s)


def import_template():
    cwd = os.path.dirname(os.path.realpath(__file__))
    resources_dir = "resources"
    blendfile = "BitsquidPBR.blend"
    section = "Material"
    object = "Stingray Standard"

    filepath = os.path.join(cwd, resources_dir, blendfile, section, object)
    directory = os.path.join(cwd, resources_dir, blendfile, section)
    filename = object

    bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)

    # Import material template based on Janfon1's guide
    # https://steamcommunity.com/sharedfiles/filedetails/?id=2208764780
    blendfile = "StandardPBR.blend"
    object = "Standard"
    filepath = os.path.join(cwd, resources_dir, blendfile, section, object)
    directory = os.path.join(cwd, resources_dir, blendfile, section)
    filename = object

    bpy.ops.wm.append(filepath=filepath, filename=filename, directory=directory)

    # fake user so it doesn't get lost
    bpy.data.materials["Standard"].use_fake_user = True

def is_deleted_cronjob():
    """
    cronjob to check if a unit has been deleted from the scene and mark it for deletion
    """
    unit_ids = server_updaters.get_all_unit_ids()
    for obj in bpy.data.objects:
        if 'bitsquid_db_id' in obj:
            bitsquid_id = obj['bitsquid_db_id']
            if bitsquid_id in unit_ids:
                unit_ids.pop(unit_ids.index(bitsquid_id))

    server_updaters.mark_deleted_units(unit_ids)

    # runs only on first startup to clean up database
    if is_deleted_cronjob.startup:
        server_updaters.remove_deleted_units(unit_ids)
        is_deleted_cronjob.startup = False

    return 0.5

is_deleted_cronjob.startup = True

@persistent
def on_depsgraph_update(scene):
    """
    updates the database of units only if an obj is marked as a bitsquid unit
    """
    depsgraph = bpy.context.evaluated_depsgraph_get()
    object_update_queue = {}
    for update in depsgraph.updates:
        if update.is_updated_transform:
            obj = bpy.context.active_object
            if 'IDString64' in obj:
                object_update_queue[obj] = update

    # sorts out duplicates
    for obj,_ in object_update_queue.items():
        server_updaters.update_unit_in_db(obj, scene)

@persistent
def load_handler(dummy):
    import_template()

@persistent
def server_handler(dummy):
    """
    handler to start the fastapi server
    """
    server_thread = Thread(
        target=uvicorn.run,
        kwargs={
            'app':server.app,
            'host':"0.0.0.0",
            'port':2523
            })
    server_thread.daemon = True
    server_thread.start()


def register():
    from bpy.utils import register_class

    for cls in classes:
        register_class(cls)

    bpy.types.TOPBAR_MT_file_import.append(menu_func_import_bsi)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import_compiled_unit)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import_compiled_animation)
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import_compiled_material)

    bpy.types.Scene.bitsquid = PointerProperty(type=BitsquidSettings)
    bpy.types.Object.bitsquid = PointerProperty(type=BitsquidObjectSettings)
    bpy.types.Material.bitsquid = PointerProperty(type=BitsquidMaterialSettings)

    # reset rainbow table
    rainbow_table.resource_map = rainbow_table.RainbowTable()

    # we don't know where the user has installed this addon, so we have to look in all script paths
    possible_script_paths = script_paths(check_all=True)

    # find where this addon is located
    dict_path = None
    for path in possible_script_paths:
        possible_dict_path = os.path.join(path, "addons", "bitsquid", "murmur")
        if os.path.isfile(os.path.join(possible_dict_path, "rainbow_table.py")):
            dict_path = possible_dict_path
            break

    # if the addon folder is located
    if dict_path:
        # look for dictionary files
        dict_files = glob.glob(f"{dict_path}/dictionaries/dictionary_*")

        # if we have dictionary files, load rainbow table
        if len(dict_files) > 0:
            load_rainbow_table(dict_files)

    bpy.app.handlers.load_post.append(load_handler)
    bpy.app.handlers.load_post.append(server_handler)
    bpy.app.handlers.depsgraph_update_post.append(on_depsgraph_update)
    bpy.app.timers.register(is_deleted_cronjob)


def unregister():
    del bpy.types.Scene.bitsquid
    del rainbow_table.resource_map

    from bpy.utils import unregister_class

    for cls in reversed(classes):
        unregister_class(cls)

    bpy.app.handlers.load_post.remove(load_handler)
    bpy.app.handlers.load_post.remove(server_handler)
    bpy.app.handlers.depsgraph_update_post.remove(on_depsgraph_update)
    bpy.app.timers.unregister(is_deleted_cronjob)


if __name__ == "__main__":
    register()
