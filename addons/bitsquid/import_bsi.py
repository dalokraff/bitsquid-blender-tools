#    Bitsquid Blender Tools
#    Copyright (C) 2021  Lucas Schwiderski
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.


import sys
import zlib
import bpy
import math
import traceback

from mathutils import Vector, Matrix
from bpy_extras.io_utils import unpack_list
from bitsquid import sjson


def find(arr, f):
    """
    Find a value in a list by executing `f`
    on each item until it returns `true`.
    """
    for key, val in arr.items():
        if f(val, key):
            return val, key


def create_mesh(self, context, name, node_data, geo_data):
    """
    Create a Blender mesh object from a BSI node definition
    and additional data from the file.
    """

    # A list of 3-dimensional vectors. Each vector encodes a vertex position.
    vertices = []
    # A list of vectors, where each vector contains three indices into
    # `vertices`. Those three indices define the vertices that make up
    # the face.
    faces = []
    normals = []
    uv_name = "UVMap"
    uvs = []

    for i, index_stream in enumerate(geo_data["indices"]["streams"]):
        data_stream = geo_data["streams"][i]
        stride = data_stream["stride"] / 4

        for channel in data_stream["channels"]:
            stream_data = data_stream["data"]
            if channel["name"] == 'POSITION':
                # NOTE: Do we need to handle `stride != 3`?
                # Since the value seems to be fixed per stream, a higher
                # stride would only be possible for objects that can be built
                # entirely from quads, which is very uncommon.
                if stride != 3:
                    raise NotImplementedError("stride != 3 cannot be handled")

                # Get vertex positions.
                # Iterate over data in sets of three values. Each set
                # represents `x`, `y` and `z`.
                for j in range(0, len(stream_data), 3):
                    vertices.append(Vector((
                        stream_data[j],
                        stream_data[j + 1],
                        stream_data[j + 2],
                    )))

                # Get face definitions. Values are vertex indices.
                # Iteration works just like vertices above.
                # Vertex indices must be integers, so we cannot use
                # Vector().
                for j in range(0, len(index_stream), 3):
                    faces.append((
                        index_stream[j],
                        index_stream[j + 1],
                        index_stream[j + 2],
                    ))

                print(vertices)
                print(faces)
            elif channel["name"] == 'NORMAL':
                # Get normal vectors.
                tmp_normal_list = [stream_data[j:j + 3] for j in range(0, len(stream_data), 3)]
                # Iterate over faces and vertices to find corresponding
                # normal vector.
                for face_normal_ids in [index_stream[j:j + 3] for j in range(0, len(index_stream), 3)]:
                    for vtx_id in face_normal_ids:
                        normals.append(Vector((tmp_normal_list[vtx_id][0], tmp_normal_list[vtx_id][1], tmp_normal_list[vtx_id][2])))
            elif channel["name"] in {'ALPHA', 'COLOR'}:
                # Not sure if this can be intended as a primitive material,
                # but I'll assume it's not. And while Blender does feature
                # the concept of viewport-only vertex colors, that's rather
                # low priority to implement.
                # self.report({'INFO'}, "Ignoring vertex color data")
                continue
            elif channel["name"] == 'TEXCOORD':
                uv_name = "UVMap{}".format(channel["index"] + 1)
                uv_data = data_stream["data"]
                uv_defs = [uv_data[j:j+2] for j in range(0, len(uv_data), 2)]
                uvs = [uv_defs[index_stream[j]] for j in range(0, len(index_stream))]
            else:
                # TODO: Implement other channel types
                # self.report(
                #     {'WARNING'},
                #     "Unknown channel type: {}".format(channel["name"])
                # )
                continue

    mesh = bpy.data.meshes.new(name)
    mesh.from_pydata(vertices, [], faces)

    # Try to add split normal custom set to the mesh.
    try:
        mesh.normals_split_custom_set(normals)
        # Set smooth shading to true so we can see the effect of the normals.
        mesh.use_auto_smooth = True
    except RuntimeError as e:
        self.report({'WARNING'}, f"{e}on mesh {mesh.name}")

    if len(uvs) > 0:
        uv_layer = mesh.uv_layers.new(name=uv_name)
        uv_layer.data.foreach_set("uv", unpack_list(uvs))

    return mesh


def matrix_from_list(list):
    """
    Builds a square Matrix from a list of values in column order.

    When cross-referencing the `bsi_importer` and Maya's Python docs,
    it appears as though matrices stored in `.bsi` should be row ordered,
    but they are, in fact, column ordered.
    """
    stride = int(math.sqrt(len(list)))
    rows = []
    for i in range(stride):
        row = (list[i], list[i+stride], list[i+(stride*2)], list[i+(stride*3)])
        rows.append(row)

    return Matrix(rows)


def import_joint(
            self,
            context,
            name,
            node_data,
            armature,
            parent_bone,
            parent_rotation,
            global_data
        ):
    """
    Imports a joint and all of its children.
    In BSI (smilar to Maya) skeletons are defined as a series of joints,
    with bones added virtually in between, when needed.

    In Blender, skeletons are defined with bones, where each bone has a `head`
    and `tail`. So we need to convert the list of joints to a series of bones
    instead.
    The challenge here is the fact that joints have rotation data, whereas
    `head` and `tail` for bones don't. This changes how the position data has
    to be treated, as it is relative to the respective previous joint.
    Compared to the code that imports mesh objects, we can't just apply
    the matrix to the bone and have it position itself relative to its parent.
    Instead, we have to manually keep track of the parent's rotation.
    """
    if "local" not in node_data:
        raise RuntimeError("No position value for joint '{}'".format(name))

    mat = matrix_from_list(node_data["local"])
    translation, rotation, _ = mat.decompose()

    if name.endswith("_scale"):
        # print("Skipping joint '{}'".format(name))
        bone = parent_bone
    else:
        bone = armature.edit_bones.new(name)
        if parent_bone:
            # The values for `bone.head` and `bone.tail` are relative to their
            # parent, so we need to apply that first.
            bone.parent = parent_bone
            bone.use_connect = True
            # bone.head = parent_bone.tail
        else:
            bone.head = Vector((0, 0, 0))

        if parent_rotation:
            print("[import_joint] {} Parent @ Local:".format(name), parent_rotation, parent_rotation @ translation)
            bone.tail = bone.head + (parent_rotation @ translation)
        else:
            bone.tail = bone.head + translation

        print("[import_joint] {} Local:".format(name), translation)
        print("[import_joint] {} Bone:".format(name), bone.head, bone.tail)

    if "children" in node_data:
        for child_name, child_data in node_data["children"].items():
            if child_data["parent"] != name:
                raise RuntimeError(
                    "Assigned parent '{}' doesn't match actual parent node '{}'"
                    .format(child_data["parent"], name)
                )

            if child_name.startswith("j_"):
                child_bone = import_joint(
                    self,
                    context,
                    child_name,
                    child_data,
                    armature,
                    bone,
                    rotation,
                    global_data
                )
                child_bone.parent = bone
            else:
                # DEBUG: ignoring these for now
                continue
                # Not entirely sure, but I think these are considered
                # "controller nodes" in Maya. Would make sense based on
                # name.
                if "children" in child_data:
                    raise RuntimeError(
                        "Controller node '{}' has children."
                        .format(child_name)
                    )

                if "geometries" not in child_data:
                    raise RuntimeError(
                        "Controller node '{}' has no geometry."
                        .format(child_name)
                    )

                child_obj = import_node(
                    self,
                    context,
                    child_name,
                    child_data,
                    global_data,
                )
                # TODO: How to parent to a bone?
                child_obj.parent = bone

    return bone


def import_armature(self, context, name, node_data, global_data):
    armature = context.blend_data.armatures.new(name)

    # An armature itself cannot exist in the view layer.
    # We need to create an object from it
    obj = bpy.data.objects.new(name, armature)
    context.collection.objects.link(obj)

    # Armature needs to be in EDIT mode to allow adding bones
    context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode='EDIT', toggle=False)

    if "local" not in node_data:
        raise RuntimeError("No position value for joint '{}'".format(name))

    mat = matrix_from_list(node_data["local"])
    obj.matrix_local = mat
    # DEBUG
    mat_loc, mat_rot, mat_scale = mat.decompose()
    print("[import_joint] {}".format(name), mat_loc, mat_rot)

    if "children" in node_data:
        for child_name, child_data in node_data["children"].items():
            if child_data["parent"] != name:
                raise RuntimeError(
                    "Assigned parent '{}' doesn't match actual parent node '{}'"
                    .format(child_data["parent"], name)
                )

            if child_name.startswith("j_"):
                import_joint(
                    self,
                    context,
                    child_name,
                    child_data,
                    armature,
                    None,
                    None,
                    global_data
                )
            else:
                # DEBUG: Ignoring these for now
                continue
                # Not entirely sure, but I think these are considered
                # "controller nodes" in Maya. Would make sense based on
                # name.
                if "children" in child_data:
                    raise RuntimeError(
                        "Controller node '{}' has children."
                        .format(child_name)
                    )

                child_obj = import_node(
                    self,
                    context,
                    child_name,
                    child_data,
                    global_data,
                )
                child_obj.parent = obj

    # Disable EDIT mode
    context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode='OBJECT')

    return obj


def import_geometry(self, context, name, node_data, global_data):
    if len(node_data["geometries"]) > 1:
        self.report(
            {'WARNING'},
            "More than one geometry for node '{}'.".format(name)
        )

    geometry_name = node_data["geometries"][0]
    geo_data = global_data["geometries"][geometry_name]

    mesh = create_mesh(self, context, name, node_data, geo_data)
    obj = bpy.data.objects.new(mesh.name, mesh)
    obj.matrix_world = Matrix()

    # Check of a local offset
    if "local" in node_data:
        mat = matrix_from_list(node_data["local"])
        obj.matrix_local = mat

    # Recurse into child nodes and parent them to the current object
    if "children" in node_data:
        for child_name, child_data in node_data["children"].items():
            if child_data["parent"] != name:
                raise RuntimeError(
                    "Assigned parent '{}' doesn't match actual parent node '{}'"
                    .format(child_data["parent"], name)
                )

            child_obj = import_node(
                self,
                context,
                child_name,
                child_data,
                global_data,
            )

            if not isinstance(child_obj, bpy.types.Object):
                raise RuntimeError(
                    "Node of type '{}' cannot be child of a geometry node."
                    .format(type(child_obj))
                )

            child_obj.parent = obj

    # Make sure all objects are linked to the current collection.
    # Otherwise they won't show up in the outliner.
    collection = context.collection
    collection.objects.link(obj)
    return obj


def import_node(self, context, name, node_data, global_data):
    """Import a BSI node. Recurses into child nodes."""
    has_geometry = "geometries" in node_data
    is_joint = name in global_data["joint_index"]

    if is_joint:
        if has_geometry:
            raise RuntimeError("Found geometry data in joint '{}'".format(name))

        if not name.startswith("j_"):
            raise RuntimeError("Invalid name for joint: '{}".format(name))

        self.report({'INFO'}, "Creating Armature '{}'".format(name))
        return import_armature(self, context, name, node_data, global_data)

    if has_geometry:
        return import_geometry(self, context, name, node_data, global_data)
    else:
        # Only the root node should be left now.
        # It needs slightly different treatment compared to a regular geometry
        # node
        if name != "root_point":
            self.report({'WARNING'}, "Unknown kind of node: '{}'. Falling back to Empty.".format(name))

        obj = bpy.data.objects.new(name, None)
        # Decrease axis size to prevent overcrowding in the viewport
        obj.empty_display_size = 0.1

        if "children" in node_data:
            for child_name, child_data in node_data["children"].items():
                if child_data["parent"] != name:
                    raise RuntimeError(
                        "Assigned parent '{}' doesn't match actual parent node '{}'"
                        .format(child_data["parent"], name)
                    )

                child_obj = import_node(
                    self,
                    context,
                    child_name,
                    child_data,
                    global_data,
                )
                child_obj.parent = obj

        # Make sure all objects are linked to the current collection.
        # Otherwise they won't show up in the outliner.
        collection = context.collection
        collection.objects.link(obj)

        return obj


def load(self, context, filepath, *, relpath=None):
    try:
        with open(filepath, 'rb') as f:
            data = f.read()

            if data[:4] == b'bsiz':
                data = zlib.decompress(data[8:])

            data = data.decode("utf-8")
            global_data = sjson.loads(data)
    except Exception:
        self.report({'ERROR'}, "Failed to parse SJSON: {}".format(filepath))
        traceback.print_exc(file=sys.stderr)
        return {'CANCELLED'}

    # Nothing to do if there are no nodes
    if "nodes" not in global_data:
        self.report({'WARNING'}, "No nodes to import in {}".format(filepath))
        return {'CANCELLED'}

    # Build joint index
    joint_index = []
    if "skins" in global_data:
        for v in global_data["skins"].values():
            for joint in v["joints"]:
                name = joint["name"]
                if name in global_data["geometries"]:
                    self.report({'ERROR'}, "Found joint with mesh data.")
                    return {'CANCELLED'}

                if name not in joint_index:
                    joint_index.append(joint['name'])

    global_data["joint_index"] = joint_index

    for name, node_data in global_data["nodes"].items():
        import_node(self, context, name, node_data, global_data)

    return {'FINISHED'}
