"""
Helper functions
"""

import os
import sys
import importlib
import subprocess
from pathlib import Path


# https://blender.stackexchange.com/a/238995
def install_package(module_name: str):
    py_exec = str(sys.executable)
    # Get lib directory
    lib = Path(py_exec).parent.parent / "lib"
    # Ensure pip is installed
    subprocess.run([py_exec, "-m", "ensurepip", "--user"])
    # Update pip (not mandatory)
    # subprocess.call([py_exec, "-m", "pip", "install", "--upgrade", "pip" ])
    # Install packages
    result = subprocess.run(
        args=[py_exec, "-m", "pip", "install", f"--target={str(lib)}", module_name],
        check=True,
        capture_output=True,
    )
    return


def ensure_module(self, module_name: str) -> bool:
    # is module already imported?
    """
    if module_name in locals():
        return True
    """

    if module_name == "PIL":
        package_name = "pillow"
    else:
        package_name = module_name

    try:
        importlib.import_module(module_name)
        return True

    except ImportError:
        self.report(
            {"INFO"},
            f"Python module '{module_name}' cannot be imported: attempting to install now",
        )
        try:
            install_package(package_name)
            importlib.import_module(module_name)
            self.report(
                {"INFO"}, f"Python module '{module_name}' successfully installed"
            )
            return True
        except Exception as e:
            self.report(
                {"WARNING"},
                f"Python package '{package_name}' could not be installed:\n{e}\nTry running Blender as administrator?",
            )
            return False
