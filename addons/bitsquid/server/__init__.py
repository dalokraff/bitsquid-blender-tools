from fastapi import FastAPI
import bitsquid.server.units as units
from bitsquid.server import models
from bitsquid.server.database import engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

app.include_router(units.router)
