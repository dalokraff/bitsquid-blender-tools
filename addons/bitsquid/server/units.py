import bpy
from sqlalchemy.orm import Session
from fastapi import APIRouter, Depends

from bitsquid.server_updaters.dependancy import get_db
from bitsquid.server_updaters.unit.model import UnitModel

router = APIRouter(
    prefix="/units",
    tags=["units"]
    )

@router.get("/all")
def get_all(db: Session=Depends(get_db)):
    """get all Units"""
    results = UnitModel.get_all(db)
    return results

@router.get("/all_alive")
def get_alive(db: Session=Depends(get_db)):
    """get all Units that aren't marked for deletion"""
    results = UnitModel.get_all_non_deleted(db)
    return results

@router.get('/updated_after')
async def updated_after(time_after: float, time_before: float, db: Session=Depends(get_db)):
    """gets units after given time"""
    results = UnitModel.get_updated_between(time_after, time_before, db)
    return results

@router.get('/delete_marked_units')
async def delete_the_marked(db: Session=Depends(get_db)):
    """
    deletes all units that have been marked for deletion
    """
    deleted_ids = UnitModel.delete_marked_units(db)
    return deleted_ids