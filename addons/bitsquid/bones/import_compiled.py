import os
from typing import Optional
import bitsquid.config as c
import bitsquid.stingray as s
import bitsquid.deserializing as d


def BonesResourceDeserializer(self, data: bytes) -> s.BonesResource:
    """
    A .bones file only contains the bones that are specified for use in the corresponding
    state machine. A mesh may have more bones than are used in the state machine.
    """
    result = s.BonesResource()
    result.BoneCount = d.ReadUint32(data)
    result.LodCount = d.ReadUint32(data)
    # some .bones files are weird and have a single LOD, but no data
    if result.BoneCount == 0:
        if len(data) != 12:
            raise Exception(
                f"Bones file has 0 bones but length is {len(data)} instead of 12"
            )
        result.BoneNameHashes = []
        result.LodLevels = []
        result.BoneNames = []
        return result
    result.BoneNameHashes = [d.ReadIDString32(data) for i in range(result.BoneCount)]
    result.LodLevels = [d.ReadUint32(data) for i in range(result.LodCount)]
    ROF = data[c.offset : :]
    result.BoneNames = d.StringArrayDeserializer(ROF)
    # we should have the same number of hashes and names
    # if we don't, something vv bad has happened
    assert len(result.BoneNameHashes) == len(result.BoneNames)
    return result


def load(self, filepath: str) -> s.BonesResource:
    with open(filepath, "rb") as f:
        data = f.read()
    self.report({"INFO"}, f"Importing .bones file: {filepath}")

    c.offset = 0
    bones_res = BonesResourceDeserializer(self, data)

    return bones_res


def try_load(self, directory: str, name: str) -> Optional[s.BonesResource]:
    """
    Try to load the .bones file of the given name in the given folder
    """
    filepath = os.path.join(directory, f"{name}.bones")
    try:
        bones_res = load(self, filepath)
        return bones_res
    except Exception as e:
        self.report({"WARNING"}, f"Could not import .bones resource: {filepath}: {e}")
        return None
