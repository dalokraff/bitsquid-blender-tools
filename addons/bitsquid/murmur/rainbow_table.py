"""
Adapted from https://gitlab.com/lschwiderski/vt2_bundle_unpacker
murmur/rainbow_table.rs
"""

from bitsquid.murmur import murmurhash64 as m64
from bitsquid import config


class IDString32:
    # really just a uint32
    ID: int

    # debug_name: str
    def __init__(self, h: int | str):
        if isinstance(h, str):
            if len(h) != 8:
                raise Exception(f"IDString {h} is not 32 bits long")
            try:
                h = int(h, base=16)
            except ValueError as e:
                raise Exception(f"Cannot convert IDString {h} to int") from e

        if isinstance(h, int):
            if not 0 <= h <= 2**32 - 1:
                raise ValueError(f"{h} is out of range")
            self.ID = h
            if config.dump_idstrings:
                config.seen_idstring32.append(h)

        else:
            raise ValueError(f"{h} is not an integer or string")

    def __str__(self):
        # fixed width 8 character upper case hex string, no 0x prefix
        s = f"{self.ID:08X}"
        return s

    def __eq__(self, other):
        return self.ID == other.ID

    def __hash__(self):
        return self.ID

    def lookup(self):
        # tired of typing config.resource_map.trylookup_idstring()

        # check global setting
        assert config.GlobalSettings
        if not config.GlobalSettings.LOOKUP_IDSTRINGS:
            return str(self)

        return resource_map.trylookup_idstring(self)


class IDString64:
    # really a uint64
    ID: int

    # debug_name: str
    def __init__(self, h: int | str):
        if isinstance(h, str):
            if len(h) != 16:
                raise Exception(f"IDString {h} is not 64 bits long")
            try:
                h = int(h, base=16)
            except ValueError as e:
                raise Exception(f"Cannot convert IDString {h} to int") from e

        if isinstance(h, int):
            if not 0 <= h <= 2**64 - 1:
                raise ValueError(f"{h} is out of range")
            self.ID = h
            if config.dump_idstrings:
                config.seen_idstring64.append(h)

        else:
            raise ValueError(f"{h} is not an integer or string")

    def __str__(self):
        # fixed width 16 character upper case hex string, no 0x prefix
        s = f"{self.ID:016X}"
        return s

    def __eq__(self, other):
        return self.ID == other.ID

    def __hash__(self):
        return self.ID

    def lookup(self):
        # tired of typing config.resource_map.trylookup_idstring()

        # check global setting
        assert config.GlobalSettings
        if not config.GlobalSettings.LOOKUP_IDSTRINGS:
            return str(self)

        return resource_map.trylookup_idstring(self)


SEED = 0


class RainbowTable:
    long_map: dict
    short_map: dict

    def __init__(self):
        self.long_map = {}
        self.short_map = {}

    def add(self, _hash: int, s: str):
        self.long_map[_hash] = s
        self.short_map[_hash >> 32] = s

    def lookup(self, _hash: int) -> str | None:
        if _hash in self.long_map.keys():
            return self.long_map[_hash]
        else:
            return self.lookup_short(_hash)

    def lookup_short(self, _hash: int) -> str | None:
        if _hash in self.short_map.keys():
            return self.short_map[_hash]
        else:
            return None

    # use this one for actually adding strings
    def addstr(self, s: str):
        # turn string into list of ints
        key = [ord(char) for char in s]
        h = m64.hash64(key, SEED)
        # check for collision
        preexisting = self.lookup(h)
        if preexisting and preexisting != s:
            raise Exception(
                f'Murmur hash collision: "{s}" and "{self.lookup(h)}" both have same hash {h}'
            )
        # add hash and string to RainbowTable
        self.add(h, s)

    def trylookup_idstring(self, h: IDString32 | IDString64) -> str:
        # Takes an IDString32 or IDString64
        # looks up the given hash
        # if original string is found: returns original string
        # else returns the string of the same hash
        result = self.lookup(h.ID)
        if result == "":
            return ""
        if not result:
            return str(h)
        else:
            return result

    def trylookup_int(self, h: int, h_type: int) -> str:
        # look up an integer hash directly
        result = self.lookup(h)
        if result == "":
            return ""
        if not result:
            if h_type == 32:
                return f"{result:08X}"
            elif h_type == 64:
                return f"{result:016X}"
            else:
                raise Exception(f"Unknown hash length argument {h_type}")
        else:
            return result

    def trylookup_str(self, h: str) -> str:
        # look up the string of a hash
        # eg "0A3AA9B9E3C65505"
        # returns original string if lookup fails
        if len(h) != 8 and len(h) != 16:
            raise Exception(f"IDString {h} is not 32 or 64 bytes long")

        try:
            x = int(h, base=16)
        except ValueError as e:
            raise Exception(f"Cannot convert IDString {h} to int") from e

        result = self.lookup(x)
        if result or result == "":
            return result
        else:
            return h


# Murmur hash rainbow table
resource_map = RainbowTable()
