"""
Adapted from Darktide animation importing wip by https://github.com/ManShanko
Deserializes and imports compiled .animation files (extracted from bundles) into blender.
"""

import os
import math
from collections import OrderedDict
import bpy
from mathutils import Vector, Matrix, Quaternion, Euler
import bitsquid.config as c
import bitsquid.stingray as s
import bitsquid.deserializing as d
import bitsquid.bones.import_compiled as import_bones
from bitsquid.murmur.rainbow_table import IDString64


def try_find_object_by_id(id) -> bpy.types.Object | None:
    # find object in scene with given SceneGraphNodeIndex in its custom data
    for obj in bpy.data.objects:
        if "SceneGraphNodeIndex" in obj.data.keys():
            if obj.data["SceneGraphNodeIndex"] == id:
                return obj
    return None


# stingray::BeatTime
class BeatTime:
    T: float  # uint32
    ID: int  # uint32


# stingray::interleaved_animation::HeaderData


# stingray::InterleavedAnimationEvaluator::PosSample
class PosSample:
    T: float
    V: Vector  # vec3


# stingray::InterleavedAnimationEvaluator::RotSample
class RotSample:
    T: float
    Q: Quaternion


# stingray::InterleavedAnimationEvaluator::SclSample
class SclSample:
    T: float
    V: Vector  # vec3


# stingray::InterleavedAnimationEvaluator::Bone
class Bone:
    Pos: PosSample
    Rot: RotSample
    Scl: SclSample


# stingray::InterleavedAnimationEvaluator
class InterleavedAnimationEvaluator:
    T: float
    AtEnd: bool  # byte
    NumBones: int
    NumTriggers: int
    Triggers: list[int]  # up to 16


class Sample:
    Type: s.InterleavedAnimationItemType
    BoneID: int
    Time: float
    Data: Vector | Quaternion

    def __init__(
        self,
        _type: s.InterleavedAnimationItemType,
        bone_id: int,
        time: float,
        data: Vector | Quaternion,
    ):
        self.Type = _type
        self.BoneID = bone_id
        self.Time = time
        self.Data = data


class Animation:
    Name: str
    Header: s.AnimationResource
    Sync: list[tuple[Vector, Quaternion, Vector]]
    Data: list[Sample]

    SortedData: OrderedDict

    def sort_data(self):
        # organize time series data by bone id
        self.SortedData = OrderedDict()
        for sample in self.Data:
            # start list for bone if it doesn't exist
            if sample.BoneID not in self.SortedData.keys():
                self.SortedData[sample.BoneID] = {
                    "position": [],
                    "rotation": [],
                    "scale": [],
                }

            pair = (sample.Time, sample.Data)
            if (
                sample.Type == s.InterleavedAnimationItemType.UNPACKED_POSITION_KEY_ITEM
                or sample.Type == s.InterleavedAnimationItemType.POSITION_KEY_ITEM
            ):
                self.SortedData[sample.BoneID]["position"].append(pair)

            elif (
                sample.Type == s.InterleavedAnimationItemType.UNPACKED_ROTATION_KEY_ITEM
                or sample.Type == s.InterleavedAnimationItemType.ROTATION_KEY_ITEM
            ):
                self.SortedData[sample.BoneID]["rotation"].append(pair)

            elif (
                sample.Type == s.InterleavedAnimationItemType.UNPACKED_SCALE_KEY_ITEM
                or sample.Type == s.InterleavedAnimationItemType.SCALE_KEY_ITEM
            ):
                self.SortedData[sample.BoneID]["scale"].append(pair)

        # ensure samples are sorted by time
        for bone_dic in self.SortedData.values():
            bone_dic["position"].sort(key=lambda x: x[0])
            bone_dic["rotation"].sort(key=lambda x: x[0])
            bone_dic["scale"].sort(key=lambda x: x[0])

    def make_pose_location_keyframe(
        self, pose: bpy.types.Pose, bone_name: str, time: float, value: Vector
    ):
        bone = pose.bones[bone_name]
        t = time * c.AnimationSettings.FRAMERATE
        m = Matrix.LocRotScale(
            value * c.AnimationSettings.POSITION_UNIT_MULTIPLIER, None, None
        )
        if bone.parent:
            bone.matrix_basis = m
        else:
            bone.matrix_basis = m
        bone.keyframe_insert("location", frame=t)

    def make_pose_rotation_keyframe(
        self, pose: bpy.types.Pose, bone_name: str, time: float, value: Quaternion
    ):
        bone = pose.bones[bone_name]
        t = time * c.AnimationSettings.FRAMERATE
        # failed attempts to make Stingray animation data work in Blender
        # value.rotate(Matrix.Rotation(math.radians(90), 4, "Z"))
        # value.rotate(Matrix.Rotation(math.radians(90), 4, "Y"))
        # local_transform = Matrix.LocRotScale(None, value, None)
        # w,x,y,z = value.w, value.x, value.y, value.z
        # value.rotate(Matrix.Rotation(math.radians(-90), 4, (1,0,0)))
        # value = Quaternion((w,x,y,-z))
        # value.rotate(Matrix.Rotation(math.radians(90), 4, (1,0,0)))
        # value.rotate(Matrix.Rotation(math.radians(90), 4, (0,1,0)))
        if bone.parent:
            bone.rotation_quaternion = value
            # bone.matrix = bone.parent.matrix @ local_transform
        else:
            bone.rotation_quaternion = value
        # pose.bones[bone_name].rotation_quaternion = value
        bone.keyframe_insert("rotation_quaternion", frame=t)

    def make_pose_scale_keyframe(
        self, pose: bpy.types.Pose, bone_name: str, time: float, value: Vector
    ):
        bone = pose.bones[bone_name]
        t = time * c.AnimationSettings.FRAMERATE
        m = Matrix.LocRotScale(None, None, value)
        # pose.bones[bone_name].scale = value
        if bone.parent:
            bone.matrix_basis = m
        else:
            bone.matrix_basis = m
        bone.keyframe_insert("scale", frame=t)

    def make_matrix_keyframe(self, obj: bpy.types.Object, time: float, value: Matrix):
        t = time * c.AnimationSettings.FRAMERATE
        if type(obj) == bpy.types.PoseBone:
            obj.matrix = value
        else:
            obj.matrix_local = value
        obj.keyframe_insert("scale", frame=t)
        obj.keyframe_insert("rotation_quaternion", frame=t)
        obj.keyframe_insert("location", frame=t)

    def make_location_keyframe(self, obj: bpy.types.Object, time: float, value: Vector):
        t = time * c.AnimationSettings.FRAMERATE
        if type(obj) == bpy.types.PoseBone:
            # this part is the secret sauce to make Stingray animation data work in Blender
            if obj.parent:
                matrix_local = obj.parent.matrix.inverted() @ obj.matrix
            else:
                matrix_local = obj.matrix
            old_loc, _old_rot, _old_scl = matrix_local.decompose()
            loc_diff = value - old_loc
            obj.location = loc_diff * c.AnimationSettings.POSITION_UNIT_MULTIPLIER

        else:
            obj.location = value
        obj.keyframe_insert("location", frame=t)

    def make_rotation_keyframe(
        self, obj: bpy.types.Object, time: float, value: Quaternion
    ):
        t = time * c.AnimationSettings.FRAMERATE
        if type(obj) == bpy.types.PoseBone:
            # value.rotate(Euler((math.radians(180), math.radians(180), 0.0)))
            # this part is the secret sauce to make Stingray animation data work in Blender
            if obj.parent:
                matrix_local = obj.parent.matrix.inverted() @ obj.matrix
            else:
                matrix_local = obj.matrix
            _old_loc, old_rot, _old_scl = matrix_local.decompose()
            rot_diff = old_rot.rotation_difference(value)
            obj.rotation_quaternion = rot_diff

        else:
            obj.rotation_quaternion = value
        obj.keyframe_insert("rotation_quaternion", frame=t)

    def make_scale_keyframe(self, obj: bpy.types.Object, time: float, value: Vector):
        # is the value difference stuff needed for scale?
        t = time * c.AnimationSettings.FRAMERATE
        obj.scale = value
        obj.keyframe_insert("scale", frame=t)

    def animate_object(
        self, obj: bpy.types.Object | bpy.types.PoseBone, anim_bone_id: int
    ):
        samples = self.SortedData[anim_bone_id]

        # do sync
        sync_pos = self.Sync[anim_bone_id][0]
        sync_rot = self.Sync[anim_bone_id][1]
        sync_scl = self.Sync[anim_bone_id][2]

        self.make_scale_keyframe(obj, 0, sync_scl)
        for t, v in samples["scale"]:
            self.make_scale_keyframe(obj, t, v)

        self.make_rotation_keyframe(obj, 0, sync_rot)
        previous_rot = sync_rot
        for t, v in samples["rotation"]:
            # normalize current quaternion with previous one
            v = s.NormalizeRepresentation(previous_rot, v)
            self.make_rotation_keyframe(obj, t, v)
            previous_rot = v

        self.make_location_keyframe(obj, 0, sync_pos)
        for t, v in samples["position"]:
            self.make_location_keyframe(obj, t, v)

    def animate_bone(
        self, armature_obj: bpy.types.Object, name: str, anim_bone_id: int
    ):
        assert bpy.context.mode == "POSE"

        # ensure armature is currently active object
        bpy.context.view_layer.objects.active = armature_obj

        pose = bpy.context.object.pose

        # reset pose
        # bpy.ops.pose.select_all(action="SELECT")
        # bpy.ops.pose.loc_clear()
        # bpy.ops.pose.rot_clear()
        # bpy.ops.pose.scale_clear()

        bone = pose.bones[name]
        self.animate_object(bone, anim_bone_id)

        # reset pose
        # bpy.ops.pose.select_all(action="SELECT")
        # bpy.ops.pose.loc_clear()
        # bpy.ops.pose.rot_clear()
        # bpy.ops.pose.scale_clear()

        return

    def import_to_blender(self, bones_res: s.BonesResource):
        self.sort_data()

        pose = bpy.context.object.pose

        # we could theoretically use either the list of hashes or list of bone names from the .bones file.
        # if we use the list of hashes and feed it into the rainbow table, we might be more likely
        # to search for the actual names of the bones in blender
        canonical_bones_list = [i.lookup() for i in bones_res.BoneNameHashes]
        assert len(pose.bones) == len(canonical_bones_list)

        # do keyframes
        bones = self.SortedData.keys()

        if len(bones) > len(pose.bones):
            raise Exception(
                f"Animation has more bones ({len(bones)}) than the skeleton ({len(pose.bones)}): it cannot be for this unit"
            )

        # get current scene
        current_scene = bpy.context.window.scene
        # enable subframes
        current_scene.show_subframe = True
        # set num frames, start and end frames
        num_frames = math.ceil(self.Header.Length * c.AnimationSettings.FRAMERATE)
        bpy.context.scene.frame_start = 0
        if bpy.context.scene.frame_end < num_frames:
            bpy.context.scene.frame_end = num_frames
        # set animation fps
        bpy.context.scene.render.fps = c.AnimationSettings.FRAMERATE

        # get armature
        armature_obj = bpy.context.active_object
        new_animdata = armature_obj.animation_data_create()

        # make new action
        action_name = self.Name
        new_action = bpy.data.actions.new(action_name)
        new_animdata.action = new_action

        # switch to pose mode
        bpy.ops.object.mode_set(mode="POSE")

        # reset pose
        bpy.ops.pose.select_all(action="SELECT")
        bpy.ops.pose.loc_clear()
        bpy.ops.pose.rot_clear()
        bpy.ops.pose.scale_clear()

        for bone_id in bones:
            name = canonical_bones_list[bone_id]
            obj = pose.bones[name]

            self.animate_object(obj, bone_id)

        # reset pose
        bpy.ops.pose.select_all(action="SELECT")
        bpy.ops.pose.loc_clear()
        bpy.ops.pose.rot_clear()
        bpy.ops.pose.scale_clear()

        # leave pose mode
        bpy.ops.object.mode_set(mode="OBJECT")

        # set interpolation mode to bezier for all keyframes
        fcurves = [i for i in new_action.fcurves]
        for curve in fcurves:
            for point in curve.keyframe_points:
                point.interpolation = "BEZIER"

    def import_simple_to_blender(
        self, simple_animation_group: list[int], unit_res: s.UnitResource
    ):
        """
        simple_animation_group: list of the names of the nodes in the simple animation
        objects: list of the blender objects in the simple anim
        self.SortedData.keys(): list of the joint indices in the simple animation data.
            has separate numbering from simple_animation_group, but is in the same order
        """
        if not simple_animation_group:
            raise Exception("No simple animation group supplied")

        self.sort_data()

        node_data = unit_res.SceneGraph.Nodes

        # get current scene
        current_scene = bpy.context.window.scene
        # enable subframes
        current_scene.show_subframe = True
        # set num frames, start and end frames
        num_frames = math.ceil(self.Header.Length * c.AnimationSettings.FRAMERATE)
        bpy.context.scene.frame_start = 0
        if bpy.context.scene.frame_end < num_frames:
            bpy.context.scene.frame_end = num_frames
        # set animation fps
        bpy.context.scene.render.fps = c.AnimationSettings.FRAMERATE

        # get list of imported objects that are used by the animation
        animated_imported_objects = [
            c.imported_objects.Objects[i] for i in simple_animation_group
        ]
        # get list of references to actual objects
        # actual_objects = c.imported_objects.get_reference_list()
        actual_objects = [
            c.imported_objects.try_find_object(i.Name) if i else None
            for i in animated_imported_objects
        ]

        assert (
            len(animated_imported_objects)
            == len(actual_objects)
            == len(simple_animation_group)
            == len(self.SortedData.keys())
        )

        # have to make a new action for each animated object
        actions = []
        for i in range(len(animated_imported_objects)):
            imported_obj = animated_imported_objects[i]
            if not imported_obj:
                continue
            name = imported_obj.Name
            actual_obj = actual_objects[i]
            if not actual_obj:
                raise Exception("Experimenting")
            new_action_name = f"{self.Name} {name}"
            new_action = bpy.data.actions.new(new_action_name)
            actions.append(new_action)
            new_animdata = actual_obj.animation_data_create()
            new_animdata.action = new_action

        # do keyframes
        bones_to_animate = []
        for i in range(len(animated_imported_objects)):
            imported_obj = animated_imported_objects[i]
            if not imported_obj:
                continue
            actual_obj = actual_objects[i]
            if imported_obj.Type == s.ObjectType.Object:
                self.animate_object(actual_obj, i)
            elif imported_obj.Type == s.ObjectType.Bone:
                # batch bones together so we don't have to switch modes often
                bones_to_animate.append((actual_obj, imported_obj.Name, i))

        if len(bones_to_animate) > 0:
            # switch to pose mode
            bpy.ops.object.mode_set(mode="POSE")

            for bone in bones_to_animate:
                self.animate_bone(bone[0], bone[1], bone[2])

            # leave pose mode
            bpy.ops.object.mode_set(mode="OBJECT")

        # set interpolation mode to bezier for all keyframes
        for action in actions:
            fcurves = [i for i in action.fcurves]
            for curve in fcurves:
                for point in curve.keyframe_points:
                    point.interpolation = "BEZIER"
        return


# based on stingray::InterleavedAnimationEvaluator::advance_one()
def advance_one(self, data: bytes) -> Sample | str:
    short1 = d.ReadUint16(data)
    # first two bits are for packed data
    # if first two bits are not zero, only look at first two bits
    if short1 & 0xC000 != 0:
        item_type = s.InterleavedAnimationItemType(short1 & 0xC000)
    else:
        item_type = s.InterleavedAnimationItemType(short1)

    if item_type.value > 6:  # packed data
        # type, bone id, time are stored in 32 bits total (2 shorts)
        # item_type (2 bits) | bone_id (10 bits) | time (20 bits)
        short2 = d.ReadUint16(data)
        combined = (short1 << 16) | short2
        # next 10 bits are bone id
        bone_id = (combined >> 20) & 0x3FF
        # assert bone_id < num_bones
        # next 20 bits are time
        time = (combined & 0xFFFFF) * 0.001
        # assert time >= 0
        # assert time <= Length

        match item_type:
            case s.InterleavedAnimationItemType.SCALE_KEY_ITEM:  # packed scale 16384
                vec = d.Read161616Vec(data)
                result = Sample(item_type, bone_id, time, vec)
                return result
            case (
                s.InterleavedAnimationItemType.POSITION_KEY_ITEM
            ):  # packed position 32768
                vec = d.Read161616Vec(data)
                result = Sample(item_type, bone_id, time, vec)
                return result
            case (
                s.InterleavedAnimationItemType.ROTATION_KEY_ITEM
            ):  # packed rotation 49152
                quat = d.ReadPackedQuat(data)
                result = Sample(item_type, bone_id, time, quat)
                return result
            case _:
                raise Exception(
                    f"Unknown packed animation stream item type {item_type}"
                )

    else:  # unpacked data
        match item_type:
            case s.InterleavedAnimationItemType.UNPACKED_SCALE_KEY_ITEM:  # 6
                bone_id = d.ReadUint16(data)
                time = d.ReadSingle(data)
                vec = d.ReadVector3(data)
                result = Sample(item_type, bone_id, time, vec)
                return result
            case s.InterleavedAnimationItemType.TRIGGER_ITEM:  # 2
                bone_id = d.ReadUint32(data)
                time = d.ReadSingle(data)
                # can't do anything with this in blender?
                return "Trigger"
            case s.InterleavedAnimationItemType.FOOTER_ITEM:  # 3 EOF
                return "EOF"
            case s.InterleavedAnimationItemType.UNPACKED_POSITION_KEY_ITEM:  # 4
                bone_id = d.ReadUint16(data)
                time = d.ReadSingle(data)
                vec = d.ReadVector3(data)
                result = Sample(item_type, bone_id, time, vec)
                return result
            case s.InterleavedAnimationItemType.UNPACKED_ROTATION_KEY_ITEM:  # 5
                bone_id = d.ReadUint16(data)
                time = d.ReadSingle(data)
                quat = d.ReadQuaternion(data)
                result = Sample(item_type, bone_id, time, quat)
                return result
            case _:
                raise Exception(f"Unknown animation stream item type {item_type}")


# based on stingray::InterleavedAnimationEvaluator::advance_one()
def parse_animation(self, data: bytes) -> Animation:
    MAX_ANIM_LENGTH = 250.0
    # Header: 20 bytes total
    header = s.AnimationResource()
    # Is this identifier ever different?
    header.HeaderIdentifier = s.InterleavedAnimationItemType(d.ReadUint32(data))
    if header.HeaderIdentifier != s.InterleavedAnimationItemType.HEADER_ITEM:  # 0
        raise Exception("Wrong version or not a compiled .animation file")
    # Is num_bones always less than 256? bone_id is stored in
    # 10 bits in packed data, so max could be 1023?
    # BSIs use joints instead of bones: does that change when compiling?
    header.NumBones = d.ReadUint32(data)
    # What is the maximum duration of an animation?
    header.Length = d.ReadSingle(data)
    assert 0.0 <= header.Length <= MAX_ANIM_LENGTH
    # Total size of animation file in bytes
    header.Size = d.ReadUint32(data)
    # What are beats?
    # num_beats is usually 0, but sometimes 3 or 8
    header.NumBeats = d.ReadUint32(data)

    result = Animation()
    result.Header = header

    # quantization?
    for _ in range(header.NumBeats):
        time_slice = d.ReadSingle(data)
        _quant = d.ReadSingle(data)
        # should it be between 0 and maximum animation length?
        # sometimes it's greater than header.Length
        # assert 0.0 <= time_slice and time_slice <= header.Length
        assert 0.0 <= time_slice <= MAX_ANIM_LENGTH

    result.Sync = []
    # what does sync do
    # stingray::InterleavedAnimationEvaluator::reset
    sync = s.InterleavedAnimationItemType(d.ReadUint16(data))
    if sync == s.InterleavedAnimationItemType.SYNC_ITEM and header.NumBones:  # 1
        for _ in range(header.NumBones):
            pos = d.Read161616Vec(data)
            rot = d.ReadPackedQuat(data)
            scl = d.Read161616Vec(data)
            result.Sync.append((pos, rot, scl))
    elif (
        sync == s.InterleavedAnimationItemType.UNPACKED_SYNC_ITEM and header.NumBones
    ):  # 7
        for _ in range(header.NumBones):
            # order is different here for some reason?
            pos = d.ReadVector3(data)
            rot = d.ReadQuaternion(data)
            scl = d.ReadVector3(data)
            result.Sync.append((pos, rot, scl))
    else:
        raise NotImplementedError(f"Unknown sync item {sync}")

    # now the actual data
    result.Data = []
    while c.offset < len(data):
        sample = advance_one(self, data)
        if sample == "Trigger":
            continue
        if sample == "EOF":
            break
        else:
            assert isinstance(sample, Sample)
            result.Data.append(sample)
            # self.report({'INFO'}, f"Sample bone_id {sample.BoneID} time {sample.Time:.02f} type {sample.Type}")

    if c.offset != len(data):
        raise Exception(
            f"Finished deserializing at offset {c.offset}, but file length is {len(data)}"
        )
    return result


def load(self, filepath, **kwargs) -> Animation:
    with open(filepath, "rb") as f:
        data = f.read()
    filename = os.path.splitext(os.path.basename(filepath))[0]
    self.report({"INFO"}, f"Importing .animation file: {filepath}")
    c.offset = 0
    animation_data = parse_animation(self, data)
    animation_data.Name = IDString64(filename).lookup()
    return animation_data


def try_import(self, filepath: str, **kwargs) -> set:
    try:
        animation = load(self, filepath, **kwargs)

        # poll already ensures currently selected object is an armature
        current_obj = bpy.context.object
        # armature must have a skeleton to apply animation data from a full animation
        if "SkeletonName" not in current_obj:
            raise Exception(
                f"Selected armature {current_obj.name} does not have a SkeletonName"
            )
        if current_obj["SkeletonName"] == "0" * 16:
            raise Exception(
                f"Selected armature {current_obj.name} has no default skeleton"
            )
        skeleton_name = current_obj["SkeletonName"]
        current_dir = os.path.dirname(filepath)
        bones_res = import_bones.try_load(self, current_dir, skeleton_name)
        # we must have a bones resource
        if not bones_res:
            raise Exception("No bone data supplied")

        animation.import_to_blender(bones_res=bones_res)

        return {"FINISHED"}

    except Exception as e:
        self.report({"WARNING"}, f"Could not import .animation file: {filepath}: {e}")
        return {"CANCELLED"}


def import_simple(self, data: bytes, group: list[int], unit_res: s.UnitResource):
    # simple animation data
    c.offset = 0
    animation_data = parse_animation(self, data)
    animation_data.Name = "SimpleAnim"

    # where should we iterate over simple anim groups?
    # do only one for now
    animation_data.import_simple_to_blender(group, unit_res)
