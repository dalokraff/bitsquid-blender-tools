"""
Common functions used for reading binary files.
Also blender reporting
"""

import struct
import math
from mathutils import Vector, Matrix, Quaternion
import bitsquid.config as config
import bitsquid.stingray as stingray
from bitsquid.murmur.rainbow_table import IDString32, IDString64


def ReadVector(self, data: bytes, func) -> list:
    # generic vector
    result = []
    count = ReadUint32(data)
    for _ in range(count):
        result.append(func(self, data))
    return result


def ReadByte(data) -> int:
    # read a single byte (as int)
    (result,) = struct.unpack_from("<B", data, config.offset)
    config.offset += 1
    return result


def ReadBytes(data, length: int) -> bytes:
    # read `length` number of bytes
    result = data[config.offset : config.offset + length]
    config.offset += length
    return result


def ReadUint16(data) -> int:
    # read unsigned 16 bit integer (2 bytes) AKA unsigned short
    (result,) = struct.unpack_from("<H", data, config.offset)
    config.offset += 2
    return result


def ReadUint32(data) -> int:
    # read unsigned 32 bit integer (4 bytes) AKA regular unsigned int
    (result,) = struct.unpack_from("<I", data, config.offset)
    config.offset += 4
    return result


def ReadUint64(data) -> int:
    # read unsigned 64 bit integer (8 bytes) AKA unsigned long long
    (result,) = struct.unpack_from("<Q", data, config.offset)
    config.offset += 8
    return result


def ReadSingle(data) -> float:
    # read single precision (32 bit) float (4 bytes)
    (result,) = struct.unpack_from("<f", data, config.offset)
    config.offset += 4
    return result


def ReadFloatArray(data: bytes) -> list[float]:
    result = []
    count = ReadUint32(data)
    for _ in range(count):
        result.append(ReadSingle(data))
    return result


def ReadVector3(data) -> Vector:
    # read 3 floats (12 bytes)
    result = Vector(
        (ReadSingle(data), ReadSingle(data), ReadSingle(data))  # X  # Y  # Z
    )
    return result


def ReadVector3Array(data) -> list[Vector]:
    result = []
    count = ReadUint32(data)
    for _ in range(count):
        result.append(ReadVector3(data))
    return result


def ReadVector2(data) -> Vector:
    # read 2 floats (8 bytes)
    result = Vector(
        (
            ReadSingle(data),  # X
            ReadSingle(data),  # Y
        )
    )
    return result


def ReadMatrix4x4(data) -> Matrix:
    # read 16 floats (64 bytes)
    # Stingray matrices are stored in column-major order
    # mathutils.Matrix needs a list of rows
    # eg M23 is column 2, row 3
    M11 = ReadSingle(data)
    M12 = ReadSingle(data)
    M13 = ReadSingle(data)
    M14 = ReadSingle(data)

    M21 = ReadSingle(data)
    M22 = ReadSingle(data)
    M23 = ReadSingle(data)
    M24 = ReadSingle(data)

    M31 = ReadSingle(data)
    M32 = ReadSingle(data)
    M33 = ReadSingle(data)
    M34 = ReadSingle(data)

    M41 = ReadSingle(data)
    M42 = ReadSingle(data)
    M43 = ReadSingle(data)
    M44 = ReadSingle(data)

    result = Matrix(
        (
            [M11, M21, M31, M41],
            [M12, M22, M32, M42],
            [M13, M23, M33, M43],
            [M14, M24, M34, M44],
        )
    )
    return result


def ReadMatrix3x3(data) -> Matrix:
    # read 9 floats (36 bytes)
    # actually returns a Matrix4x4
    M11 = ReadSingle(data)
    M12 = ReadSingle(data)
    M13 = ReadSingle(data)
    M14 = float(0)

    M21 = ReadSingle(data)
    M22 = ReadSingle(data)
    M23 = ReadSingle(data)
    M24 = float(0)

    M31 = ReadSingle(data)
    M32 = ReadSingle(data)
    M33 = ReadSingle(data)
    M34 = float(0)

    M41 = float(0)
    M42 = float(0)
    M43 = float(0)
    M44 = float(1)

    result = Matrix(
        (
            [M11, M21, M31, M41],
            [M12, M22, M32, M42],
            [M13, M23, M33, M43],
            [M14, M24, M34, M44],
        )
    )
    return result


def ReadQuaternion(data: bytes) -> Quaternion:
    # read a quaternion (4 single precision floats)
    x = ReadSingle(data)
    y = ReadSingle(data)
    z = ReadSingle(data)
    w = ReadSingle(data)
    # Stingray uses x,y,z,w format
    # the mathutils library uses w,x,y,z format!
    # TODO a normalisation step is sometimes done here
    return Quaternion(Vector((w, x, y, z)))


def ReadBool(data) -> bool:
    # read a single byte and return a bool
    byte = ReadByte(data)
    match byte:
        case 0:
            return False
        case 1:
            return True
        case _:
            raise Exception(f"Can't interpret value {byte} as bool")


def ReadByteArray(data) -> bytes:
    # read a uint32 `size`
    # `size` is the number of bytes to read after that
    size = ReadUint32(data)
    if size > 0:
        result = data[config.offset : config.offset + size]
        config.offset += size
        return result
    else:
        return bytes()


def ReadIntArray(data) -> list[int]:
    # read a uint32 `count`
    # read `count` uint32s after that
    count = ReadUint32(data)
    result = []
    for i in range(count):
        result.append(ReadUint32(data))
    return result


def ReadIDString32(data) -> IDString32:
    # read and return an IDString32 object (4 bytes)
    result = IDString32(ReadUint32(data))
    return result


def ReadIDString64(data) -> IDString64:
    # read and return an IDString64 object (8 bytes)
    result = IDString64(ReadUint64(data))
    return result


def ReadIDString32Array(data: bytes) -> list[IDString32]:
    result = []
    count = ReadUint32(data)
    for _ in range(count):
        result.append(ReadIDString32(data))
    return result


def ReadIDString64Array(data: bytes) -> list[IDString64]:
    result = []
    count = ReadUint32(data)
    for _ in range(count):
        result.append(ReadIDString64(data))
    return result


def StringArrayDeserializer(data) -> list[str]:
    # reads an array of null-terminated strings
    # use `.removesuffix("\0")` to remove trailing null character
    strings = data.decode("utf-8").removesuffix("\0").split("\0")
    return strings


def Read161616Vec(data: bytes) -> Vector:
    # https://bitsquid.blogspot.com/2009/11/bitsquid-low-level-animation-system.html
    # stingray can store a Vector3 in a packed form (6 bytes)
    # animation vec3s are stored in 16*3 = 48 bits
    # each 16 bits represents a value in the range -10 m to 10 m
    scale_factor = 20 / (2**16)  # value of each 'step' within the range
    x = ReadUint16(data) * scale_factor - 10
    y = ReadUint16(data) * scale_factor - 10
    z = ReadUint16(data) * scale_factor - 10
    return Vector((x, y, z))


def ReadPackedQuat(data: bytes) -> Quaternion:
    # packed quaternions are stored in 4 bytes
    dword = ReadUint32(data)
    # A compressed quaternion only has the smallest 3 components
    # We have to reconstruct the largest component
    quat = [None, None, None, None]
    biggest_comp = dword & 3
    comp1 = (((dword >> 2) & 0x3FF) * 0.0014648438) - 0.75
    comp2 = (((dword >> 12) & 0x3FF) * 0.0014648438) - 0.75
    comp3 = ((dword >> 22) * 0.0014648438) - 0.75
    quat[biggest_comp] = math.sqrt(1 - comp1**2 - comp2**2 - comp3**2)
    quat[(biggest_comp + 1) & 3] = comp1
    quat[(biggest_comp - 2) & 3] = comp2
    quat[(biggest_comp - 1) & 3] = comp3
    # TODO a normalisation step is sometimes done here
    # Stingray uses x,y,z,w format
    # the mathutils library uses w,x,y,z format!
    return Quaternion(Vector((quat[3], quat[0], quat[1], quat[2])))


def l_info(bself, msg, l_offset=None):
    if l_offset:
        bself.report({"INFO"}, f"{l_offset}\t{msg}")
    else:
        bself.report({"INFO"}, msg)


def l_debug(bself, msg, l_offset=None):
    if l_offset:
        bself.report({"INFO"}, f"{l_offset}\t{msg}")
    else:
        bself.report({"INFO"}, msg)


def l_warning(bself, msg, l_offset=None):
    if l_offset:
        bself.report({"WARNING"}, f"{l_offset}\t{msg}")
    else:
        bself.report({"WARNING"}, msg)
