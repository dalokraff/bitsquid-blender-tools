"""
Adapted from https://github.com/Unordinal/VT2AssetLib
Deserializes and imports compiled .unit files (extracted from bundles) into blender.
Tries to determine object and bone true names from the murmur hash rainbow table.
"""

import os
import sys
from math import radians
import bpy
import traceback
from enum import Enum
import bitsquid.config as c
import bitsquid.stingray as s
import bitsquid.deserializing as d
import bitsquid.bones.import_compiled as import_bones
import bitsquid.animation.import_compiled as import_animation
import bitsquid.state_machine.import_compiled as import_state_machine
from mathutils import Vector, Matrix, Euler
import bitsquid.material.import_compiled as import_material
from bitsquid.murmur.rainbow_table import IDString32, IDString64, resource_map
from bitsquid.server_updaters import add_unit_to_db


def HasSkinData(mesh_obj: s.MeshObject, mesh_geos: list[s.MeshGeometry]) -> bool:
    return (mesh_obj.SkinIndex - len(mesh_geos)) > 0


def ChannelDeserializer(self, data: bytes) -> s.Channel:
    self.report({"DEBUG"}, "Deserializing channel...")
    result = s.Channel()
    result.Component = s.VertexComponent(d.ReadUint32(data))
    result.Type = s.ChannelType(d.ReadUint32(data))
    result.Set = d.ReadUint32(data)
    result.Stream = d.ReadUint32(data)
    result.IsInstance = d.ReadByte(data)
    self.report(
        {"DEBUG"},
        f"Finished deserializing channel. component={result.Component} channeltype={result.Type}",
    )
    return result


def IndexBufferDeserializer(self, data: bytes) -> s.IndexBuffer:
    self.report({"DEBUG"}, "Deserializing index buffer...")
    result = s.IndexBuffer()
    result.Validity = s.Validity(d.ReadUint32(data))
    result.StreamType = s.StreamType(d.ReadUint32(data))
    result.IndexFormat = s.IndexFormat(d.ReadUint32(data))
    result.IndexCount = d.ReadUint32(data)
    indexBytesLength = d.ReadUint32(data)
    result.Data = d.ReadBytes(data, indexBytesLength)
    self.report(
        {"DEBUG"},
        f"Finished deserializing index buffer. validity={result.Validity} streamtype={result.StreamType} indexformat={result.IndexFormat} indexcount={result.IndexCount}",
    )
    return result


def VertexBufferDeserializer(self, data: bytes) -> list[s.VertexBuffer]:
    # deserializes `vertexBufferCount` number of vertex buffers
    result = []
    vertexBufferCount = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing [{vertexBufferCount}] vertex buffer(s)...")
    for i in range(vertexBufferCount):
        VB = s.VertexBuffer()
        dataLength = d.ReadUint32(data)
        assert dataLength >= 0
        VB.Data = d.ReadBytes(data, dataLength)  # read `dataLength` number of bytes
        VB.Validity = s.Validity(d.ReadUint32(data))
        VB.StreamType = s.StreamType(d.ReadUint32(data))
        VB.Count = d.ReadUint32(data)
        VB.Stride = d.ReadUint32(data)
        result.append(VB)
        self.report(
            {"DEBUG"},
            f"Finished deserializing vertex buffer. datalength={dataLength} validity={VB.Validity} streamtype={VB.StreamType}",
        )
    channelCount = d.ReadUint32(data)
    assert channelCount == vertexBufferCount
    self.report({"DEBUG"}, f"Deserializing [{channelCount}] channel(s)...")
    for i in range(channelCount):
        result[i].Channel = ChannelDeserializer(self, data)
    self.report({"DEBUG"}, f"Finished deserializing [{len(result)}] vertex buffers")
    return result


def BatchRangeArrayDeserializer(self, data: bytes) -> list[s.BatchRange]:
    result = []
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing batch range array of length [{count}]...")
    for i in range(count):
        BR = s.BatchRange()
        BR.MaterialIndex = d.ReadUint32(data)
        BR.Start = d.ReadUint32(data)
        BR.Size = d.ReadUint32(data)
        BR.BoneSet = d.ReadUint32(data)
        result.append(BR)
    self.report({"DEBUG"}, f"Finished deserializing {len(result)} batch ranges")
    return result


# stingray::BoundingVolume::serialize
def BoundingVolumeDeserializer(self, data: bytes) -> s.BoundingVolume:
    self.report({"DEBUG"}, "Deserializing bounding volume...")
    result = s.BoundingVolume()
    result.LowerBounds = d.ReadVector3(data)
    result.UpperBounds = d.ReadVector3(data)
    result.Radius = d.ReadSingle(data)
    self.report({"DEBUG"}, "Finished deserializing bounding volume")
    return result


def IDString32ArrayDeserializer(self, data: bytes) -> list[IDString32]:
    result = []
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing hash array of length [{count}]...")
    for i in range(count):
        result.append(d.ReadIDString32(data))
    self.report({"DEBUG"}, f"Finished deserializing {len(result)} hashes")
    return result


def MeshGeometryArrayDeserializer(self, data: bytes) -> list[s.MeshGeometry]:
    result = []
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing mesh geometry array of length [{count}]...")
    for _ in range(count):
        MG = s.MeshGeometry()
        MG.VertexBuffers = VertexBufferDeserializer(self, data)
        MG.IndexBuffer = IndexBufferDeserializer(self, data)
        MG.BatchRanges = BatchRangeArrayDeserializer(self, data)
        MG.BoundingVolume = BoundingVolumeDeserializer(self, data)
        MG.Materials = IDString32ArrayDeserializer(self, data)
        result.append(MG)
    self.report(
        {"DEBUG"},
        f"Finished deserializing mesh geometry array of length [{len(result)}]",
    )
    return result


def SkinDataArrayDeserializer(self, data: bytes) -> list[s.SkinData]:
    sd_result = []
    sd_count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing skin data array of length [{sd_count}]...")
    for i in range(sd_count):
        SD = s.SkinData()
        # get inverse bind matrices
        ibmCount = d.ReadUint32(data)
        self.report({"DEBUG"}, f"Deserializing [{ibmCount}] inverse bind matrices...")
        ibm_result = []
        for j in range(ibmCount):
            ibm = d.ReadMatrix4x4(data)
            ibm_result.append(ibm)
        # get node indices
        ni_count = d.ReadUint32(data)
        self.report({"DEBUG"}, f"Deserializing [{ni_count}] node indices...")
        ni_result = []
        for j in range(ni_count):
            ni = d.ReadUint32(data)
            ni_result.append(ni)
        # get matrix index sets
        mis_count = d.ReadUint32(data)
        self.report({"DEBUG"}, f"Deserializing [{mis_count}] matrix index sets...")
        mis_result = []
        for j in range(mis_count):
            # get setindexcount
            sic_count = d.ReadUint32(data)
            set_result = []
            for k in range(sic_count):
                set_result.append(d.ReadUint32(data))
            mis_result.append(set_result)
        SD.InvBindMatrices = ibm_result
        SD.NodeIndices = ni_result
        SD.MatrixIndexSets = mis_result
        sd_result.append(SD)
    self.report(
        {"DEBUG"},
        f"Finished deserializing skin data array of length [{len(sd_result)}]",
    )
    return sd_result


# stingray::Vector<stingray::Pair<stingray::IdString32,stingray::Vector<int>,0,1>>::serialize
def SimpleAnimationGroupArrayDeserializer(
    self, data: bytes
) -> list[s.SimpleAnimationGroup]:
    saga_result = []
    sag_count = d.ReadUint32(data)
    self.report(
        {"DEBUG"},
        f"Deserializing simple animation group array of length [{sag_count}]...",
    )
    for i in range(sag_count):
        SAG = s.SimpleAnimationGroup()
        SAG.Name = d.ReadIDString32(data)
        # group data now
        gd_count = d.ReadUint32(data)
        gd_result = []
        for j in range(gd_count):
            # is this right? What is a DelegateSerializer
            gd_result.append(d.ReadUint32(data))
        SAG.Data = gd_result
        saga_result.append(SAG)
    self.report(
        {"DEBUG"},
        f"Finished deserializing simple animation group array of length [{len(saga_result)}]",
    )
    return saga_result


def SceneGraphDeserializer(self, data: bytes) -> s.SceneGraph:
    result = s.SceneGraph()
    nodeCount = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing scene graph with [{nodeCount}] nodes...")
    # do NOT do it this way, use list comprehension instead!
    # result.Nodes = [SceneGraphData()] * nodeCount
    result.Nodes = [s.SceneGraphData() for i in range(nodeCount)]
    for i in range(nodeCount):
        rotation = d.ReadMatrix3x3(data)
        position = d.ReadVector3(data)
        scale = d.ReadVector3(data)
        result.Nodes[i].LocalTransform = s.GetTRSMatrixM(position, rotation, scale)

    for node in result.Nodes:
        node.WorldTransform = d.ReadMatrix4x4(data)
    for node in result.Nodes:
        node.ParentType = s.ParentType(d.ReadUint16(data))
        node.ParentIndex = d.ReadUint16(data)
    for node in result.Nodes:
        node.Name = d.ReadIDString32(data)
    self.report(
        {"DEBUG"},
        f"Finished deserializing scene graph with [{len(result.Nodes)}] nodes",
    )
    return result


def MeshObjectsArrayDeserializer(self, data: bytes) -> list[s.MeshObject]:
    me_count = d.ReadUint32(data)
    self.report(
        {"DEBUG"}, f"Deserializing mesh objects array of length [{me_count}]..."
    )
    result = [s.MeshObject() for i in range(me_count)]
    for i in range(me_count):
        result[i].Name = d.ReadIDString32(data)
        result[i].NodeIndex = d.ReadUint32(data)
        result[i].GeometryIndex = d.ReadUint32(data)
        result[i].SkinIndex = d.ReadUint32(data)
        # TODO decompose into actual flags?
        # result[i].Flags = s.RenderableFlags(d.ReadUint32(data))
        result[i].Flags = d.ReadUint32(data)
        result[i].BoundingVolume = BoundingVolumeDeserializer(self, data)
    self.report(
        {"DEBUG"},
        f"Finished deserializing mesh objects array of length [{len(result)}]",
    )
    return result


# stingray::ActorResource::Shape::serialize
# stingray::Vector<stingray::ActorResource::Shape>::serialize
def ShapeArrayDeserializer(self, data: bytes) -> list[s.ActorResource.Shape]:
    # TODO finish implementing
    count = d.ReadUint32(data)
    result = [s.ActorResource.Shape() for i in range(count)]
    for i in range(count):
        result[i].Type = s.ShapeType(d.ReadUint32(data))
        result[i].Material = d.ReadIDString32(data)
        result[i].ShapeTemplate = d.ReadIDString32(data)
        result[i].LocalTM = d.ReadMatrix4x4(data)
        result[i].ShapeData = d.ReadByteArray(data)
        result[i].ShapeNode = d.ReadIDString32(data)
        t = result[i].Type
        match t:
            case s.ShapeType.BoxShape:  # 1
                d.ReadVector3(data)
            case s.ShapeType.CapsuleShape:  # 2
                d.ReadUint32(data)  # radius?
                d.ReadUint32(data)  # height?
            case s.ShapeType.HeightFieldShape:  # 5
                d.ReadUint32(data)  # columns?
                d.ReadUint32(data)  # rows?
                d.ReadUint32(data)  # height_scale?
                d.ReadUint32(data)  # row_scale?
                d.ReadUint32(data)  # column_scale?
                d.ReadByte(data)  # multiple_materials?
            case t if t.value > 5:
                raise Exception(f"Unknown Shape type {result[i].Type}")
            case s.ShapeType.SphereShape:  # 0
                d.ReadUint32(data)  # radius?
    return result


# stingray::ActorResource::serialize
# stingray::StrongPointerVectorSerializer<stingray::ActorResource>::serialize
def ActorResourceArrayDeserializer(self, data: bytes) -> list[s.ActorResource]:
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing length [{count}] ActorResource array")
    result = [s.ActorResource() for i in range(count)]
    for i in range(count):
        result[i].Name = d.ReadIDString32(data)
        result[i].ActorTemplate = d.ReadIDString32(data)
        result[i].Node = d.ReadIDString32(data)
        result[i].Mass = d.ReadUint32(data)
        result[i].Shapes = ShapeArrayDeserializer(self, data)
        result[i].OnStartTouch = d.ReadUint32(data)
        result[i].OnStayTouching = d.ReadUint32(data)
        result[i].OnEndTouch = d.ReadUint32(data)
        result[i].OnTriggerEnter = d.ReadUint32(data)
        result[i].OnTriggerLeave = d.ReadUint32(data)
        result[i].OnTriggerStay = d.ReadUint32(data)
        result[i].Enabled = d.ReadByte(data)
    self.report({"DEBUG"}, f"Finished length [{len(result)}] ActorResource array")
    return result


# stingray::StrongPointerVectorSerializer<stingray::Camera>::serialize
def CameraArrayDeserializer(self, data: bytes) -> list[s.Camera]:
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing length [{count}] Camera array")
    result = [s.Camera() for i in range(count)]
    for i in range(count):
        result[i].Name = d.ReadIDString32(data)
        result[i].Node = s.SceneGraphHandle()
        result[i].Node.Index = d.ReadUint32(data)
        result[i].CameraData = s.CameraData()
        result[i].CameraData.NearRange = d.ReadUint32(data)
        result[i].CameraData.FarRange = d.ReadUint32(data)
        result[i].CameraData.VerticalFov = d.ReadUint32(data)
        result[i].CameraData.Type = d.ReadUint32(data)
    return result


# stingray::operator&
def LightDataDeserializer(self, data: bytes) -> s.LightData:
    result = s.LightData()
    result.Color = d.ReadVector3(data)
    result.Intensity = d.ReadUint32(data)
    result.VolumetricIntensity = d.ReadUint32(data)
    result.Parameters = []
    for i in range(6):
        result.Parameters.append(d.ReadUint32(data))
    result.Type = d.ReadUint32(data)
    result.Flags = d.ReadUint32(data)
    result.Material = d.ReadIDString64(data)
    return result


# stingray::StrongPointerVectorSerializer<stingray::Light>::serialize
def LightArrayDeserializer(self, data: bytes) -> list[s.Light]:
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing length [{count}] Light array")
    result = [s.Light() for i in range(count)]
    for i in range(count):
        result[i].Name = d.ReadIDString32(data)
        result[i].Node = s.SceneGraphHandle()
        result[i].Node.Index = d.ReadUint32(data)
        result[i].LightData = LightDataDeserializer(self, data)
    return result


# stingray::Vector<stingray::LODObjectData::Step>::serialize
def StepArrayDeserializer(self, data: bytes) -> list[s.LODObjectData.Step]:
    count = d.ReadUint32(data)
    result = [s.LODObjectData.Step() for i in range(count)]
    for i in range(count):
        result[i].unk1 = d.ReadUint32(data)
        result[i].VisibleHeightRange = d.ReadUint32(data)
        meshes_count = d.ReadUint32(data)
        result[i].Meshes = []
        for _ in range(meshes_count):
            result[i].Meshes.append(d.ReadUint32(data))
    return result


# stingray::StrongPointerSerializer<stingray::LODObjectData>::serialize
# stingray::StrongPointerVectorSerializer<stingray::LODObjectData>::serialize
def LODObjectDataArrayDeserializer(self, data: bytes) -> list[s.LODObjectData]:
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing length [{count}] LODObjectData array")
    result = [s.LODObjectData() for i in range(count)]
    for i in range(count):
        result[i].Name = d.ReadIDString32(data)
        result[i].Node = s.SceneGraphHandle()
        result[i].Node.Index = d.ReadUint32(data)
        result[i].Steps = StepArrayDeserializer(self, data)
        result[i].BoundingVolume = BoundingVolumeDeserializer(self, data)
        result[i].Flags = d.ReadUint32(data)
    return result


# stingray::terrain::Node::serialize
def TerrainNodeDeserializer(self, data: bytes) -> s.Terrain.Node:
    result = s.Terrain.Node()
    result.HeightMin = d.ReadUint32(data)
    result.HeightMax = d.ReadUint32(data)
    result.Children = [None for i in range(4)]
    result.Children[0] = d.ReadUint32(data)
    result.Children[1] = d.ReadUint32(data)
    result.Children[2] = d.ReadUint32(data)
    result.Children[3] = d.ReadUint32(data)
    return result


# stingray::Vector<stingray::terrain::Node>::serialize
def TerrainNodeArrayDeserializer(self, data: bytes) -> list[s.Terrain.Node]:
    count = d.ReadUint32(data)
    result = []
    for i in range(count):
        result.append(TerrainNodeDeserializer(self, data))
    return result


# stingray::TerrainData::Layer::DecorationMaterial::serialize
def TerrainDataLayerDecorationMaterialDeserializer(
    self, data: bytes
) -> s.TerrainData.Layer.DecorationMaterial:
    result = s.TerrainData.Layer.DecorationMaterial()
    result.NUnits = d.ReadUint32(data)
    # function does something weird here
    # reads data interleaved?
    # TODO do properly
    result.Units = []
    for i in range(4):
        d.ReadIDString64(data)  # resource
        d.ReadUint32(data)  # mesh
        d.ReadUint32(data)  # density
    return result


# stingray::TerrainData::Layer::SurfaceProperties::serialize
def TerrainDataLayerSurfacePropertiesDeserializer(
    self, data: bytes
) -> s.TerrainData.Layer.TDSurfaceProperties:
    result = s.TerrainData.Layer.TDSurfaceProperties()
    result.NumContexts = d.ReadUint32(data)
    result.Contexts = [s.TerrainData.Layer.Context for i in range(result.NumContexts)]
    for i in range(result.NumContexts):
        result.Contexts[i].Name = d.ReadIDString32(data)
        result.Contexts[i].Materials = []
        for _ in range(5):
            result.Contexts[i].Materials.append(d.ReadIDString32(data))
    return result


# stingray::TerrainData::Layer::serialize
def TerrainDataLayerDeserializer(self, data: bytes) -> s.TerrainData.Layer:
    result = s.TerrainData.Layer()
    result.Map = d.ReadIDString64(data)
    result.Resolution = d.ReadUint32(data)
    result.Type = d.ReadUint32(data)
    result.Materials = []
    result.DecorationMaterials = []
    for i in range(4):
        result.Materials.append(d.ReadIDString32(data))
        result.DecorationMaterials.append(
            TerrainDataLayerDecorationMaterialDeserializer(self, data)
        )
    result.SurfaceProperties = TerrainDataLayerSurfacePropertiesDeserializer(self, data)
    return result


# stingray::TerrainData::serialize
def TerrainDataDeserializer(self, data: bytes) -> s.TerrainData:
    result = s.TerrainData()
    result.Name = d.ReadIDString32(data)
    result.Node = s.SceneGraphHandle()
    result.Node.Index = d.ReadUint32(data)
    result.BoundingVolume = BoundingVolumeDeserializer(self, data)
    result.Flags = d.ReadUint32(data)
    result.Nodes = TerrainNodeArrayDeserializer(self, data)
    result.BaseMaterial = d.ReadIDString32(data)
    result.NLayers = d.ReadUint32(data)
    result.Layers = []
    for i in range(result.NLayers):
        result.Layers.append(TerrainDataLayerDeserializer(self, data))
    result.HeightData = d.ReadByteArray(data)
    return result


# stingray::StrongPointerVectorSerializer<stingray::TerrainData>::serialize
def TerrainDataArrayDeserializer(self, data: bytes) -> list[s.TerrainData]:
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing length [{count}] TerrainData array")
    result = []
    for i in range(count):
        result.append(TerrainDataDeserializer(self, data))
    return result


def MotorDeserializer(self, data: bytes) -> s.JointDesc.Motor:
    result = s.JointDesc.Motor()
    result.VelocityTarget = d.ReadUint32(data)
    result.MaxForce = d.ReadUint32(data)
    return result


def SpringDeserializer(self, data: bytes) -> s.JointDesc.Spring:
    result = s.JointDesc.Spring()
    result.Spring = d.ReadUint32(data)
    result.Damper = d.ReadUint32(data)
    result.Target = d.ReadUint32(data)
    return result


# stingray::JointDesc::RevoluteJoint::serialize
def RevoluteJointDeserializer(self, data: bytes) -> s.JointDesc.RevoluteJoint:
    result = s.JointDesc.RevoluteJoint()
    result.LimitLow = d.ReadUint32(data)
    result.LimitHigh = d.ReadUint32(data)
    result.Motor = MotorDeserializer(self, data)
    result.Spring = SpringDeserializer(self, data)
    return result


# stingray::JointDesc::DistanceJoint::serialize
def DistanceJointDeserializer(self, data: bytes) -> s.JointDesc.DistanceJoint:
    result = s.JointDesc.DistanceJoint()
    result.MinDistance = d.ReadUint32(data)
    result.MaxDistance = d.ReadUint32(data)
    result.Spring = SpringDeserializer(self, data)
    return result


# stingray::JointDesc::SphericalJoint::serialize
def SphericalJointDeserializer(self, data: bytes) -> s.JointDesc.SphericalJoint:
    result = s.JointDesc.SphericalJoint()
    result.SwingAxis = d.ReadVector3(data)
    result.TwistLimitLow = d.ReadUint32(data)
    result.TwistLimitHigh = d.ReadUint32(data)
    result.SwingLimit = d.ReadUint32(data)
    result.TwistSpring = SpringDeserializer(self, data)
    result.SwingSpring = SpringDeserializer(self, data)
    result.JointSpring = SpringDeserializer(self, data)
    return result


# stingray::JointDesc::serialize
# stingray::Vector<stingray::JointDesc>::serialize
def JointDescArrayDeserializer(self, data: bytes) -> list[s.JointDesc]:
    count = d.ReadUint32(data)
    self.report({"DEBUG"}, f"Deserializing length [{count}] JointDesc array")
    result = [s.JointDesc() for i in range(count)]
    for i in range(count):
        result[i].Name = d.ReadIDString32(data)
        result[i].Enabled = d.ReadByte(data)
        result[i].Actor1 = d.ReadIDString32(data)
        result[i].Actor2 = d.ReadIDString32(data)
        result[i].Normal1 = d.ReadVector3(data)
        result[i].Normal2 = d.ReadVector3(data)
        result[i].Axis1 = d.ReadVector3(data)
        result[i].Axis2 = d.ReadVector3(data)
        result[i].GlobalAxis = d.ReadVector3(data)
        result[i].Anchor1 = d.ReadVector3(data)
        result[i].Anchor2 = d.ReadVector3(data)
        result[i].GlobalAnchor = d.ReadVector3(data)
        result[i].MaxForce = d.ReadUint32(data)
        result[i].MaxTorque = d.ReadUint32(data)
        result[i].JointFlags = d.ReadUint32(data)
        result[i].Type = s.JointType(d.ReadUint32(data))
        t = result[i].Type.value
        # maybe VT2 only uses revolute, distance, and spherical joints?
        if t != 0:
            if t == 1:  # Revolute
                result[i].Joint = RevoluteJointDeserializer(self, data)
            elif (t - 2) > 2:
                if t == 5:  # Distance
                    result[i].Joint = DistanceJointDeserializer(self, data)
                elif (t - 6) > 1:
                    raise Exception(f"Wrong joint type {result[i].Type}")
        else:  # Spherical
            result[i].Joint = SphericalJointDeserializer(self, data)
    return result


# stingray::MoverDesc::serialize
# stingray::Vector<stingray::MoverDesc>::serialize
def MoverDescArrayDeserializer(self, data: bytes) -> list[s.MoverDesc]:
    count = d.ReadUint32(data)
    result = [s.MoverDesc() for i in range(count)]
    for i in range(count):
        result[i].Name = d.ReadIDString32(data)
        result[i].Height = d.ReadUint32(data)
        result[i].Radius = d.ReadUint32(data)
        result[i].CollisionFilter = d.ReadIDString32(data)
        result[i].SlopeLimit = d.ReadUint32(data)
        result[i].OnActorHit = d.ReadUint32(data)
        result[i].OnMoverHit = d.ReadUint32(data)
    return result


# stingray::Vector<stingray::Pair<stingray::IdString32,stingray::ScriptMethod,0,1>>::serialize
def ScriptEventsDataArrayDeserializer(
    self, data: bytes
) -> list[tuple[IDString32, s.ScriptMethod]]:
    count = d.ReadUint32(data)
    result = []
    for i in range(count):
        first = d.ReadIDString32(data)
        second = s.ScriptMethod()
        second.Klass = d.ReadByteArray(data)
        second.F = d.ReadByteArray(data)
        result.append((first, second))
    return result


def VisibilityGroupsArrayDeserializer(
    self, data: bytes
) -> list[tuple[IDString32, list[int]]]:
    count = d.ReadUint32(data)
    result = []
    for i in range(count):
        first = d.ReadIDString32(data)
        second = d.ReadIntArray(data)
        result.append((first, second))
    return result


def MaterialsArrayDeserializer(
    self, data: bytes
) -> list[tuple[IDString32, IDString64]]:
    count = d.ReadUint32(data)
    result = []
    for i in range(count):
        first = d.ReadIDString32(data)
        second = d.ReadIDString64(data)
        result.append((first, second))
    return result


# stingray::UnitResource::serialize
def UnitResourceDeserializer(self, data: bytes) -> s.UnitResource:
    VERSION = 189
    UR = s.UnitResource()
    UR.Version = d.ReadUint32(data)
    self.report({"INFO"}, f"Unit resource version {UR.Version}")
    if ((UR.Version - VERSION) & 0xFFFEFFFF) != 0:
        self.report(
            {"WARNING"}, f"Version mismatch: expecting {VERSION} but got {UR.Version}"
        )
    UR.MeshGeometries = MeshGeometryArrayDeserializer(self, data)
    UR.Skins = SkinDataArrayDeserializer(self, data)
    # simple animation data is actually a .animation file embedded in the unit file
    UR.SimpleAnimation = d.ReadByteArray(data)
    if UR.SimpleAnimation:
        self.report(
            {"INFO"},
            f"Deserialized SimpleAnimation array length [{len(UR.SimpleAnimation)}]",
        )
    else:
        self.report({"DEBUG"}, "No SimpleAnimation data")
    UR.SimpleAnimationGroups = SimpleAnimationGroupArrayDeserializer(self, data)
    UR.SceneGraph = SceneGraphDeserializer(self, data)
    UR.Meshes = MeshObjectsArrayDeserializer(self, data)
    # each MeshObject has one MeshGeometry
    if len(UR.MeshGeometries) != len(UR.Meshes):
        raise Exception(
            f"Number of MeshGeometries [{len(UR.MeshGeometries)}] and number of Meshes [{len(UR.Meshes)}] do not match"
        )
    # ActorResource
    UR.Actors = ActorResourceArrayDeserializer(self, data)
    UR.Cameras = CameraArrayDeserializer(self, data)
    UR.Lights = LightArrayDeserializer(self, data)
    UR.LODObjects = LODObjectDataArrayDeserializer(self, data)
    UR.Terrains = TerrainDataArrayDeserializer(self, data)
    # no or very few unit files in VT2 have jointdesc data?
    UR.Joints = JointDescArrayDeserializer(self, data)
    UR.Movers = MoverDescArrayDeserializer(self, data)
    # no unit files in VT2 have script events data?
    UR.ScriptEvents = ScriptEventsDataArrayDeserializer(self, data)
    UR.HasAnimationBlenderBones = d.ReadBool(data)
    # most animation state machine data arrays are short (50 - 100 bytes)
    UR.AnimationStateMachine = d.ReadByteArray(data)
    if len(UR.AnimationStateMachine) > 0:
        self.report(
            {"DEBUG"},
            f"Found animation state machine data [{len(UR.AnimationStateMachine)}]",
        )
    # most unit files have dynamic data (8 - several hundred - over 1000 bytes)
    UR.DynamicData = d.ReadByteArray(data)
    if len(UR.DynamicData) > 0:
        self.report({"DEBUG"}, f"Found dynamic data [{len(UR.DynamicData)}]")

    UR.VisibilityGroups = VisibilityGroupsArrayDeserializer(self, data)
    if len(UR.VisibilityGroups) > 0:
        self.report({"DEBUG"}, f"Found [{len(UR.VisibilityGroups)}] visibility groups")

    UR.Flow = d.ReadByteArray(data)
    if len(UR.Flow) > 0:
        self.report({"DEBUG"}, f"Found flow data [{len(UR.Flow)}]")

    UR.FlowDynamicData = d.ReadByteArray(data)
    if len(UR.FlowDynamicData) > 0:
        self.report({"DEBUG"}, f"Found flow dynamic data [{len(UR.FlowDynamicData)}]")

    UR.MeshGeometryTriangleFinder = d.ReadByteArray(data)
    if len(UR.MeshGeometryTriangleFinder) > 0:
        self.report(
            {"DEBUG"},
            f"Found mesh geometry triangle finder [{len(UR.MeshGeometryTriangleFinder)}]",
        )

    UR.PhysicsSceneData = d.ReadByteArray(data)
    if len(UR.PhysicsSceneData) > 0:
        self.report({"DEBUG"}, f"Found physics scene data [{len(UR.PhysicsSceneData)}]")

    UR.PhysicsSceneData64bit = d.ReadByteArray(data)
    if len(UR.PhysicsSceneData64bit) > 0:
        self.report(
            {"DEBUG"},
            f"Found physics scene data 64bit [{len(UR.PhysicsSceneData64bit)}]",
        )

    UR.DefaultMaterialResource = d.ReadIDString64(data)
    self.report(
        {"DEBUG"}, f"Default material resource [{str(UR.DefaultMaterialResource)}]"
    )

    UR.Materials = MaterialsArrayDeserializer(self, data)
    self.report({"DEBUG"}, f"Materials: [{len(UR.Materials)}]")

    # what is apex data?
    UR.ApexData = d.ReadByteArray(data)

    # VT2 doesn't have any vroom vroom vehicles, so we can ignore?
    vehicle_count = d.ReadUint32(data)
    if vehicle_count > 0:
        raise NotImplementedError(
            f"Unit has {vehicle_count} vehicles; vehicle importing not supported"
        )

    UR.SkeletonName = d.ReadIDString64(data)
    self.report({"INFO"}, f"Default skeleton name: {str(UR.SkeletonName)}")

    # we should be at EOF now
    if c.offset != len(data):
        raise Exception(
            f"Finished deserializing at offset {c.offset}, but file length is {len(data)}"
        )

    return UR


# https://blender.stackexchange.com/a/170910
def find_blender_object(self, name: str) -> bpy.types.Object | None:
    for obj in bpy.context.scene.objects:
        if obj.name == name:
            return obj
    return None


def find_mesh_obj(
    mesh_obj_list: list[s.MeshObject], node_index: int
) -> s.MeshObject | None:
    # finds the MeshObject in the list that has the given node index
    result = [meshobj for meshobj in mesh_obj_list if meshobj.NodeIndex == node_index]
    if len(result) == 0:
        return None
    elif len(result) == 1:
        return result[0]
    else:
        # just to make sure
        raise Exception(f"More than one MeshObject found with NodeIndex {node_index}")


def GetOrMakeVertexGroup(b_object, name: str):
    # works for currently active object
    # look for vertex group with the given name
    # if it doesn't exist, make one
    try:
        # group = bpy.context.active_object.vertex_groups[name]
        group = b_object.vertex_groups[name]
        return group
    except Exception:
        # new_vertex_group = bpy.context.active_object.vertex_groups.new(name=name)
        new_vertex_group = b_object.vertex_groups.new(name=name)
        return new_vertex_group


def find_direct_children(scene_graph: list[s.SceneGraphData], node: int) -> list[int]:
    # find the direct children of a scene graph node
    result = []
    for i in range(node + 1, len(scene_graph)):
        if scene_graph[i].ParentIndex == node:
            result.append(i)
    return result


def make_armature(
    self,
    context,
    joint_indices: list[int],
    node_data: list[s.SceneGraphData],
    skeleton_name: IDString64,
    animations_list: list[IDString64],
) -> tuple[bpy.types.Armature, bpy.types.Object]:
    def get_children_of_joint(joint_index: int) -> list[int]:
        children = []
        for i in range(joint_index + 1, len(node_data)):
            if node_data[i].ParentIndex == joint_index:
                children.append(i)
        children = [i for i in children if i in joint_indices]
        return children

    def get_average_translation_of_children(bone_index: int):
        children = get_children_of_joint(bone_index)
        avg = None
        if len(children) > 1:
            # find average of their translations?
            vecs = [node_data[i].WorldTransform.decompose()[0] for i in children]
            vec_sum = Vector((0, 0, 0))
            for vec in vecs:
                vec_sum += vec
            avg = vec_sum / len(children)
        return avg

    MINUMUM_BONE_LENGTH = 0.001
    DEFAULT_LENGTH = 0.1

    # make a new armature
    armature = context.blend_data.armatures.new("armature")
    # An armature itself cannot exist in the view layer.
    # We need to create an object from it
    armature_obj = bpy.data.objects.new("armature object", armature)
    armature_obj.rotation_mode = "QUATERNION"
    # Link to collection so we can see it
    context.collection.objects.link(armature_obj)
    # Armature needs to be in EDIT mode to allow adding bones
    context.view_layer.objects.active = armature_obj
    bpy.ops.object.mode_set(mode="EDIT", toggle=False)

    # iterate over indices of nodes that are joints so we can build the armature
    first_bone_made = False
    for joint_index in joint_indices:
        current_node = node_data[joint_index]
        new_name = current_node.Name.lookup()
        new_bone = armature.edit_bones.new(new_name)

        # if we set matrix then set length, blender rotates the bone
        # so we should set length first
        new_bone.length = DEFAULT_LENGTH
        new_bone.matrix = current_node.WorldTransform

        # systematically change bone directions to better fit blender
        # nvm this breaks animations. maybe find a way to change both?
        # https://blender.stackexchange.com/a/196134
        """
        old_head = new_bone.head.copy()
        R = Matrix.Rotation(radians(-90), 4, new_bone.z_axis.normalized())
        new_bone.transform(R, roll=True)
        R = Matrix.Rotation(radians(-90), 4, new_bone.y_axis.normalized())
        new_bone.transform(R, roll=True)
        offset_vec = -(new_bone.head - old_head)
        new_bone.head += offset_vec
        new_bone.tail += offset_vec
        """
        # Assumption: all joints in a unit file have an internal parent (unless they are the 0th node)
        if joint_index != 0:
            assert current_node.ParentType.value == 1

        parent_node = node_data[current_node.ParentIndex]
        parent_name = parent_node.Name.lookup()
        parent_is_not_joint = current_node.ParentIndex not in joint_indices

        # changing this now
        """
        # the parent of a bone should always be a bone, unless this is the first bone
        if (current_node.ParentIndex not in joint_indices) and first_bone_made:
            raise Exception(
                f"New bone {new_name} (node {joint_index}) parent {parent_name} (node {current_node.ParentIndex}) is not a joint"
            )
            # self.report({'WARNING'}, f"New bone {new_name} [{joint_index}] parent {parent_name} [{current_node.ParentIndex}] is not a joint")
        """

        if not first_bone_made:
            first_bone_made = True

            # give armature object custom properties
            # give it the node index and IDString of the first joint
            armature_obj["SceneGraphNodeIndex"] = joint_index
            armature_obj["IDString32"] = str(current_node.Name)

            # add armature to imported objects
            assert not c.imported_objects.Armature
            c.imported_objects.Armature = armature_obj
            c.imported_objects.Objects[joint_index] = s.ImportedObject(
                armature_obj, armature_obj.name, current_node.Name
            )

            # time for the first bone in the armature
            # get list of joints that are direct children to the first bone
            children: list[int] = find_direct_children(node_data, joint_index)
            children = [i for i in children if i in joint_indices]

            # we shouldn't change the rotation of joints
            # changing their tails does just that

            new_bone.length = DEFAULT_LENGTH

        elif parent_is_not_joint:
            # do something?
            new_bone.use_connect = False
            new_bone.length = DEFAULT_LENGTH

        else:  # parent must be a bone
            parent_bone = armature.edit_bones[
                parent_name
            ]  # find parent EditBone by name in armature
            parent_children = get_children_of_joint(current_node.ParentIndex)

            if len(parent_children) > 1:  # parent bone has multiple children
                new_bone.use_connect = False
                new_bone.parent = parent_bone

            else:  # parent bone has one child
                new_bone.use_connect = False
                new_bone.parent = parent_bone
                new_bone.length = parent_bone.length

        # add bone to new objects list
        c.imported_objects.Objects[joint_index] = s.ImportedObject(
            new_bone, new_bone.name, current_node.Name
        )
        # add index to bone
        # EditBone has no attribute 'data'
        new_bone["SceneGraphNodeIndex"] = joint_index
        new_bone["IDString32"] = str(current_node.Name)

    # check if any bones still have 0 length
    # I don't know how to fix it, so warn the user for now
    for bone in armature.edit_bones.items():
        if bone[1].length < MINUMUM_BONE_LENGTH:
            raise Exception(f"Bone {bone[1].name} is too short: {bone[1].length}")
            self.report(
                {"WARNING"}, f"Bone {bone[1].name} has 0 length and will be deleted"
            )

    context.view_layer.objects.active = armature_obj
    bpy.ops.object.mode_set(mode="OBJECT")

    # give armature object a custom property so we know what .bones file it needs
    # https://blender.stackexchange.com/q/282242
    armature_obj["SkeletonName"] = str(skeleton_name)
    armature_obj["Animations"] = [str(i) for i in animations_list]

    return (armature, armature_obj)


def import_simple_animations(self, unit_resource: s.UnitResource):
    if len(unit_resource.SimpleAnimationGroups) > 1:  # TODO
        raise NotImplementedError(
            f"Cannot support SimpleAnimationGroups > 1 ({len(unit_resource.SimpleAnimationGroups)})"
        )

    if len(unit_resource.SimpleAnimationGroups) < 1:
        # nothing to do
        return

    group = unit_resource.SimpleAnimationGroups[0]
    assert group.Data[0] == 4294967295  # does this mean root node?
    assert group.Name.ID == 0  # when does it have a name?

    # figure out bones
    # these ints are the indices of nodes that are being animated
    # we need to send their names to animation importing
    """
    names: list[str] = []
    # manually add the 0th name
    names.append(new_objects[0].Name)
    for index in group.Data[1:]:
        names.append(new_objects[index].Name)
    """
    adjusted_group = group.Data.copy()
    adjusted_group[0] = 0

    try:
        import_animation.import_simple(
            self, unit_resource.SimpleAnimation, adjusted_group, unit_resource
        )
    except Exception as e:
        self.report({"WARNING"}, f"Could not import simple animation: {e}")


def import_materials(self, unit_res: s.UnitResource, directory: str):
    for tup in unit_res.Materials:
        mat_filename = f"{str(tup[1])}.material"
        mat_path = os.path.join(directory, mat_filename)
        mat_name = tup[0]
        import_material.try_import(self, mat_path, name=mat_name)


def load(self, context, filepath: str, relpath=None, **kwargs) -> set:
    assert c.CompiledUnitSettings

    current_dir = os.path.dirname(filepath)
    unit_name = IDString64(os.path.splitext(os.path.basename(filepath))[0])

    # can we read the file?
    try:
        with open(filepath, "rb") as f:
            data = f.read()
    except Exception as e:
        self.report({"ERROR"}, f"Can't read file {filepath}. {e}")
        traceback.print_exc(file=sys.stderr)
        return {"CANCELLED"}

    self.report({"INFO"}, f"Importing .unit file: {filepath}")
    # is it a compiled .unit file (of a version we can handle?)
    if data[:4] != b"\xbd\x00\x00\x00":
        self.report(
            {"ERROR"}, f"Wrong version or not a compiled .unit file: {filepath}"
        )
        traceback.print_exc(file=sys.stderr)
        return {"CANCELLED"}
    c.offset = 0

    # deserialize the unit file
    unit_data = UnitResourceDeserializer(self, data)

    # does the unit file have node data?
    # Nothing to do if there are no nodes
    if not len(unit_data.SceneGraph.Nodes) > 0:
        self.report({"ERROR"}, f"No nodes to import in {filepath}")
        return {"CANCELLED"}

    node_data: list[s.SceneGraphData] = unit_data.SceneGraph.Nodes
    mesh_objects: list[s.MeshObject] = unit_data.Meshes
    mesh_geometries: list[s.MeshGeometry] = unit_data.MeshGeometries
    skins: list[s.SkinData] = unit_data.Skins

    bones_res = None
    if c.CompiledUnitSettings.IMPORT_JOINTS:
        # try to import skeleton if unit has one
        if unit_data.SkeletonName.ID != 0:
            bones_res = import_bones.try_load(
                self, current_dir, str(unit_data.SkeletonName)
            )
            if not bones_res:
                self.report(
                    {"WARNING"},
                    f"Unit file names a skeleton {str(unit_data.SkeletonName)}.bones but the file could not be loaded from the current folder",
                )
        else:
            self.report(
                {"INFO"},
                "Unit does not have a default skeleton, so we have to guess which nodes are joints.",
            )

        if bones_res:
            # add bone names to rainbow table
            self.report({"INFO"}, "Loaded .bones file for unit")
            for name in bones_res.BoneNames:
                resource_map.addstr(name)
            self.report(
                {"INFO"},
                f"Added {len(bones_res.BoneNames)} strings to the rainbow table",
            )

            # if a node is named in the default skeleton, it should be in the scene graph
            node_names = [i.Name.lookup() for i in node_data]
            for h in bones_res.BoneNameHashes:
                if h.lookup() not in node_names:
                    raise Exception(
                        f"Bone {h.lookup()} is in the bones resource, but is not in the unit scene graph."
                    )

            # if a unit has a bones resource, it must also have an animation state machine
            # try to get list of animations it uses
            statemachine_path = os.path.join(current_dir, f"{unit_name}.state_machine")
            animations_list = import_state_machine.try_get_animations_list(
                self, statemachine_path
            )
        else:
            animations_list = []

    # TEST
    meshobjs_already_seen = []
    # TEST

    # Import the unit file by iterating over the node array
    # get list of the node indices referred to by any SkinData objects
    skindata_node_indices = [skindata.NodeIndices for skindata in skins]
    # flatten list
    skindata_node_indices = [
        index for sublist in skindata_node_indices for index in sublist
    ]

    def node_is_joint(i: int) -> bool:
        # not all joints are referred to in the skinning data
        # is a joint if it's listed in the .bones file
        # if we have no .bones file, we have to guess
        if bones_res:
            return node_data[i].Name in bones_res.BoneNameHashes
        else:
            return (
                ((i in skindata_node_indices) and (i != 0))
                or new_name.endswith("_scale")
                or new_name.startswith("j_")
            )

    joint_index_list: list[int] = []

    # iterate through nodes to find joints
    for i in range(len(node_data)):
        current_node: s.SceneGraphData = node_data[i]
        new_name: str = current_node.Name.lookup()
        has_geometry: bool = unit_data.does_node_have_geometry(i)
        # the first node is NOT always `root_point` (DE542DA9)
        is_joint: bool = node_is_joint(i)
        # a node should never have geometry AND be a joint
        # except for a few files where it does :(
        if has_geometry and is_joint:
            raise Exception(f"Node {new_name} [{i}] is a joint with mesh data")
        if is_joint:
            joint_index_list.append(i)

    if bones_res:
        try:
            assert len(bones_res.BoneNameHashes) == len(joint_index_list)
        except AssertionError:
            raise

    # initialize list of imported objects (used by simple animations)
    c.imported_objects = s.ImportedObjects()
    c.imported_objects.Objects = [None for i in range(len(node_data))]

    # new_objects: list[s.ImportedObject | None] = [None for i in range(len(node_data))]
    armature: bpy.types.Armature | None = (
        None  # can a unit file have more than one armature?
    )
    armature_obj: bpy.types.Object | None = None

    # if there are joints and import_joints is enabled, make an armature
    if (len(joint_index_list) > 0) and c.CompiledUnitSettings.IMPORT_JOINTS:
        armature, armature_obj = make_armature(
            self,
            context,
            joint_index_list,
            node_data,
            unit_data.SkeletonName,
            animations_list,
        )
        assert armature and armature_obj
        if len(armature.bones) != len(joint_index_list):
            raise Exception(
                f"Unit has [{len(joint_index_list)}] bones but the imported armature has [{len(armature.bones)}]"
            )

    if c.CompiledUnitSettings.IMPORT_MATERIALS:
        import_materials(self, unit_data, current_dir)

    first_bone_reached = False
    # iterate over node list
    for i in range(len(node_data)):
        current_node = node_data[i]
        new_name = current_node.Name.lookup()
        has_geometry = unit_data.does_node_have_geometry(i)
        is_joint = i in joint_index_list
        new_type = None

        if has_geometry:
            if not c.CompiledUnitSettings.IMPORT_MESHES:
                continue

            new_type = s.ObjectType.Object
            # get MeshObject that references this node
            mesh_obj = find_mesh_obj(mesh_objects, i)
            if not mesh_obj:
                raise Exception(f"Could not find MeshObject {new_name}")

            # node name and MeshObject.Name should be the same
            assert new_name == mesh_obj.Name.lookup()
            # node index and MeshObject.NodeIndex should be the same
            assert i == mesh_obj.NodeIndex
            # each MeshObject should have its own node
            # there should be a 1-1 correspondence between MeshObjects and their nodes
            assert i not in meshobjs_already_seen
            meshobjs_already_seen.append(i)

            if not mesh_obj.HasMeshGeo():
                raise Exception(f"MeshObject {new_name} has no geometry")

            # this list is not 0 indexed? what other ones are like this?
            mesh_geo: s.MeshGeometry = mesh_geometries[mesh_obj.GeometryIndex - 1]

            # make blender mesh and object
            new_obj = mesh_geo.create_blender_mesh(self, new_name)
            # new_obj = bpy.data.objects.new(new_name, new_obj)

            # get skin for MeshObject
            if HasSkinData(mesh_obj, mesh_geometries):
                skin: s.SkinData | None = skins[
                    mesh_obj.SkinIndex - len(mesh_objects) - 1
                ]  # funny indexing
            else:
                skin = None

            # if we have skin data
            if skin and c.CompiledUnitSettings.IMPORT_BLEND_WEIGHTS:
                for batch in mesh_geo.BatchRanges:
                    # this is a list of joint node indices?
                    joints_to_do = skin.GetJointsBelongingToSet(batch.BoneSet)
                    # make vertex groups for each bone
                    for index in joints_to_do:
                        bone_name = node_data[index].Name.lookup()
                        vertex_group = GetOrMakeVertexGroup(new_obj, bone_name)

                blend_indices = mesh_geo.BlendIndices
                blend_weights = mesh_geo.BlendWeights
                assert len(blend_indices) == len(blend_weights)

                # bpy.ops.object.mode_set(mode='EDIT')
                vertex_groups = new_obj.vertex_groups
                mesh = new_obj.data
                assert len(blend_indices) == len(mesh.vertices)

                for j in range(len(mesh.vertices)):
                    igroup = blend_indices[j]
                    wgroup = blend_weights[j]
                    assert len(igroup) == 4
                    assert len(wgroup) == 4
                    for k in range(4):
                        vertex_groups[igroup[k]].add([j], wgroup[k], "ADD")

                # transform weight data from vertex focus to group focus
                # dict of group index: vertex index, weight
                # new_data: dict[int, list[tuple[int, float]]] = {}
                """
                new_data = {}
                for j in range(0, len(blend_indices), 4):
                    vertex_index = int(j/4)
                    groups = blend_indices[j:j+4]
                    weights = blend_weights[j:j+4]
                    for k in range(4):
                        if groups[k] not in new_data.keys():
                            new_data[groups[k]] = [[],[]]
                        new_data[groups[k]][0].append(vertex_index)
                        new_data[groups[k]][1].append(weights[k])
                """

                """
                for k,v in new_data.items():
                    new_obj.vertex_groups[k].add(v[0], v[1], "REPLACE")

                """
                """
                for j in range(len(mesh.vertices)):
                    indices = blend_indices[j:j+4]
                    weights = blend_weights[j:j+4]
                    for k in range(4):
                        new_obj.vertex_groups[indices[k]].add([j], weights[k], "ADD")
                """

                # bpy.ops.object.mode_set(mode='OBJECT')

                new_modifier = new_obj.modifiers.new("Armature", "ARMATURE")
                new_modifier.object = armature_obj
                # https://blender.stackexchange.com/a/238055
                # add a weighted normal modifier to make normals not look bad when posing bones
                new_modifier = new_obj.modifiers.new(
                    "Weighted normal", "WEIGHTED_NORMAL"
                )

                # TODO also a good idea to go to edit mode, select all verts, M, merge by distance

        elif is_joint:
            if (
                not c.CompiledUnitSettings.NODE_PARENTING
                or not c.CompiledUnitSettings.IMPORT_JOINTS
            ):
                continue

            # we already made an armature
            new_type = s.ObjectType.Bone
            # but does it need to be parented?
            if not first_bone_reached:
                first_bone_reached = True

            # continue
        else:  # is empty
            if not c.CompiledUnitSettings.IMPORT_EMPTIES:
                continue
            new_type = s.ObjectType.Object
            new_obj = bpy.data.objects.new(new_name, None)
            # Decrease axis size to prevent overcrowding in the viewport
            new_obj.empty_display_size = 0.1

        if not is_joint:
            collection = context.collection
            if new_obj.name not in collection.objects:
                collection.objects.link(new_obj)
            # put new object in list of imported nodes
            c.imported_objects.Objects[i] = s.ImportedObject(
                new_obj, new_obj.name, current_node.Name
            )
            # all stingray rotations use quaternions
            new_obj.rotation_mode = "QUATERNION"

        # if NODE_PARENTING setting is set to false, don't parent non-bone objects
        if c.CompiledUnitSettings.NODE_PARENTING:
            # how should we parent the new object?
            match current_node.ParentType.value:
                case 0:  # None, no parent
                    self.report({"DEBUG"}, f"Object {new_name} has no parent")

                case 1:  # Internal, parent in file
                    parent = c.imported_objects.Objects[current_node.ParentIndex]
                    if not parent:
                        # parent might be an object we skipped importing
                        self.report(
                            {"INFO"},
                            f"Parent object for object {new_name} cannot be found. Maybe it was skipped?",
                        )
                        continue

                    parent_name = parent.Name
                    parent_obj = find_blender_object(self, parent_name)

                    # set parent for armature if we haven't already
                    if new_type == s.ObjectType.Bone:
                        assert armature_obj
                        if not first_bone_reached:
                            first_bone_reached = True
                            armature_obj.parent = parent_obj

                        if parent.Type == s.ObjectType.Object:
                            # if new object parent is not a bone, we must do a funky workaround
                            # by giving the bone a child-of constraint to the parent
                            new_bone = c.imported_objects.Objects[i]
                            assert new_bone
                            new_bone_pose = armature_obj.pose.bones[new_bone.Name]
                            bpy.context.view_layer.objects.active = armature_obj
                            bpy.ops.object.mode_set(mode="POSE", toggle=False)
                            new_constraint = new_bone_pose.constraints.new("CHILD_OF")
                            new_constraint.target = parent_obj
                            bpy.ops.object.mode_set(mode="OBJECT")

                    # new object must be a non-bone object
                    elif parent.Type == s.ObjectType.Object:  # if parent is not a bone
                        self.report(
                            {"DEBUG"}, f"Object {new_name} has parent {parent_name}"
                        )
                        new_obj.parent = parent_obj

                    else:  # parent must be a bone
                        assert armature
                        try:  # parenting an object to a bone require special treatment
                            armature_bone = armature.bones[parent_name]
                            # we have to do bone parenting in pose mode
                            bpy.context.view_layer.objects.active = armature_obj
                            bpy.ops.object.select_all(action="DESELECT")
                            new_obj.select_set(True)
                            bpy.ops.object.mode_set(mode="POSE", toggle=False)
                            armature_bone = armature.bones[parent_name]
                            bpy.context.object.data.bones.active = (
                                armature_bone  # select bone by name
                            )
                            bpy.ops.object.parent_set(type="BONE", keep_transform=True)
                            bpy.ops.object.mode_set(mode="OBJECT")
                            bpy.ops.object.select_all(action="DESELECT")
                        except KeyError:
                            # the parent bone probably got deleted by blender for being length 0
                            self.report(
                                {"WARNING"},
                                f"Parent bone {parent_name} for bone {new_name} cannot be found",
                            )

                case 2:  # External
                    raise NotImplementedError("ParentType = External")
                case 3:  # Unlinked
                    raise NotImplementedError("ParentType = Unlinked")
                case _:
                    raise Exception(
                        f"Unknown ParentType: {current_node.ParentType.value}"
                    )

        if not is_joint:
            # set custom property
            if new_obj:
                new_obj["SceneGraphNodeIndex"] = i
                new_obj["IDString32"] = str(current_node.Name)
            # Set local and world transforms
            new_obj.matrix_world = current_node.WorldTransform
            # new_obj.matrix_local = current_node.LocalTransform # do we need this?
            self.report({"DEBUG"}, f"Finished importing object {new_obj}")

        if i == 0:
            new_obj["IDString64"] = str(unit_name)
            add_unit_to_db(new_obj)

    # simple animations
    if c.CompiledUnitSettings.IMPORT_SIMPLEANIMS:
        import_simple_animations(self, unit_data)

    return {"FINISHED"}
